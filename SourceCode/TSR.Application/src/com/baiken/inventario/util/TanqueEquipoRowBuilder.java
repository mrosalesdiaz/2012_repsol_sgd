package com.baiken.inventario.util;

import java.text.MessageFormat;
import java.util.List;

import com.baiken.framework.report.excel.ICellBuilder;
import com.inventario.dto.Equipo;
import com.inventario.dto.Solicitud;
import com.inventario.dto.Tanque;

public class TanqueEquipoRowBuilder implements ICellBuilder {

    private String[][] data;

    public TanqueEquipoRowBuilder(Solicitud solicitud) {
        int height = Math.max(solicitud.getTanque().size(), solicitud.getEquipo().size());

        data = new String[height][9];

        List<Tanque> tanques = solicitud.getTanque();

        List<Equipo> equipos = solicitud.getEquipo();

        for (int i = 0; i < height; i++) {
            int count = 0;
            data[i][count++] = MessageFormat.format("{0}", i < tanques.size() ? tanques.get(i).getCantidad() : "");
            data[i][count++] = MessageFormat.format("{0}", i < tanques.size() ? tanques.get(i).getCapacidad() : "");
            data[i][count++] = MessageFormat.format("{0}", i < tanques.size() ? tanques.get(i).getOrientacion() : "");
            data[i][count++] = MessageFormat.format("{0}", i < tanques.size() ? tanques.get(i).getInstalacion() : "");

            data[i][count++] = "";

            data[i][count++] = MessageFormat.format("{0}", i < equipos.size() ? equipos.get(i).getCantidad() : "");
            data[i][count++] = MessageFormat.format("{0}", i < equipos.size() ? equipos.get(i).getEquipo() : "");
            data[i][count++] = MessageFormat.format("{0}", i < equipos.size() ? equipos.get(i).getMarca() : "");
            data[i][count++] = MessageFormat.format("{0}", i < equipos.size() ? equipos.get(i).getModelo() : "");
        }
        System.out.println("TanqueEquipoRowBuilder.TanqueEquipoRowBuilder()");
    }

    @Override
    public Object processRow(int row, int col) {
        String returnValue = "";

        if (row < data.length) {
            if (col < data[row].length) {
                returnValue = data[row][col];
            }
        }

        return returnValue;
    }

}
