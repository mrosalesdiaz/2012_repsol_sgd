package com.inventario.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.baiken.data.xml.DTOBase;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.NONE)
public class LiquidacionItem extends DTOBase {
    @XmlElement
    private int cantidad;
    public static String CANTIDAD="cantidad";
    @XmlElement
    private String tipo;
    public static String TIPO="tipo";
    @XmlElement
    private String descripcion;
    public static String DESCRIPCION="descripcion";
    @XmlElement
    private double monto;
    public static String MONTO="monto";
    public int getCantidad() {
        return cantidad;
    }
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public double getMonto() {
        return monto;
    }
    public void setMonto(double monto) {
        this.monto = monto;
    }

}
