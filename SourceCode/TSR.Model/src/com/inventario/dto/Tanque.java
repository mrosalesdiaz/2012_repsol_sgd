package com.inventario.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.baiken.data.xml.DTOBase;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.NONE)
public class Tanque extends DTOBase {
    @XmlElement
    private int cantidad;
    public static String CANTIDAD="cantidad";
    @XmlElement
    private double capacidad;
    public static String CAPACIDAD="capacidad";
    @XmlElement
    private String orientacion;
    public static String ORIENTACION="orientacion";
    @XmlElement
    private String instalacion;
    public static String INSTALACION="instalacion";

    public double getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(double capacidad) {
        this.capacidad = capacidad;
    }

    public String getOrientacion() {
        return orientacion;
    }

    public void setOrientacion(String orientacion) {
        this.orientacion = orientacion;
    }

    public String getInstalacion() {
        return instalacion;
    }

    public void setInstalacion(String instalacion) {
        this.instalacion = instalacion;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getCantidad() {
        return cantidad;
    }

}
