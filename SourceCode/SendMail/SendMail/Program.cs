using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using System.Web;
using System.Collections.Specialized;
using System.IO;

namespace SendMail
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            String parameters=Environment.GetCommandLineArgs()[1];

            Uri uri = new Uri(parameters);

            NameValueCollection query = HttpUtility.ParseQueryString(uri.PathAndQuery);

            Microsoft.Office.Interop.Outlook.Application a = new Microsoft.Office.Interop.Outlook.Application();
            MailItem mailItem = (MailItem)a.CreateItem(OlItemType.olMailItem);
            mailItem.Subject = query["subject"];
            mailItem.To = String.Format("{0}@{1}",uri.UserInfo,uri.Host);
          //  mailItem.To = uri.;
            mailItem.Body = query["body"];
            mailItem.Importance = OlImportance.olImportanceNormal;
            mailItem.Display(false);


            foreach (string var in query["attach"].Split(",".ToCharArray()))
            {
                mailItem.Attachments.Add(
                var,
                OlAttachmentType.olByValue,
                1,
                Path.GetFileName(var));
    
            }
            
        }
    }
}