/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventario.view.util;

import java.awt.Graphics;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultCellEditor;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * 
 */
public class BeanTableCellRenderer extends DefaultTableCellRenderer {

    private String key;
    private Field[] fieldDef;

    public BeanTableCellRenderer(Class clazz, String key) {
        this.key = key;
        try {
            String[] keys = key.split("\\.");
            fieldDef = new Field[keys.length];
            String propertyName = "";
            Class tempClass = clazz;
            for (int i = 0; i < fieldDef.length; i++) {
                propertyName = keys[i];
                this.fieldDef[i] = tempClass.getDeclaredField(propertyName);
                tempClass = this.fieldDef[i].getType();
            }


        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    protected void setValue(Object value) {
        Object returnValue = null;

        if (value == null) {
            returnValue = "";
        } else {
            try {
                Object tempIntance=value;
                for (int i = 0; i < fieldDef.length; i++) {
                    Field tempField = fieldDef[i];
                    tempField.setAccessible(true);
                    tempIntance = tempField.get(tempIntance);
                    tempField.setAccessible(false);
                }
                returnValue=tempIntance;
            } catch (Exception e) {
                returnValue = "ERROR!!!" + e.getMessage();
            }
        }

        super.setValue(returnValue);
    }
}
