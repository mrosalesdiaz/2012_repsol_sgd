package com.baiken.framework.report.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.poi.hsmf.dev.HSMFDump;
import org.apache.poi.hssf.util.AreaReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellReference;

class SimpleReporter implements IReporteableClass {

    private File templateFile;
    private Map<String, String> namedCells;
    private Map<Integer, Integer> insertRows;
    private Map<Integer, ICellBuilder> builders;

    public SimpleReporter(File templateFile) {
        namedCells = new LinkedHashMap<String, String>();
        insertRows = new LinkedHashMap<Integer, Integer>();
        builders = new LinkedHashMap<Integer, ICellBuilder>();
        setTemplate(templateFile);
    }

    @Override
    public Set<String> getVariablesName() {
        return namedCells.keySet();
    }

    @Override
    public <Type> void setVariable(String variableName, Type value) {
        if (!namedCells.containsKey(variableName)) {
            throw new IndexOutOfBoundsException("Field: " + variableName + " does not exists");
        } else {
            namedCells.put(variableName, String.valueOf(value));
        }
    }

    @Override
    public <Type> void setFormula(String variableName, int newRows) {
        if (!namedCells.containsKey(variableName)) {
            throw new IndexOutOfBoundsException("Field: " + variableName + " does not exists");
        } else {
            namedCells.put(variableName, String.valueOf("formula_" + newRows));
        }
    }

    @Override
    public void addRowsBelow(int rowIndex, int numberOfRows, ICellBuilder builder) {
        insertRows.put(rowIndex, numberOfRows);
        builders.put(rowIndex, builder);
    }

    @Override
    public void save(File file) {
        String debugString = "Start";
        Workbook wb;

        FileOutputStream out;
        try {
            debugString = "Loading template from: " + templateFile.getAbsolutePath();

            wb = WorkbookFactory.create(templateFile);

            Sheet sheet0 = wb.getSheetAt(0);
            sheet0.setSelected(true);

            // Sets ta value for named cells
            for (Entry<String, String> row : namedCells.entrySet()) {
                if (row.getValue() == null) {
                    continue;
                }

                int index = wb.getNameIndex(row.getKey());

                Name namedCell = wb.getNameAt(index);

                AreaReference areaReference = new AreaReference(namedCell.getRefersToFormula());
                CellReference[] crefs = areaReference.getAllReferencedCells();

                for (CellReference cellReference : crefs) {
                    sheet0.getRow(cellReference.getRow()).getCell(cellReference.getCol()).setCellValue(row.getValue() == null ? "" : row.getValue());
                }
            }

            int totalRowsAdded = 0;
            {// AddRows and process values
                List<Integer> indexes = new LinkedList<Integer>(builders.keySet());

                Collections.sort(indexes);

                CellStyle[] rowStyle = null;

                for (int i = 0; i < indexes.size(); i++) {
                    int originalIndexBelowToInsert = indexes.get(i);
                    int rowIndexBelowToInsert = originalIndexBelowToInsert + totalRowsAdded;
                    int numberOfRowAdded = insertRows.get(originalIndexBelowToInsert);
                    ICellBuilder builder = builders.get(originalIndexBelowToInsert);

                    if (rowStyle == null) {
                        rowStyle = new CellStyle[sheet0.getRow(rowIndexBelowToInsert).getLastCellNum()];
                        for (int j = 0; j < rowStyle.length; j++) {
                            Cell cell = sheet0.getRow(rowIndexBelowToInsert).getCell(j);
                            if (cell != null) {
                                rowStyle[j] = cell.getCellStyle();
                            }
                        }
                    }
                    if (numberOfRowAdded > 1) {
                        sheet0.shiftRows(rowIndexBelowToInsert, sheet0.getLastRowNum(), numberOfRowAdded - 1, false, false);

                    }

                    // TODO: Iterates between cells to add
                    for (int j = 0; j < numberOfRowAdded; j++) {
                        int newRowIndex = rowIndexBelowToInsert + j;
                        Row newRow = sheet0.createRow(newRowIndex);
                        for (int k = 0; k < rowStyle.length; k++) {
                            if (rowStyle[k] != null) {
                                int newCellIndex = k;
                                Cell newCell = newRow.createCell(k);
                                newCell.setCellStyle(rowStyle[k]);
                                if (builder != null) {
                                    Object processedValue = builder.processRow(j, k);
                                    if (processedValue == null) {
                                        newCell.setCellValue("");
                                    } else if (processedValue.getClass().equals(Double.class)) {
                                        newCell.setCellValue((Double) processedValue);
                                    } else if (processedValue.getClass().equals(Integer.class)) {
                                        newCell.setCellValue((Integer) processedValue);
                                    } else {
                                        newCell.setCellValue((String) processedValue);
                                    }

                                }
                            }
                        }
                    }
                    totalRowsAdded += numberOfRowAdded - 1;
                    rowStyle = null;
                }
            }

            {// update formulas

                int nameCount = wb.getNumberOfNames();

                Name name;
                for (int nameIndex = 0; nameIndex < nameCount; nameIndex++) {
                    name = wb.getNameAt(nameIndex);

                    if (name.getNameName().startsWith("formula_")) {
                        AreaReference areaReference = new AreaReference(name.getRefersToFormula());
                        CellReference[] crefs = areaReference.getAllReferencedCells();

                        for (CellReference cellReference : crefs) {
                            String formula = sheet0.getRow(cellReference.getRow()).getCell(cellReference.getCol()).getCellFormula();
                            Object[] parameters = new MessageFormat("SUM({0}:{1})").parse(formula);

                            parameters[0] = "" + parameters[0].toString().subSequence(0, 1)
                                    + (Integer.parseInt(parameters[0].toString().subSequence(1, parameters[0].toString().length()).toString()) - totalRowsAdded);

                            formula = MessageFormat.format("SUM({0}:{1})", parameters);
                            sheet0.getRow(cellReference.getRow()).getCell(cellReference.getCol()).setCellFormula(formula);

                        }
                    }
                }

            }

            sheet0.setForceFormulaRecalculation(true);
            out = new FileOutputStream(file);
            debugString = "Wrinting the file into:" + file.getAbsolutePath();
            wb.write(out);
            debugString = "Closing the file:" + file;
            out.close();
            
            insertRows = new LinkedHashMap<Integer, Integer>();
            builders = new LinkedHashMap<Integer, ICellBuilder>();

        } catch (Exception e) {
            throw new RuntimeException(debugString, e);
        }
    }

    @Override
    public void setTemplate(File file) {
        String debugString = "Setting file template" + file;
        this.templateFile = file;

        try {
            debugString = "Loading the template file from: " + templateFile.getAbsolutePath();
            Workbook wb;
            try {
                debugString = "Creating instance of workbook from file " + templateFile.getAbsolutePath();
                wb = WorkbookFactory.create(templateFile);
            } catch (Exception e) {
                throw new RuntimeException(debugString, e);
            }

            int nameCount = wb.getNumberOfNames();
            debugString = "Loading file names in file:" + templateFile.getAbsolutePath();

            Name name;
            for (int nameIndex = 0; nameIndex < nameCount; nameIndex++) {
                debugString = "Getting values for index: " + nameIndex;
                name = wb.getNameAt(nameIndex);
                debugString = "puting name: " + name.getNameName() + " in the list";
                namedCells.put(name.getNameName(), null);
            }
            wb = null;
        } catch (Exception e) {
            throw new RuntimeException(debugString, e);
        }
    }

    private static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }
        if (!destFile.exists()) {
            destFile.createNewFile();
        }
        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }

    }
}
