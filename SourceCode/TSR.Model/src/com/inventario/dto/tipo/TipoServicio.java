package com.inventario.dto.tipo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.baiken.data.xml.DTOBase;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.NONE)
public class TipoServicio extends DTOBase {
    @XmlElement
    private String nombre;
    public static String NOMBRE = "nombre";

    @XmlElement
    private String inicial;
    public static String INICIAL = "inicial";
    @XmlElement
    private int contador;
    public static String CONTADOR = "contador";

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setInicial(String inicial) {
        this.inicial = inicial;
    }

    public String getInicial() {
        return inicial;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    public int getContador() {
        return contador;
    }

}
