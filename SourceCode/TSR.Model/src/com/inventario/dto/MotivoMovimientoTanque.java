package com.inventario.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.baiken.data.xml.DTOBase;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.NONE)
public class MotivoMovimientoTanque extends DTOBase {
    @XmlElement
    private String instalacionReal;
    public static String INSTALACIONREAL = "instalacionReal";
    @XmlElement
    private String descripcion;
    public static String DESCRIPCION = "descripcion";
    @XmlElement
    private String numeroGrafo;
    public static String NUMEROGRAFO = "numeroGrafo";
    @XmlElement
    private String numeroReserva;
    public static String NUMERORESERVA = "numeroReserva";
    @XmlElement
    private String serieCodigo;
    public static String SERIECODIGO = "serieCodigo";

    @XmlElement
    private int cantidad;
    public static String CANTIDAD = "cantidad";
    @XmlElement
    private double capacidad;
    public static String CAPACIDAD = "capacidad";
    public String getInstalacionReal() {
        return instalacionReal;
    }
    public void setInstalacionReal(String instalacionReal) {
        this.instalacionReal = instalacionReal;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getNumeroGrafo() {
        return numeroGrafo;
    }
    public void setNumeroGrafo(String numeroGrafo) {
        this.numeroGrafo = numeroGrafo;
    }
    public String getNumeroReserva() {
        return numeroReserva;
    }
    public void setNumeroReserva(String numeroReserva) {
        this.numeroReserva = numeroReserva;
    }
    public String getSerieCodigo() {
        return serieCodigo;
    }
    public void setSerieCodigo(String serieCodigo) {
        this.serieCodigo = serieCodigo;
    }
    public int getCantidad() {
        return cantidad;
    }
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    public double getCapacidad() {
        return capacidad;
    }
    public void setCapacidad(double capacidad) {
        this.capacidad = capacidad;
    }

}
