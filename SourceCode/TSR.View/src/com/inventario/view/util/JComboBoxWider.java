/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventario.view.util;

import java.awt.Dimension;
import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;

/**
 *com.inventario.view.util.JComboBoxWider
 * 
 */
public class JComboBoxWider extends JComboBox {

    public JComboBoxWider() {
    }

    public JComboBoxWider(final Object items[]) {
        super(items);
    }

    public JComboBoxWider(Vector items) {
        super(items);
    }

    public JComboBoxWider(ComboBoxModel aModel) {
        super(aModel);
    }
    private boolean layingOut = false;

    public void doLayout() {
        try {
            layingOut = true;
            super.doLayout();
        } finally {
            layingOut = false;
        }
    }

    public Dimension getSize() {
        Dimension dim = super.getSize();
        if (!layingOut) {
            dim.width = Math.max(dim.width, getPreferredSize().width);
        }
        return dim;
    }
}
