package com.baiken.data.xml;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;



public abstract class DAOBase<DTOType extends DTOBase> {

    private static final String UTF_8 = "UTF-8";

    private String ROOT_TAG_NAME ;

    public File baseFolder;

    private JAXBContext context;


    /**
     * The generic type for this class. It is going to use to get the class of
     * the DTO passed as {@link DTOBase}.
     */
    protected final ParameterizedType parameterizedType = (ParameterizedType) this.getClass().getGenericSuperclass();

    /**
     * Class for the {@link DTOBase} for this {@link DAOBase}
     */
    @SuppressWarnings("unchecked")
    protected final Class<DTOBase> dtoClass = (Class<DTOBase>) parameterizedType.getActualTypeArguments()[0];
    public DAOBase() {
        try {
            ROOT_TAG_NAME=(""+dtoClass.getSimpleName().charAt(0)).toLowerCase()+dtoClass.getSimpleName().substring(1,dtoClass.getSimpleName().length());
            baseFolder = new File("data", this.getClass().getName());
            if (!baseFolder.exists()) {
                if (baseFolder.isFile()) {
                    throw new RuntimeException("La ruta: " + baseFolder + " ya existe como archivo. Este tiene q ser una carpeta. Elimine el archivo.");
                } else {
                    baseFolder.mkdirs();
                }
            }

            context = JAXBContext.newInstance(dtoClass);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized DTOType find(String id) {
        DTOType returnValue = null;
        FileInputStream fileInputStream = null;
        InputStreamReader fileReader = null;
        Unmarshaller unmarshaller = null;
        try {
            fileInputStream = new FileInputStream(new File(baseFolder, id + ".xml"));
            fileReader = new InputStreamReader(fileInputStream, UTF_8);
            unmarshaller = context.createUnmarshaller();
            returnValue = (DTOType) unmarshaller.unmarshal(fileReader);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            DataXmlUtil.release(fileReader, "close");
            DataXmlUtil.release(fileInputStream, "close");
        }
        return returnValue;
    }

    public synchronized void delete(String id) {
        try {
            File file = new File(baseFolder, id + ".xml");
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void save(DTOType dto) {
        FileOutputStream fileOutputStream = null;
        OutputStreamWriter fileWriter = null;
        Marshaller marshaller = null;
        try {
            fileOutputStream = new FileOutputStream(new File(baseFolder, dto.getId() + ".xml"));
            fileWriter = new OutputStreamWriter(fileOutputStream, UTF_8);
            marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(dto, fileWriter);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            DataXmlUtil.release(fileWriter, "close");
            DataXmlUtil.release(fileOutputStream, "close");
        }

    }

    public synchronized List<DTOType> search(final String xPathString) {
        List<DTOType> returnValue;
        File[] files;
        DocumentBuilderFactory factory;
        DocumentBuilder builder;
        Document doc;
        List<String> selectedIds;
        try {
            returnValue = new ArrayList<DTOType>();
            selectedIds = new ArrayList<String>();
            factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();

            files = baseFolder.listFiles(new FileFilter() {

                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().toLowerCase().endsWith(".xml");
                }
            });

            for (File file : files) {
                doc = builder.parse(file);
                XPathFactory xPathfactory = XPathFactory.newInstance();
                XPath xpath = xPathfactory.newXPath();
                XPathExpression expr = xpath.compile(xPathString);

                Object result = expr.evaluate(doc);
                if (result != null && result instanceof String && ((String) result).trim().toLowerCase().equals("true")) {
                    selectedIds.add(file.getName().substring(0,file.getName().indexOf(".")));
                }

            }
            {// read data
                DTOType dto = null;
                FileInputStream fileInputStream = null;
                InputStreamReader fileReader = null;
                Unmarshaller unmarshaller = null;
                unmarshaller = context.createUnmarshaller();
                for (String dtoId : selectedIds) {

                    try {
                        fileInputStream = new FileInputStream(new File(baseFolder, dtoId + ".xml"));
                        fileReader = new InputStreamReader(fileInputStream, UTF_8);
                        dto = (DTOType) unmarshaller.unmarshal(fileReader);
                        returnValue.add(dto);

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        throw new RuntimeException(e);
                    } finally {
                        DataXmlUtil.release(fileReader, "close");
                        DataXmlUtil.release(fileInputStream, "close");
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            returnValue = new ArrayList<DTOType>();
        }
        return returnValue;
    }

    public synchronized int count(String xPathString) {
        int returnValue;
        File[] files;
        DocumentBuilderFactory factory;
        DocumentBuilder builder;
        Document doc;
        List<String> selectedIds;
        try {
            selectedIds = new ArrayList<String>();
            factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();

            files = baseFolder.listFiles(new FileFilter() {

                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().toLowerCase().endsWith(".xml");
                }
            });

            for (File file : files) {
                doc = builder.parse(file);
                XPathFactory xPathfactory = XPathFactory.newInstance();
                XPath xpath = xPathfactory.newXPath();
                XPathExpression expr = xpath.compile(xPathString);

                Object result = expr.evaluate(doc);
                if (result != null && result instanceof String && !((String) result).trim().equals("")) {
                    selectedIds.add(result.toString());
                }

            }
            returnValue = selectedIds.size();
        } catch (Exception e) {
            e.printStackTrace();
            returnValue = -1;
        }
        return returnValue;
    }

    public synchronized List<DTOType> list() {
    	List<DTOType> returnValue;
        File[] files;
        try {
            returnValue = new ArrayList<DTOType>();

            files = baseFolder.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().toLowerCase().endsWith(".xml");
                }
            });
            {// read data
                DTOType dto = null;
                FileInputStream fileInputStream = null;
                InputStreamReader fileReader = null;
                Unmarshaller unmarshaller = null;
                unmarshaller = context.createUnmarshaller();
                for (File file : files) {
                    try {
                        fileInputStream = new FileInputStream(new File(baseFolder, file.getName() ));
                        fileReader = new InputStreamReader(fileInputStream, UTF_8);
                        dto = (DTOType) unmarshaller.unmarshal(fileReader);
                        returnValue.add(dto);

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        throw new RuntimeException(e);
                    } finally {
                        DataXmlUtil.release(fileReader, "close");
                        DataXmlUtil.release(fileInputStream, "close");
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            returnValue = new ArrayList<DTOType>();
        }
        return returnValue;
    }

    public synchronized int count() {
        return count("/"+ROOT_TAG_NAME+"/@id != ''");
    }
}
