package com.inventario.dao;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.baiken.data.xml.DAOBase;
import com.inventario.dto.Canal;
import com.inventario.dto.Planta;
import com.inventario.dto.Configuraciones;

public class DAOConfiguracion extends DAOBase<Configuraciones> {

    public String generarNumeroParaTipoSolicitud(String inicialTipoServicio, String planta) {
        String key = inicialTipoServicio + planta;
        Configuraciones configuraciones = null;
        try {

            configuraciones = find(key);
        } catch (Exception e) {
        }

        if (configuraciones == null) {
            configuraciones = new Configuraciones();
            configuraciones.setId(key);
            configuraciones.setCounter(1);
            save(configuraciones);
        }

        int counter = configuraciones.getCounter();
        ++counter;
        configuraciones.setCounter(counter);
        save(configuraciones);

        return MessageFormat.format("{0}{1}-{2,number,000000}", inicialTipoServicio, planta, counter);
    }

}
