package com.inventario.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;

import com.baiken.data.xml.DTOBase;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.NONE)
public class Planta extends DTOBase {
	public static String NOMBRECENTROCOSTO = "nombreCentroCosto";
	@XmlElement
	private String nombreCentroCosto;

	public static String CODIGODEPLANTA = "codigoDePlanta";
	@XmlElement
	private String codigoDePlanta;

	public static String NOMBREDEPLANTA = "nombreDePlanta";
	@XmlElement
	private String nombreDePlanta;

	public String getNombreCentroCosto() {
		return nombreCentroCosto;
	}

	public void setNombreCentroCosto(String nombre) {
		this.nombreCentroCosto = nombre;
	}

	public void setCodigoDePlanta(String nombreDePlanta) {
		this.codigoDePlanta = nombreDePlanta;
	}

	public String getCodigoDePlanta() {
		return codigoDePlanta;
	}

	public void setNombreDePlanta(String nombreDePlanta) {
		this.nombreDePlanta = nombreDePlanta;
	}

	public String getNombreDePlanta() {
		return nombreDePlanta;
	}

}
