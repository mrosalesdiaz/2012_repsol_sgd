package com.baiken.inventario.util;

import java.util.List;

import com.baiken.framework.report.excel.ICellBuilder;
import com.inventario.dto.MotivoMovimientoTanque;
import com.inventario.dto.Solicitud;

public class MotivoMovimientoRowBuilder implements ICellBuilder {

    private String[][] data;

    public MotivoMovimientoRowBuilder(Solicitud solicitud) {
        int height = solicitud.getMovimientosTanque().size();

        data = new String[height][8];

        List<MotivoMovimientoTanque> motivoMovimiento = solicitud.getMovimientosTanque();

        for (int i = 0; i < height; i++) {
            int count = 0;
            data[i][count++] = "" + (i + 1);
            data[i][count++] = motivoMovimiento.get(i).getInstalacionReal();
            data[i][count++] = motivoMovimiento.get(i).getDescripcion();
            data[i][count++] = motivoMovimiento.get(i).getNumeroGrafo();
            data[i][count++] = motivoMovimiento.get(i).getNumeroReserva();
            data[i][count++] = "" + motivoMovimiento.get(i).getCantidad();
            data[i][count++] = "" + (int)motivoMovimiento.get(i).getCapacidad();
            data[i][count++] = motivoMovimiento.get(i).getSerieCodigo();
        }

    }

    @Override
    public Object processRow(int row, int col) {
        String returnValue = "";

        if (row < data.length) {
            if (col < data[row].length) {
                returnValue = data[row][col];
            }
        }

        return returnValue;
    }

}
