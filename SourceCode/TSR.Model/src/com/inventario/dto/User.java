package com.inventario.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;

import com.baiken.data.xml.DTOBase;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.NONE)
public class User extends DTOBase {

    public static String ID = "id";

    @XmlElement
    private String userName;
    public static String USERNAME = "userName";

    @XmlElement
    private String password;
    public static String PASSWORD = "password";

    @XmlElement
    private String role;
    public static String ROLE = "role";

    @XmlElement
    private String fullName;
    public static String FULLNAME = "fullName";

    @XmlElement
    private String email;
    public static String EMAIL = "email";

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
