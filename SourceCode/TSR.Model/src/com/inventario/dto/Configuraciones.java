package com.inventario.dto;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.baiken.data.xml.DTOBase;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.NONE)
public class Configuraciones extends DTOBase {
    @XmlElement
    private int counter;

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public int getCounter() {
        return counter;
    }

}
