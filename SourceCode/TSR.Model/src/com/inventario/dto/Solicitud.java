package com.inventario.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.baiken.data.xml.DTOBase;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.NONE)
public class Solicitud extends DTOBase {
    public static String MOTIVOPARAEJECUCION = "motivoParaEjecucion";
    public static String MONTOLIQUIDACION = "montoLiquidacion";
    public static String EECC = "EECC";
    public static String MOTIVOMOVIMIENTOTANQUEDESCRIPCION = "motivoMovimientoTanqueDescripcion";
    public static String MOVIMIENTOTANQUECOMENTARIO = "movimientoTanqueComentario";
    public static String CANAL = "canal";
    public static String CENTROCOSTO = "centroCosto";
    public static String CODIGOINSTALACION = "codigoInstalacion";
    public static String CODIGOSOLICITUD = "codigoSolicitud";
    public static String CODIGOORDENSOPORTETECNICO = "codigoOrdenSoporteTecnico";
    public static String CODIGOTITULAR = "codigoTitular";
    public static String CUENTACONTABLE = "cuentaContable";
    public static String DESCRIPCIONSERVICIO = "descripcionServicio";
    public static String DIRECCIONOBRA = "direccionObra";
    public static String DISTANCIAHORIZONTAL = "distanciaHorizontal";
    public static String DISTANCIAVERTICAL = "distanciaVertical";
    public static String EMAILCONTACTO = "emailContacto";
    public static String EQUIPO = "equipo";
    public static String FECHAEMISION = "fechaEmision";
    public static String FECHAFINOBRA = "fechaFinObra";
    public static String FECHAINICIOOBRA = "fechaInicioObra";
    public static String INGENIERODT = "ingenieroDT";
    public static String INSTALACION = "instalacion";
    public static String JUSTIFICACIONAFI = "justificacionAFI";
    public static String NOMBRECOMPLETOSOLICITADOR = "nombreCompletoSolicitador";
    public static String NOMBREPLANTA = "nombrePlanta";
    public static String CODIGOPLANTA = "codigoPlanta";
    public static String NUMEROAFI = "numeroAFI";
    public static String PERSONACONTACTONOMBRE = "personaContacto";
    public static String PERSONACONTACTOTELEFONO = "personaContacto";
    public static String PERSONACONTACTOMAIL = "personaContacto";
    public static String RAZONCOMERCIAL = "razonComercial";
    public static String RAZONSOCIAL = "razonSocial";
    public static String REFERENCIA = "referencia";
    public static String RUCODNI = "rucOdni";
    public static String SUPERVISORCOMERCIAL = "supervisorComercial";
    public static String TANQUE = "tanque";
    public static String TELEFONOCONTACTO = "telefonoContacto";
    public static String TIPOPARTIDA = "tipoPartida";
    public static String TIPOSOLICITUD = "tipoSolicitud";
    public static String TITULAR = "titular";
    public static String ARCHIVOSADJUNTOS = "archivosAdjuntos";
    public static String ESTADO = "estado";
    public static String RESPONSABLEEECC = "responsableEECC";
    public static String MONTOPRESUPUESTADO = "montoPresupuestado";
    public static String MONTOFACTURADO = "montoFacturado";
    public static String FECHASALIDA = "fechaSalida";
    public static String NUMEROGUIA = "numeroGuia";
    public static String FECHARECEPCION = "fechaRecepcion";
    public static String LIQUIDACIONES = "liquidaciones";
    public static String MOVIMIENTOTANQUE = "movimientoTanque";
    public static String FECHAFINALIZACION = "fechaFinalizacion";

    @XmlElement
    private String movimientoTanqueComentario;
    @XmlElement
    private Date fechaFinalizacion;
    @XmlElement
    private boolean movimientoTanque;

    @XmlElement
    private List<LiquidacionItem> liquidaciones;

    @XmlElement
    private Date fechaRecepcion;
    @XmlElement
    private String numeroGuia;
    @XmlElement
    private Date fechaSalida;

    @XmlElement
    private double montoPresupuestado;

    @XmlElement
    private double montoFacturado;

    @XmlElement
    private String motivoParaEjecucion;
    @XmlElement
    private String responsableEECC;
    @XmlElement
    private String codigoOrdenSoporteTecnico;
    @XmlElement
    private double montoLiquidacion;
    @XmlElement
    private String eecc;
    @XmlElement
    private String motivoMovimientoTanqueDescripcion;
    @XmlElementWrapper(name = "motivoMovimientoTanques")
    @XmlElement
    private List<MotivoMovimientoTanque> movimientosTanque;
    @XmlElement
    private int estado;
    @XmlElement
    private List<ArchivoAdjunto> archivosAdjuntos;
    @XmlElement
    private String canal;
    @XmlElement
    private String centroCosto;
    @XmlElement
    private String codigoInstalacion;
    @XmlElement
    private String codigoSolicitud;
    @XmlElement
    private String codigoTitular;
    @XmlElement
    private String cuentaContable;
    @XmlElement
    private String descripcionServicio;

    @XmlElement
    private String direccionObra;

    @XmlElement
    private double distanciaHorizontal;

    @XmlElement
    private double distanciaVertical;

    @XmlElementWrapper(name = "equipos")
    @XmlElement
    private List<Equipo> equipo;
    @XmlElement
    private Date fechaEmision;

    @XmlElement
    private Date fechaFinObra;
    @XmlElement
    private Date fechaInicioObra;
    @XmlElement
    private User ingenieroDT;
    @XmlElement
    private String instalacion;
    @XmlElement
    private String justificacionAFI;
    @XmlElement
    private String nombrePlanta;
    @XmlElement
    private String codigoPlanta;
    @XmlElement
    private String numeroAFI;
    @XmlElement
    private String personaContactoNombre;
    @XmlElement
    private String personaContactoTelefono;
    @XmlElement
    private String personaContactoEmail;
    @XmlElement
    private String razonComercial;
    @XmlElement
    private String razonSocial;

    @XmlElement
    private String referencia;
    @XmlElement
    private String rucOdni;
    @XmlElement
    private User supervisorComercial;
    @XmlElementWrapper(name = "tanques")
    @XmlElement
    private List<Tanque> tanque;
    @XmlElement
    private String tipoPartida;
    @XmlElement
    private String tipoSolicitud;
    @XmlElement
    private String titular;

    public String getCentroCosto() {
        return centroCosto;
    }

    public String getCodigoInstalacion() {
        return codigoInstalacion;
    }

    public String getCodigoSolicitud() {
        return codigoSolicitud;
    }

    public String getCodigoTitular() {
        return codigoTitular;
    }

    public String getCuentaContable() {
        return cuentaContable;
    }

    public String getDescripcionServicio() {
        return descripcionServicio;
    }

    public String getDireccionObra() {
        return direccionObra;
    }

    public double getDistanciaHorizontal() {
        return distanciaHorizontal;
    }

    public double getDistanciaVertical() {
        return distanciaVertical;
    }

    public List<Equipo> getEquipo() {
        if (equipo == null) {
            equipo = new ArrayList<Equipo>();
        }

        return equipo;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public Date getFechaFinObra() {
        return fechaFinObra;
    }

    public Date getFechaInicioObra() {
        return fechaInicioObra;
    }

    public String getInstalacion() {
        return instalacion;
    }

    public String getJustificacionAFI() {
        return justificacionAFI;
    }

    public String getNombrePlanta() {
        return nombrePlanta;
    }

    public String getNumeroAFI() {
        return numeroAFI;
    }

    public String getRazonComercial() {
        return razonComercial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public String getReferencia() {
        return referencia;
    }

    public String getRucOdni() {
        return rucOdni;
    }

    public List<Tanque> getTanque() {
        if (tanque == null) {
            tanque = new ArrayList<Tanque>();
        }

        return tanque;
    }

    public String getTipoPartida() {
        return tipoPartida;
    }

    public String getTipoSolicitud() {
        return tipoSolicitud;
    }

    public String getTitular() {
        return titular;
    }

    public void setCentroCosto(String centroCosto) {
        this.centroCosto = centroCosto;
    }

    public void setCodigoInstalacion(String codigoInstalacion) {
        this.codigoInstalacion = codigoInstalacion;
    }

    public void setCodigoSolicitud(String codigoSolicitud) {
        this.codigoSolicitud = codigoSolicitud;
    }

    public void setCodigoTitular(String codigoTitular) {
        this.codigoTitular = codigoTitular;
    }

    public void setCuentaContable(String cuentaContable) {
        this.cuentaContable = cuentaContable;
    }

    public void setDescripcionServicio(String descripcionServicio) {
        this.descripcionServicio = descripcionServicio;
    }

    public void setDireccionObra(String direccionObra) {
        this.direccionObra = direccionObra;
    }

    public void setDistanciaHorizontal(double distanciaHorizontal) {
        this.distanciaHorizontal = distanciaHorizontal;
    }

    public void setDistanciaVertical(double distanciaVertical) {
        this.distanciaVertical = distanciaVertical;
    }

    public void setEquipo(List<Equipo> equipo) {
        this.equipo = equipo;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public void setFechaFinObra(Date fechaFinObra) {
        this.fechaFinObra = fechaFinObra;
    }

    public void setFechaInicioObra(Date fechaInicioObra) {
        this.fechaInicioObra = fechaInicioObra;
    }

    public void setInstalacion(String instalacion) {
        this.instalacion = instalacion;
    }

    public void setJustificacionAFI(String justificacionAFI) {
        this.justificacionAFI = justificacionAFI;
    }

    public void setNombrePlanta(String nombrePlanta) {
        this.nombrePlanta = nombrePlanta;
    }

    public void setNumeroAFI(String numeroAFI) {
        this.numeroAFI = numeroAFI;
    }

    public void setRazonComercial(String razonComercial) {
        this.razonComercial = razonComercial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public void setRucOdni(String rucOdni) {
        this.rucOdni = rucOdni;
    }

    public void setTanque(List<Tanque> tanque) {
        this.tanque = tanque;
    }

    public void setTipoPartida(String tipoPartida) {
        this.tipoPartida = tipoPartida;
    }

    public void setTipoSolicitud(String tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public void setSupervisorComercial(User supervisorComercial) {
        this.supervisorComercial = supervisorComercial;
    }

    public User getSupervisorComercial() {
        return supervisorComercial;
    }

    public void setIngenieroDT(User ingenieroDT) {
        this.ingenieroDT = ingenieroDT;
    }

    public User getIngenieroDT() {
        return ingenieroDT;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getCanal() {
        return canal;
    }

    public String getPersonaContactoNombre() {
        return personaContactoNombre;
    }

    public void setPersonaContactoNombre(String personaContactoNombre) {
        this.personaContactoNombre = personaContactoNombre;
    }

    public String getPersonaContactoTelefono() {
        return personaContactoTelefono;
    }

    public void setPersonaContactoTelefono(String personaContactoTelefono) {
        this.personaContactoTelefono = personaContactoTelefono;
    }

    public String getPersonaContactoEmail() {
        return personaContactoEmail;
    }

    public void setPersonaContactoEmail(String personaContactoEmail) {
        this.personaContactoEmail = personaContactoEmail;
    }

    public void setArchivosAdjuntos(List<ArchivoAdjunto> archivosAdjuntos) {
        this.archivosAdjuntos = archivosAdjuntos;
    }

    public List<ArchivoAdjunto> getArchivosAdjuntos() {
        if (archivosAdjuntos == null) {
            archivosAdjuntos = new ArrayList<ArchivoAdjunto>();

        }

        return archivosAdjuntos;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getEstado() {
        return estado;
    }

    public void setEecc(String eecc) {
        this.eecc = eecc;
    }

    public String getEecc() {
        return eecc;
    }

    public void setMontoLiquidacion(double montoLiquidacion) {
        this.montoLiquidacion = montoLiquidacion;
    }

    public double getMontoLiquidacion() {
        return montoLiquidacion;
    }

    public void setMotivoMovimientoTanqueDescripcion(String motivoMovimientoTanqueDescripcion) {
        this.motivoMovimientoTanqueDescripcion = motivoMovimientoTanqueDescripcion;
    }

    public String getMotivoMovimientoTanqueDescripcion() {
        return motivoMovimientoTanqueDescripcion;
    }

    public void setMovimientosTanque(List<MotivoMovimientoTanque> movimientosTanque) {
        this.movimientosTanque = movimientosTanque;
    }

    public List<MotivoMovimientoTanque> getMovimientosTanque() {
        if (movimientosTanque == null) {
            movimientosTanque = new ArrayList<MotivoMovimientoTanque>();
        }
        return movimientosTanque;
    }

    public void setCodigoOrdenSoporteTecnico(String codigoOrdenSoporteTecnico) {
        this.codigoOrdenSoporteTecnico = codigoOrdenSoporteTecnico;
    }

    public String getCodigoOrdenSoporteTecnico() {
        return codigoOrdenSoporteTecnico;
    }

    public void setMotivoParaEjecucion(String motivoParaEjecucion) {
        this.motivoParaEjecucion = motivoParaEjecucion;
    }

    public String getMotivoParaEjecucion() {
        return motivoParaEjecucion;
    }

    public void setResponsableEECC(String responsableEECC) {
        this.responsableEECC = responsableEECC;
    }

    public String getResponsableEECC() {
        return responsableEECC;
    }

    public void setCodigoPlanta(String coidgoPlanta) {
        this.codigoPlanta = coidgoPlanta;
    }

    public String getCodigoPlanta() {
        return codigoPlanta;
    }

    public void setMontoPresupuestado(double montoPresupuestado) {
        this.montoPresupuestado = montoPresupuestado;
    }

    public double getMontoPresupuestado() {
        return montoPresupuestado;
    }

    public void setMontoFacturado(double montoFacturado) {
        this.montoFacturado = montoFacturado;
    }

    public double getMontoFacturado() {
        return montoFacturado;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setNumeroGuia(String numeroGuia) {
        this.numeroGuia = numeroGuia;
    }

    public String getNumeroGuia() {
        return numeroGuia;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setLiquidaciones(List<LiquidacionItem> liquidaciones) {
        this.liquidaciones = liquidaciones;
    }

    public List<LiquidacionItem> getLiquidaciones() {
        if (liquidaciones == null) {
            liquidaciones = new ArrayList<LiquidacionItem>();
        }
        return liquidaciones;
    }

    public void setMovimientoTanque(boolean movimientoTanque) {
        this.movimientoTanque = movimientoTanque;
    }

    public boolean isMovimientoTanque() {
        return movimientoTanque;
    }

    public void setFechaFinalizacion(Date fechaFinalizacion) {
        this.fechaFinalizacion = fechaFinalizacion;
    }

    public Date getFechaFinalizacion() {
        return fechaFinalizacion;
    }

	public void setMovimientoTanqueComentario(String movimientoTanqueComentario) {
		this.movimientoTanqueComentario = movimientoTanqueComentario;
	}

	public String getMovimientoTanqueComentario() {
		return movimientoTanqueComentario;
	}

}
