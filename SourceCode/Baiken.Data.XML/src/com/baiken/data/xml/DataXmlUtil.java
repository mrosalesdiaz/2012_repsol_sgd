package com.baiken.data.xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class DataXmlUtil {
	public static void release(Object ref, String methodWithNoParameterToCall) {
		// THe object is null
		if (ref != null) {
			try {
				ref.getClass().getMethod(methodWithNoParameterToCall)
						.invoke(ref);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public static Document merge(String expression, File... files)
			throws Exception {
		XPathFactory xPathFactory = XPathFactory.newInstance();
		XPath xpath = xPathFactory.newXPath();
		XPathExpression compiledExpression = xpath.compile(expression);
		return merge(compiledExpression, files);
	}

	public static Document merge(XPathExpression expression, File... files)
			throws Exception {
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
				.newInstance();
		docBuilderFactory.setIgnoringElementContentWhitespace(true);
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document base = docBuilder.parse(files[0]);

		Node results = (Node) expression.evaluate(base, XPathConstants.NODE);
		if (results == null) {
			throw new IOException(files[0]
					+ ": expression does not evaluate to node");
		}

		for (int i = 1; i < files.length; i++) {
			Document merge = docBuilder.parse(files[i]);
			Node nextResults = (Node) expression.evaluate(merge,
					XPathConstants.NODE);
			while (nextResults != null && nextResults.hasChildNodes()) {
				Node kid = nextResults.getFirstChild();
				nextResults.removeChild(kid);
				kid = base.importNode(kid, true);
				results.appendChild(kid);
			}
		}

		return base;
	}
}
