package com.baiken.data.xml.test;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlCDATA;

import com.baiken.data.xml.DTOBase;

@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class User extends DTOBase {
	@XmlElement
	@XmlCDATA
	private String nombreDeUsuario;
	@XmlElement
	private String clave;

	public String getNombreDeUsuario() {
		return nombreDeUsuario;
	}

	public void setNombreDeUsuario(String nombreDeUsuario) {
		this.nombreDeUsuario = nombreDeUsuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

}
