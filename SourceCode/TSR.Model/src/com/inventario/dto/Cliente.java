package com.inventario.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;

import com.baiken.data.xml.DTOBase;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.NONE)
public class Cliente extends DTOBase {
    @XmlElement
    private String razonSocial;
    public static final String RAZON_SOCIAL = "razonSocial";
    @XmlElementWrapper(name = "razonesComerciales")
    @XmlElement
    private List<String> razonComercial;
    public static final String RAZON_COMERCIAL = "razonComercial";

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public List<String> getRazonComercial() {
        return razonComercial;
    }

    public void setRazonComercial(List<String> razonComercial) {
        this.razonComercial = razonComercial;
    }

}
