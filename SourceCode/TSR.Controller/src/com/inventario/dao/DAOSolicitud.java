package com.inventario.dao;

import java.io.File;
import java.io.IOException;

import com.baiken.data.xml.DAOBase;
import com.baiken.data.xml.FileUtil;
import com.inventario.dto.ArchivoAdjunto;
import com.inventario.dto.Canal;
import com.inventario.dto.Solicitud;

public class DAOSolicitud extends DAOBase<Solicitud> {

    public void adjuntarArchivo(String solicitudId, String fileName, File file) {
        File folder = new File(baseFolder, solicitudId);
        if (!folder.exists()) {
            folder.mkdirs();
        }

        FileUtil.copyfile(file, new File(folder, fileName));
    }

    public File obtenerCopiaTemprarlDelArchivo(String description) {
        File archivoTempral;
        try {
            archivoTempral = File.createTempFile("Adjunto", description.substring(description.indexOf(".") , description.length()));
            File localFile = new File(baseFolder, description);
            FileUtil.copyfile(localFile, archivoTempral);
            return archivoTempral;
        } catch (IOException e) {
            throw new RuntimeException("Error creado archivo temporal verificar que exista la variable de entorno %TEMP%", e);
        }
    }
}
