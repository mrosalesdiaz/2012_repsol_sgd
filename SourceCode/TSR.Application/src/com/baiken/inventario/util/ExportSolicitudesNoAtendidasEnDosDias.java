package com.baiken.inventario.util;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baiken.framework.report.excel.ICellBuilder;
import com.inventario.dto.Solicitud;
import com.inventario.dto.tipo.TipoEstadoSolicitud;

public class ExportSolicitudesNoAtendidasEnDosDias implements ICellBuilder {

    private int rowCount;
    private Object[][] data;

    public ExportSolicitudesNoAtendidasEnDosDias(List<Solicitud> listaSolicitudes) {
        List<Solicitud> tempList = new ArrayList<Solicitud>();
        Map<String, Integer> days = new HashMap<String, Integer>();
        for (int i = 0; i < listaSolicitudes.size(); i++) {
            Solicitud solicitud = listaSolicitudes.get(i);

            {
                Calendar currentDate = Calendar.getInstance();

                currentDate.set(Calendar.HOUR, 0);
                currentDate.set(Calendar.MINUTE, 0);

                Calendar emisionCalendar = Calendar.getInstance();
                emisionCalendar.setTime(solicitud.getFechaEmision());
                emisionCalendar.set(Calendar.HOUR, 0);
                emisionCalendar.set(Calendar.MINUTE, 0);
                int day = DateUtil.DateDiffInHours(emisionCalendar, currentDate);

                if (day > 48 && solicitud.getEstado() == TipoEstadoSolicitud.PENDIENTE) {
                    tempList.add(solicitud);
                    days.put(solicitud.getId(), day / 24 + (day % 24 > 0 ? 1 : 0));
                }
            }

        }
        data = new Object[tempList.size()][26];
        for (int i = 0; i < tempList.size(); i++) {
            Solicitud solicitud = tempList.get(i);
            int pos = 0;
            data[i][pos++] = solicitud.getCodigoSolicitud();
            data[i][pos++] = days.get(solicitud.getId());
            data[i][pos++] = solicitud.getEstado() == 0 ? "PENDIENTE" : "ATENDIDA";
            data[i][pos++] = MessageFormat.format("{0,date,yyyy/MM/dd}", solicitud.getFechaEmision());
            data[i][pos++] = solicitud.getTipoSolicitud();
            data[i][pos++] = solicitud.getSupervisorComercial().getFullName();
            data[i][pos++] = solicitud.getIngenieroDT().getFullName();
            data[i][pos++] = solicitud.getCanal();
            data[i][pos++] = solicitud.getRazonSocial();
            data[i][pos++] = solicitud.getRazonComercial();
            data[i][pos++] = solicitud.getRucOdni();
            data[i][pos++] = solicitud.getTitular();
            data[i][pos++] = solicitud.getDireccionObra();
            data[i][pos++] = solicitud.getInstalacion();
            data[i][pos++] = solicitud.getReferencia();
            data[i][pos++] = solicitud.getDescripcionServicio();
            data[i][pos++] = MessageFormat.format("{0,date,yyyy/MM/dd}", solicitud.getFechaInicioObra());
            data[i][pos++] = MessageFormat.format("{0,date,yyyy/MM/dd}", solicitud.getFechaFinObra());
            data[i][pos++] = solicitud.getTipoPartida();
            data[i][pos++] = solicitud.getCentroCosto();
            data[i][pos++] = solicitud.getCuentaContable();
            data[i][pos++] = solicitud.getNumeroAFI();
            data[i][pos++] = solicitud.getJustificacionAFI();
            data[i][pos++] = solicitud.getPersonaContactoNombre();
            data[i][pos++] = solicitud.getPersonaContactoTelefono();
            data[i][pos++] = solicitud.getPersonaContactoEmail();
        }

        rowCount = data.length;
    }

    @Override
    public Object processRow(int row, int col) {
        Object returnValue = "";
        if (data[row][col] != null) {
            returnValue = data[row][col];
        }

        if (returnValue.getClass().equals(Date.class)) {
            returnValue = MessageFormat.format("{0,date,yyyy/MM/dd}", returnValue);
        }
        return returnValue;
    }

    public int getRowCount() {
        return rowCount;
    }

}
