package com.inventario.dao.tipo;

import com.baiken.data.xml.DAOBase;
import com.inventario.dto.Canal;
import com.inventario.dto.tipo.TipoEquipo;
import com.inventario.dto.tipo.TipoEstadoSolicitud;
import com.inventario.dto.tipo.TipoInstalacion;
import com.inventario.dto.tipo.TipoServicio;

public class DAOTipoEstadoSolicitud extends DAOBase<TipoEstadoSolicitud> {

}
