/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventario.view.util;

/**
 *
 * 
 */
public enum Role {

    RoleAdministrador("Administrador"),
    RoleGerenteComercial("Gerente Comercial"),RoleIngenieroDesarrolloTecnico("Ingeniero Desarrollo Tecnico");
    
    public final String text;
    Role(String text) {
        this.text = text;
    }
}
