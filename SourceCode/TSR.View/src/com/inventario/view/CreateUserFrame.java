/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * CreateUser.java
 *
 * Created on Feb 3, 2012, 11:18:54 PM
 */
package com.inventario.view;

import com.inventario.view.util.Role;
import com.inventario.view.util.FormUtil;
import java.util.ResourceBundle;

/**
 *
 *
 */
public class CreateUserFrame extends javax.swing.JFrame {

    private ResourceBundle bundle = ResourceBundle.getBundle("com/inventario/view/message/LocalizedMessages");
    public String userId;

    /** Creates new form CreateUser */
    public CreateUserFrame() {
        initComponents();

        cbRole.removeAllItems();

        for (Role role : Role.values()) {
            cbRole.addItem(role.text);
        }
        
        clearUI();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        tfLogin = new javax.swing.JTextField();
        tfFullName = new javax.swing.JTextField();
        pfPassword = new javax.swing.JPasswordField();
        cbRole = new javax.swing.JComboBox();
        bSave = new javax.swing.JButton();
        lErrorMessage = new javax.swing.JLabel();
        tfEmail = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("com/inventario/view/message/LocalizedMessages"); // NOI18N
        setTitle(bundle.getString("message.applicationTitle")); // NOI18N
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText(bundle.getString("message.login")); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, 342, -1));

        jLabel2.setText(bundle.getString("message.fullName")); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 342, -1));

        jLabel3.setText(bundle.getString("message.role")); // NOI18N
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 160, -1, -1));

        jLabel4.setText(bundle.getString("message.password")); // NOI18N
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 342, -1));

        tfLogin.setText("jTextField1");
        tfLogin.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfLoginFocusLost(evt);
            }
        });
        getContentPane().add(tfLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 31, 342, -1));

        tfFullName.setText("jTextField2");
        tfFullName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfFullNameFocusLost(evt);
            }
        });
        getContentPane().add(tfFullName, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, 342, -1));

        pfPassword.setText("jPasswordField1");
        pfPassword.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                pfPasswordFocusLost(evt);
            }
        });
        getContentPane().add(pfPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 230, 342, -1));

        cbRole.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Gerente Comercial", "Ingeniero Dearrollo Tecnico" }));
        getContentPane().add(cbRole, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 180, 342, -1));

        bSave.setText(bundle.getString("message.save")); // NOI18N
        getContentPane().add(bSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 260, -1, -1));

        lErrorMessage.setForeground(java.awt.Color.red);
        lErrorMessage.setText("jLabel3");
        lErrorMessage.setMaximumSize(new java.awt.Dimension(250, 14));
        getContentPane().add(lErrorMessage, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 260, -1, -1));

        tfEmail.setText("jTextField2");
        tfEmail.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfEmailFocusLost(evt);
            }
        });
        getContentPane().add(tfEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 342, -1));

        jLabel5.setText(bundle.getString("message.email")); // NOI18N
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 342, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tfLoginFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfLoginFocusLost
        String value = tfLogin.getText();
        if (value.trim().equals("")) {
            FormUtil.showMessage(tfLogin, bundle.getString("message.userNameIsRequired"), lErrorMessage, bSave);
        } else {
            FormUtil.showMessage(tfLogin, null, lErrorMessage, bSave);
        }
    }//GEN-LAST:event_tfLoginFocusLost

    private void tfFullNameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfFullNameFocusLost
        String value = tfFullName.getText();
        if (value.trim().equals("")) {
            FormUtil.showMessage(tfFullName, bundle.getString("message.fullNameIsRequired"), lErrorMessage, bSave);
        } else {
            FormUtil.showMessage(tfFullName, null, lErrorMessage, bSave);
        }
    }//GEN-LAST:event_tfFullNameFocusLost

    private void pfPasswordFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pfPasswordFocusLost
        String value = pfPassword.getText();
        if (value.trim().equals("")) {
            FormUtil.showMessage(pfPassword, bundle.getString("message.passwordIsRequired"), lErrorMessage, bSave);
        } else {
            FormUtil.showMessage(pfPassword, null, lErrorMessage, bSave);
        }
    }//GEN-LAST:event_pfPasswordFocusLost

    private void tfEmailFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfEmailFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_tfEmailFocusLost

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CreateUserFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CreateUserFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CreateUserFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CreateUserFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new CreateUserFrame().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton bSave;
    public javax.swing.JComboBox cbRole;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    public javax.swing.JLabel lErrorMessage;
    public javax.swing.JPasswordField pfPassword;
    public javax.swing.JTextField tfEmail;
    public javax.swing.JTextField tfFullName;
    public javax.swing.JTextField tfLogin;
    // End of variables declaration//GEN-END:variables

    public void clearUI() {
        tfLogin.setText("");
        tfFullName.setText("");
        pfPassword.setText("");
        cbRole.setSelectedIndex(0);
        lErrorMessage.setText("");
        userId = null;
        tfEmail.setText("");
    }
}
