package com.baiken.framework.report.excel;

public interface ICellBuilder {

	Object processRow(int row, int col);
}
