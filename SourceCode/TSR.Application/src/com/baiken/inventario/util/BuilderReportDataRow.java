package com.baiken.inventario.util;

import java.util.Calendar;

public class BuilderReportDataRow {
    private String rowHeader;

    private double enero;
    private double febrero;
    private double marzo;
    private double abril;
    private double mayo;
    private double junio;
    private double julio;
    private double agosto;
    private double septiembre;
    private double octubre;
    private double noviembre;
    private double diciembre;

    public String getRowHeader() {
        return rowHeader;
    }

    public void setRowHeader(String rowHeader) {
        this.rowHeader = rowHeader;
    }

    public double getEnero() {
        return enero;
    }

    public void setEnero(double enero) {
        this.enero = enero;
    }

    public double getFebrero() {
        return febrero;
    }

    public void setFebrero(double febrero) {
        this.febrero = febrero;
    }

    public double getMarzo() {
        return marzo;
    }

    public void setMarzo(double marzo) {
        this.marzo = marzo;
    }

    public double getAbril() {
        return abril;
    }

    public void setAbril(double abril) {
        this.abril = abril;
    }

    public double getMayo() {
        return mayo;
    }

    public void setMayo(double mayo) {
        this.mayo = mayo;
    }

    public double getJunio() {
        return junio;
    }

    public void setJunio(double junio) {
        this.junio = junio;
    }

    public double getJulio() {
        return julio;
    }

    public void setJulio(double julio) {
        this.julio = julio;
    }

    public double getAgosto() {
        return agosto;
    }

    public void setAgosto(double agosto) {
        this.agosto = agosto;
    }

    public double getSeptiembre() {
        return septiembre;
    }

    public void setSeptiembre(double septiembre) {
        this.septiembre = septiembre;
    }

    public double getOctubre() {
        return octubre;
    }

    public void setOctubre(double octubre) {
        this.octubre = octubre;
    }

    public double getNoviembre() {
        return noviembre;
    }

    public void setNoviembre(double noviembre) {
        this.noviembre = noviembre;
    }

    public double getDiciembre() {
        return diciembre;
    }

    public void setDiciembre(double diciembre) {
        this.diciembre = diciembre;
    }

    public void add(int january, double i) {
        switch (january) {
        case Calendar.JANUARY:
            enero += i;
            break;
        case Calendar.FEBRUARY:
            febrero += i;
            break;
        case Calendar.MARCH:
            marzo += i;
            break;
        case Calendar.APRIL:
            abril += i;
            break;
        case Calendar.MAY:
            mayo += i;
            break;
        case Calendar.JUNE:
            junio += i;
            break;
        case Calendar.JULY:
            julio += i;
            break;
        case Calendar.AUGUST:
            agosto += i;
            break;
        case Calendar.SEPTEMBER:
            septiembre += i;
            break;
        case Calendar.OCTOBER:
            octubre += i;
            break;
        case Calendar.NOVEMBER:
            noviembre += i;
            break;
        case Calendar.DECEMBER:
            diciembre += i;
            break;

        default:
            break;
        }

    }

    public Object[] toArrayObject() {
        return new Object[] { rowHeader, enero, febrero, marzo, abril, mayo, junio, julio, agosto, septiembre, octubre, noviembre, diciembre };
    }
}
