package com.baiken.framework.report.excel;

import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * The class implementes <b>IReporteableClass</b> to support b  
 *
 */
public interface IReporteableClass {

	Set<String> getVariablesName();

	<Type> void setVariable(String variableName, Type value);

	void addRowsBelow(int rowIndex, int numberOfRows, ICellBuilder builder);

	void save(File file);

	void setTemplate(File file);
	<Type> void setFormula(String variableName, int newRows);

}
