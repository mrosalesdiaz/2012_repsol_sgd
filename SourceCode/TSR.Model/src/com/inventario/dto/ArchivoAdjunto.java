package com.inventario.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;

import com.baiken.data.xml.DTOBase;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.NONE)
public class ArchivoAdjunto extends DTOBase {
    @XmlElement
    private Date fecha;
    public static final String FECHA = "fecha";

    @XmlElement
    private String titulo;
    public static final String TITULO = "titulo";
    

    @XmlElement
    private String fileName;
    public static final String FILENAME = "fileName";

    @XmlElement
    private List<Planta> centrosDeCosto;

    public void setCentrosDeCosto(List<Planta> centrosDeCosto) {
        this.centrosDeCosto = centrosDeCosto;
    }

    public List<Planta> getCentrosDeCosto() {
        return centrosDeCosto;
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTitulo() {
        return titulo;
    }

}
