package com.inventario.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;

import com.baiken.data.xml.DTOBase;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.NONE)
public class Canal extends DTOBase {
    @XmlElement
    private String nombre;
    public static final String NOMBRE = "nombre";

    
    @XmlElement
    private List<Planta> plantas;

    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPlantas(List<Planta> centrosDeCosto) {
        this.plantas = centrosDeCosto;
    }

    public List<Planta> getPlantas() {
        return plantas;
    }

}
