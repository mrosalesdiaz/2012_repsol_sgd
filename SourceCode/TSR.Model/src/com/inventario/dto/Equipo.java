package com.inventario.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.baiken.data.xml.DTOBase;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.NONE)
public class Equipo extends DTOBase{
    public static final String CANTIDAD = "cantidad";
    public static final String EQUIPO = "equipo";
    public static final String MARCA = "marca";
    public static final String MODELO = "modelo";
    
    @XmlElement
    private int cantidad;
    @XmlElement
    private String equipo;
    @XmlElement
    private String marca;
    @XmlElement
    private String modelo;
    public String getEquipo() {
        return equipo;
    }
    public void setEquipo(String equipo) {
        this.equipo = equipo;
    }
    public String getMarca() {
        return marca;
    }
    public void setMarca(String marca) {
        this.marca = marca;
    }
    public String getModelo() {
        return modelo;
    }
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    public int getCantidad() {
        return cantidad;
    }

}
