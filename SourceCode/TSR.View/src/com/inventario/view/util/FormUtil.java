/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventario.view.util;

import java.awt.Color;
import java.awt.Toolkit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.text.JTextComponent;

/**
 *
 * 
 */
public class FormUtil {

    public static void validateEmptyTextField(JFrame parent, JTextComponent textComponent, JLabel errorLabel, JComponent... components) {
        boolean empty = textComponent.getText().trim().equals("");
        if (empty) {
            errorLabel.setText("No puede estar vacio");
            textComponent.setBorder(BorderFactory.createLineBorder(Color.red));
            Toolkit.getDefaultToolkit().beep();
        } else {
            errorLabel.setText("");
            textComponent.setBorder((new JTextField()).getBorder());
        }
        for (JComponent jComponent : components) {
            jComponent.setEnabled(!empty);
        }

        if (parent != null) {
            parent.pack();
        }
    }

    public static void showMessage(JComponent component, String message, JLabel errorLabel, JComponent... components) {
        boolean showMessage = (message != null);

        if (showMessage) {
            errorLabel.setText(message);
            Border border = BorderFactory.createLineBorder(Color.red);


            component.setBorder(border);
            component.getInsets().top = 6;
            component.getInsets().bottom = 6;
            component.getInsets().left = 6;
            component.getInsets().right = 6;

            Toolkit.getDefaultToolkit().beep();
        } else {
            try {

                errorLabel.setText("");
                component.setBorder(((JComponent) component.getClass().newInstance()).getBorder());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (components != null) {

            for (JComponent jComponent : components) {
                if (jComponent != null) {
                    jComponent.setEnabled(!showMessage);
                }
            }
        }

    }
}
