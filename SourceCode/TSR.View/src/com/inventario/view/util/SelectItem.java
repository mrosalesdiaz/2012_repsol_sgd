/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventario.view.util;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * 
 */
public class SelectItem {

    private Object data;
    private Field toStringField;

    public SelectItem(Object data, String field) {
        this.data = data;
        try {
            toStringField = data.getClass().getDeclaredField(field);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public String toString() {
        try {
            toStringField.setAccessible(true);
            String returnValue=toStringField.get(data).toString();
            toStringField.setAccessible(false);
            return returnValue;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * @return the data
     */
    public Object getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(Object data) {
        this.data = data;
    }
}
