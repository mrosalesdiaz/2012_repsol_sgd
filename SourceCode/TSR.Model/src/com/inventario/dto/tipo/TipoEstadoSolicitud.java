package com.inventario.dto.tipo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.baiken.data.xml.DTOBase;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.NONE)
public class TipoEstadoSolicitud extends DTOBase {
    public static final int PENDIENTE=0;
    public static final int EJECUCION=1;
    public static final int FINALIZADA=2;
    
    @XmlElement
    private String nombre;
    public static String NOMBRE = "nombre";

    @XmlElement
    private int index;
    public static String INDEX = "index";

    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
