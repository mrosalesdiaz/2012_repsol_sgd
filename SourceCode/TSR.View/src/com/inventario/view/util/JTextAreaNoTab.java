/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventario.view.util;

import java.awt.event.KeyEvent;
import javax.swing.JComponent;
import javax.swing.JTextArea;
import javax.swing.text.JTextComponent;

/**
 *
 * 
 */
public class JTextAreaNoTab extends JTextArea {

    public JTextAreaNoTab() {
    }

    protected void processComponentKeyEvent(KeyEvent e) {
        if (e.getID() == KeyEvent.KEY_PRESSED
                && e.getKeyCode() == KeyEvent.VK_TAB) {
            e.consume();

            if (e.isShiftDown()) {
                transferFocusBackward();
            } else if (!e.isShiftDown()) {

                transferFocus();
            }
        } else {
            super.processComponentKeyEvent(e);
        }
    }
}
