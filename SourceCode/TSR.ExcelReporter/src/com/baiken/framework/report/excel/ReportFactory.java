package com.baiken.framework.report.excel;

import java.io.File;

public final class ReportFactory {
	public static IReporteableClass createReporteable(File templateFile) {

		IReporteableClass returnValue = (IReporteableClass) new SimpleReporter(templateFile);
		return returnValue;

	}
}
