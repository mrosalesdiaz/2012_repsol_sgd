package com.baiken.inventario;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.Vector;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;

import sun.font.GraphicComponent;

import com.baiken.inventario.util.BuildReportConteoSolicitudesPorGerenteComercial;
import com.inventario.dao.DAOSolicitud;
import com.inventario.dao.tipo.DAOTipoEstadoSolicitud;
import com.inventario.dto.ArchivoAdjunto;
import com.inventario.dto.Canal;
import com.inventario.dto.Planta;
import com.inventario.dto.Cliente;
import com.inventario.dto.Contacto;
import com.inventario.dto.CuentaContable;
import com.inventario.dto.Eecc;
import com.inventario.dto.Equipo;
import com.inventario.dto.LiquidacionItem;
import com.inventario.dto.MotivoEjecucion;
import com.inventario.dto.MotivoMovimientoTanque;
import com.inventario.dto.Partida;
import com.inventario.dto.Solicitud;
import com.inventario.dto.Tanque;
import com.inventario.dto.User;
import com.inventario.dto.tipo.TipoEquipo;
import com.inventario.dto.tipo.TipoEstadoSolicitud;
import com.inventario.dto.tipo.TipoInstalacion;
import com.inventario.dto.tipo.TipoMarca;
import com.inventario.dto.tipo.TipoOrientacion;
import com.inventario.dto.tipo.TipoServicio;
import com.inventario.view.AgregarEquipoFrame;
import com.inventario.view.AgregarMoviemientoTanqueFrame;
import com.inventario.view.AgregarTanqueFrame;
import com.inventario.view.CreateOrderFrame;
import com.inventario.view.CreateUserFrame;
import com.inventario.view.FinalizarSolicitudFrame;
import com.inventario.view.HomeFrame;
import com.inventario.view.LoginFrame;
import com.inventario.view.ReportesFrame;
import com.inventario.view.VisualizeSolicitudesFrame;
import com.inventario.view.VisualizeUsers;
import com.inventario.view.util.BeanTableCellRenderer;
import com.inventario.view.util.BeanTableModel;
import com.inventario.view.util.FormUtil;
import com.inventario.view.util.Role;
import com.inventario.view.util.SelectItem;
import com.sun.net.ssl.internal.www.protocol.https.Handler;

public class ApplicationContext {

	public static Logger logger = Logger.getLogger(ApplicationContext.class
			.getName());

	private ResourceBundle bundle = ResourceBundle
			.getBundle("com/inventario/view/message/LocalizedMessages"); // NOI18N

	private Window currentFrame;

	private LoginFrame loginFrame;
	private HomeFrame homeFrame;
	private VisualizeUsers visualizeUsers;
	private CreateUserFrame createUserFrame;
	private VisualizeSolicitudesFrame visualizeSolicitudesFrame;

	private CreateOrderFrame createOrderFrame;
	private AgregarEquipoFrame agregarEquipoFrame;
	private AgregarTanqueFrame agregarTanqueFrame;
	private AgregarMoviemientoTanqueFrame agregarMoviemientoTanqueFrame;
	private ReportesFrame reportesFrame;
	private FinalizarSolicitudFrame finalizarSolicitudFrame;

	private DataContext globalDataContext;

	private Thread waitScreen;
	private boolean isWaiting = false;

	private boolean createFrameLoaded = false;

	public ApplicationContext() {
		waitScreen = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						if (isWaiting) {
							homeFrame.getGlassPane().setBackground(Color.red);
							homeFrame.setCursor(new Cursor(Cursor.WAIT_CURSOR));
						} else {
							homeFrame.getGlassPane().setBackground(
									new Color(Color.TRANSLUCENT));
							homeFrame.setCursor(new Cursor(
									Cursor.DEFAULT_CURSOR));
						}

						Thread.currentThread().sleep(200);
					} catch (Exception e) {
					}
				}
			}
		});

		waitScreen.start();
		isWaiting = false;

		logger.fine("Initialization of DataContext");
		this.globalDataContext = new DataContext();

		{// Initializing frames
			logger.fine("Loading Login Frame");
			initilizeLoginFrame();
			logger.fine("Loading FinalizarSolicitudes Frame");
			initilizeFinalizarSolicitudFrame();
			logger.fine("Loading Home Frame");
			initializeHomeFrame();
			logger.fine("Loading VisualizarSolicitudes Frame");
			initializeVisualizarSolicitudes();
			logger.fine("Loading CreateOrder Frame");
			initializeCreateOrder();
			logger.fine("Loading Agregar Equipo Frame");
			initializeAgregarEquipoFrame();
			logger.fine("Loading AgregarTanque Frame");
			initializeAgregarTanque();
			logger.fine("Loading VisualizarUser Frame");
			initializeVisualizeUsersFrame();
			logger.fine("Loading CreateUser Frame");
			initalizeCreateUserFrame();
			logger.fine("Loading Reportes Frame");
			initalizeReportesFrame();
			logger.fine("Loading MotivoMovimiento Frame");
			initializeAgregarMotivoMovimientoTanque();
		}
	}

	private void initilizeFinalizarSolicitudFrame() {
		finalizarSolicitudFrame = new FinalizarSolicitudFrame();
		finalizarSolicitudFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		finalizarSolicitudFrame.bAgregarLiquidacion.setCursor(new Cursor(
				Cursor.HAND_CURSOR));
		finalizarSolicitudFrame.bAgregarLiquidacion
				.addMouseListener(new MouseAdapter() {

					@Override
					public void mouseClicked(MouseEvent e) {
						LiquidacionItem item = new LiquidacionItem();
						item.setCantidad((Integer) finalizarSolicitudFrame.sCantidad
								.getValue());
						item.setTipo(finalizarSolicitudFrame.cbTipoMaterial
								.getSelectedItem().toString());
						item.setDescripcion(finalizarSolicitudFrame.tfDescripcion
								.getText());
						item.setMonto((Double) finalizarSolicitudFrame.sMonto
								.getValue());

						globalDataContext.solicitud.getLiquidaciones()
								.add(item);
						updateTableLiquidacion(globalDataContext.solicitud
								.getLiquidaciones());

						finalizarSolicitudFrame.clearUI();
						super.mouseClicked(e);
					}

				});

		finalizarSolicitudFrame.bEliminarLiquidacion.setCursor(new Cursor(
				Cursor.HAND_CURSOR));
		finalizarSolicitudFrame.bEliminarLiquidacion
				.addMouseListener(new MouseAdapter() {

					@Override
					public void mouseClicked(MouseEvent e) {
						JTable jTable = finalizarSolicitudFrame.tLiquidacion;

						if (jTable.getRowCount() > 0) {
							if (jTable.getSelectedRow() != -1) {
								LiquidacionItem item = (LiquidacionItem) jTable
										.getModel().getValueAt(
												jTable.getSelectedRow(), 0);
								globalDataContext.solicitud.getLiquidaciones()
										.remove(item);

								updateTableLiquidacion(globalDataContext.solicitud
										.getLiquidaciones());
							}
						}
						super.mouseClicked(e);
					}

				});

		finalizarSolicitudFrame.bFinalizarSolicitud
				.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						globalDataContext.solicitud
								.setEstado(TipoEstadoSolicitud.FINALIZADA);

						if (globalDataContext.solicitud.getFechaFinalizacion() == null) {
							globalDataContext.solicitud
									.setFechaFinalizacion(Calendar
											.getInstance().getTime());
						}
						globalDataContext
								.guadarSolicitud(globalDataContext.solicitud);

						globalDataContext.solicitud = null;

						hideCurrentForm();
						globalDataContext
								.loadListaDeSolicitudes(buildXPathQuery());
						updateTableSolicitud(globalDataContext.listaDeSolicitudes);

					}

				});
	}

	private void initalizeReportesFrame() {
		reportesFrame = new ReportesFrame();
		reportesFrame.bExportarSolicitudesSupervisorComercial
				.setAction(new AbstractAction() {

					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							File file = File
									.createTempFile("Reportes", ".xlsx");

							globalDataContext
									.exportarSolicitudesDeGerenteComercial(file);

						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				});
		reportesFrame.bExportarSolicitudesSupervisorComercial.setText(bundle
				.getString("message.exportarSolicitudesComercial"));

		reportesFrame.bExportarOrdenesIngenieroDT
				.setAction(new AbstractAction() {

					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							File file = File
									.createTempFile("Reportes", ".xlsx");

							globalDataContext
									.exportarOrdenesDeIngenieroDT(file);

						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				});
		reportesFrame.bExportarOrdenesIngenieroDT.setText(bundle
				.getString("message.exportarOrdenesIngenieroDt"));

		reportesFrame.bExportarSolicitudesPorVencer
				.setAction(new AbstractAction() {

					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							File file = File
									.createTempFile("Reportes", ".xlsx");

							globalDataContext
									.exportarSolicitudesPorVencer(file);

						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				});
		reportesFrame.bExportarSolicitudesPorVencer
				.setText(bundle
						.getString("message.exportarSolicitudesPendientesMasDeDosDias"));
	}

	private void initalizeCreateUserFrame() {
		{// CREATE USER
			createUserFrame = new CreateUserFrame();
			createUserFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			createUserFrame.bSave.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					boolean editMode = createUserFrame.userId != null;

					User user = new User();

					user.setId(editMode ? createUserFrame.userId : UUID
							.randomUUID().toString());

					user.setUserName(createUserFrame.tfLogin.getText());
					user.setFullName(createUserFrame.tfFullName.getText());
					user.setEmail(createUserFrame.tfEmail.getText());
					user.setRole(createUserFrame.cbRole.getSelectedItem()
							.toString());
					user.setPassword(new String(createUserFrame.pfPassword
							.getPassword()));
					if (editMode) {
						globalDataContext.guardarUsuario(user);
						globalDataContext.loadListaDeUsuarios();
						updateTableUsers(globalDataContext.listaDeUsuarios);
						currentFrame.setVisible(false);
						createUserFrame.clearUI();
						createUserFrame.tfLogin.setEditable(true);
					} else {
						if (globalDataContext.validateUserName(user
								.getUserName())) {
							globalDataContext.guardarUsuario(user);
							globalDataContext.loadListaDeUsuarios();
							updateTableUsers(globalDataContext.listaDeUsuarios);
							currentFrame.setVisible(false);
							createUserFrame.clearUI();
						} else {
							FormUtil.showMessage(
									createUserFrame.tfLogin,
									bundle.getString("message.userNameAlreadyExists"),
									createUserFrame.lErrorMessage, null);
						}
					}
				}
			});
		}
	}

	private void initializeVisualizeUsersFrame() {
		{// VISUALIZE USERS
			visualizeUsers = new VisualizeUsers();
			visualizeUsers.bCreateUser.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					showDialog(createUserFrame);
					createUserFrame.clearUI();
				}
			});
			visualizeUsers.bEditUser.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					int selectedRow = visualizeUsers.tUsers.getSelectedRow();
					if (selectedRow != -1) {
						createUserFrame.clearUI();
						User user = (User) visualizeUsers.tUsers.getModel()
								.getValueAt(selectedRow, -1);
						createUserFrame.userId = user.getId();
						createUserFrame.tfLogin.setText(user.getUserName());
						createUserFrame.tfFullName.setText(user.getFullName());
						createUserFrame.pfPassword.setText(user.getPassword());
						createUserFrame.tfEmail.setText(user.getEmail());
						createUserFrame.cbRole.setSelectedItem(user.getRole());
						createUserFrame.tfLogin.setEditable(false);
						showDialog(createUserFrame);
						createUserFrame.clearUI();

						globalDataContext.loadListaDeUsuarios();
						updateTableUsers(globalDataContext.listaDeUsuarios);
					} else {
						JOptionPane.showMessageDialog(
								currentFrame,
								bundle.getString("mensage.seleccionarUnElementoDeLaLista"),
								bundle.getString("message.applicationTitle"),
								JOptionPane.WARNING_MESSAGE);
					}

				}
			});
			visualizeUsers.tUsers.getSelectionModel().addListSelectionListener(
					new ListSelectionListener() {

						@Override
						public void valueChanged(ListSelectionEvent arg0) {
							int selectedRow = visualizeUsers.tUsers
									.getSelectedRow();
							if (selectedRow != -1) {

								User user = (User) visualizeUsers.tUsers
										.getModel().getValueAt(selectedRow, -1);
								String pageContent = MessageFormat.format(
										bundle.getString("message.userPreviewTemplate"),
										user.getUserName(), user.getFullName(),
										user.getRole());

								visualizeUsers.tpPreviewUser
										.setText(pageContent);
							}

						}
					});
			visualizeUsers.bRemoveUser.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					int confirmDialogResult = JOptionPane.showConfirmDialog(
							currentFrame,
							bundle.getString("message.confirmRemoveUser"),
							bundle.getString("message.applicationTitle"),
							JOptionPane.YES_NO_OPTION,
							JOptionPane.WARNING_MESSAGE);
					int selectedRow = visualizeUsers.tUsers.getSelectedRow();

					if (selectedRow != -1) {

						if (confirmDialogResult == JOptionPane.YES_OPTION) {
							User user = (User) visualizeUsers.tUsers.getModel()
									.getValueAt(selectedRow, -1);

							globalDataContext.removeUsuario(user.getId());
							globalDataContext.loadListaDeUsuarios();
							updateTableUsers(globalDataContext.listaDeUsuarios);

						}

					} else {
						JOptionPane.showMessageDialog(
								currentFrame,
								bundle.getString("mensage.seleccionarUnElementoDeLaLista"),
								bundle.getString("message.applicationTitle"),
								JOptionPane.WARNING_MESSAGE);
					}
				}
			});
		}
	}

	private void initializeAgregarTanque() {
		{
			agregarTanqueFrame = new AgregarTanqueFrame();
			{ // Llenando los combos de esta pantalla ( solo una vez al
				// iniciar)
				agregarTanqueFrame.tipoInstalacionCombobox.removeAllItems();

				globalDataContext.loadListaDeTipoInstalacion();

				for (TipoInstalacion tipoInstalacion : globalDataContext.listaDeTipoInstalacion) {
					agregarTanqueFrame.tipoInstalacionCombobox
							.addItem(tipoInstalacion.getNombre());
				}

			}
			{
				agregarTanqueFrame.tipoOrientacionComboBox.removeAllItems();
				globalDataContext.loadListaDeTipoOrientacion();
				for (TipoOrientacion tipoOrientacion : globalDataContext.listaDeTipoOrientacion) {
					agregarTanqueFrame.tipoOrientacionComboBox
							.addItem(tipoOrientacion.getNombre());
				}
			}
			agregarTanqueFrame.bAgregar.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					Tanque tanque = new Tanque();
					tanque.setCantidad((Integer) agregarTanqueFrame.cantidadSpinner
							.getValue());
					tanque.setCapacidad((Double) agregarTanqueFrame.capacidadSpinner
							.getValue());
					tanque.setOrientacion(agregarTanqueFrame.tipoOrientacionComboBox
							.getSelectedItem().toString());
					tanque.setInstalacion(agregarTanqueFrame.tipoInstalacionCombobox
							.getSelectedItem().toString());

					globalDataContext.solicitud.getTanque().add(tanque);
					updateTableTanques(globalDataContext.solicitud.getTanque());
					agregarTanqueFrame.clearUI();
				}
			});
			agregarTanqueFrame.bCerrar.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					hideCurrentForm();
				}
			});
		}
	}

	private void initializeAgregarMotivoMovimientoTanque() {
		{
			agregarMoviemientoTanqueFrame = new AgregarMoviemientoTanqueFrame();
			agregarMoviemientoTanqueFrame.bAgregar
					.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {
							MotivoMovimientoTanque motivoMovimientoTanque = new MotivoMovimientoTanque();

							motivoMovimientoTanque
									.setInstalacionReal(agregarMoviemientoTanqueFrame.instalacionReal
											.getText());
							motivoMovimientoTanque
									.setDescripcion(agregarMoviemientoTanqueFrame.descripcion
											.getText());
							motivoMovimientoTanque
									.setNumeroGrafo(agregarMoviemientoTanqueFrame.numeroGrafo
											.getText());
							motivoMovimientoTanque
									.setNumeroReserva(agregarMoviemientoTanqueFrame.numeroReserva
											.getText());
							motivoMovimientoTanque
									.setSerieCodigo(agregarMoviemientoTanqueFrame.serieCodigo
											.getText());

							motivoMovimientoTanque
									.setCantidad((Integer) agregarMoviemientoTanqueFrame.cantidad
											.getValue());
							motivoMovimientoTanque
									.setCapacidad((Double) agregarMoviemientoTanqueFrame.capacidad
											.getValue());

							globalDataContext.solicitud.getMovimientosTanque()
									.add(motivoMovimientoTanque);
							updateTableMovimientoTanques(globalDataContext.solicitud
									.getMovimientosTanque());
							agregarMoviemientoTanqueFrame.clearUI();
						}

					});
			agregarMoviemientoTanqueFrame.bCerrar
					.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							hideCurrentForm();
						}
					});
		}
	}

	private void initializeAgregarEquipoFrame() {
		{
			agregarEquipoFrame = new AgregarEquipoFrame();
			{ // Llenando los combos de esta pantalla ( solo una vez al
				// iniciar)
				agregarEquipoFrame.tipoEquipoComboBox.removeAllItems();

				globalDataContext.loadListaDeTipoEquipo();

				for (TipoEquipo tipoEquipo : globalDataContext.listaDeTipoEquipo) {
					agregarEquipoFrame.tipoEquipoComboBox.addItem(tipoEquipo
							.getNombre());
				}

			}
			{
				agregarEquipoFrame.tipoMarcaComboBox.removeAllItems();
				globalDataContext.loadListaDeTipoMarca();
				for (TipoMarca tipoMarca : globalDataContext.listaDeTipoMarca) {
					agregarEquipoFrame.tipoMarcaComboBox.addItem(tipoMarca
							.getNombre());
				}
			}
			agregarEquipoFrame.bAgregar.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					Equipo equipo = new Equipo();
					equipo.setCantidad((Integer) agregarEquipoFrame.cantidadSpinner
							.getValue());
					equipo.setEquipo(agregarEquipoFrame.tipoEquipoComboBox
							.getSelectedItem().toString());
					equipo.setMarca(agregarEquipoFrame.tipoMarcaComboBox
							.getSelectedItem().toString());
					equipo.setModelo(agregarEquipoFrame.modeloTextField
							.getText());

					globalDataContext.solicitud.getEquipo().add(equipo);
					updateTableEquipos(globalDataContext.solicitud.getEquipo());
					agregarEquipoFrame.clearUI();
				}
			});
			agregarEquipoFrame.bCerrar.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					hideCurrentForm();
				}
			});
		}
	}

	private void initializeCreateOrder() {
		{// CREAR SOLICITU
			createOrderFrame = new CreateOrderFrame();
			{// set dimesion and position from the current window
				Insets insets = Toolkit.getDefaultToolkit().getScreenInsets(
						createOrderFrame.getGraphicsConfiguration());
				Dimension dimension = Toolkit.getDefaultToolkit()
						.getScreenSize();

				createOrderFrame.setLocation(createOrderFrame.getX(),
						insets.top);
				createOrderFrame
						.setSize(
								createOrderFrame.getWidth(),
								((int) dimension.getHeight() - (insets.top + insets.bottom)));
			}
			createOrderFrame.addWindowListener(new WindowAdapter() {

				@Override
				public void windowOpened(WindowEvent e) {
					super.windowOpened(e);

					createFrameLoaded = false;
					{/*
					 * Informacion a precargar antes de mostrar la pantalla
					 */
						// VOLVER A CARGAR LA LISTA DE CLIENTES
						createOrderFrame.cbRazonSocial.removeAllItems();

						globalDataContext.loadListaDeClientes();
						for (Cliente cliente : globalDataContext.listaDeClientes) {
							createOrderFrame.cbRazonSocial
									.addItem(new SelectItem(cliente,
											Cliente.RAZON_SOCIAL));
						}

						{// recargando los motivos, eecc y responsable eecc
							createOrderFrame.cbNombreEECC.removeAllItems();
							globalDataContext.loadListaDeEECC();

							createOrderFrame.cbResponsableEECC.removeAllItems();
							for (Eecc item : globalDataContext.listaDeEECC) {
								createOrderFrame.cbNombreEECC
										.addItem(new SelectItem(item,
												Eecc.NOMBRE));
								if (createOrderFrame.cbResponsableEECC
										.getItemCount() == 0) {
									for (Contacto responsableEecc : item
											.getResponsables()) {
										createOrderFrame.cbResponsableEECC
												.addItem(new SelectItem(
														responsableEecc,
														Contacto.NOMBRE));
									}
								}
							}

							createOrderFrame.cbMotivoEjecucion.removeAllItems();
							globalDataContext.loadListaDeMotivoEjecucion();
							for (MotivoEjecucion item : globalDataContext.listaDeMotivoEjecucion) {
								createOrderFrame.cbMotivoEjecucion
										.addItem(new SelectItem(item,
												MotivoEjecucion.NOMBRE));
							}
						}
					}
					{// Update textBox planta en caso de q no haya cargado ya
						if (createOrderFrame.tfCentroCosto.getText().trim()
								.equals("")) {
							if (createOrderFrame.cbPlanta.getItemCount() > 0) {
								createOrderFrame.tfCentroCosto
										.setText(((Planta) ((SelectItem) createOrderFrame.cbPlanta
												.getSelectedItem()).getData())
												.getNombreCentroCosto());
							}
						}

					}
					/*
					 * Configurando la pantalla para los ususarios de tipo
					 * GerenteComercial
					 */
					if (globalDataContext.loggedUser.getRole().equals(
							Role.RoleGerenteComercial.text)) {
						boolean esNuevaSolicitud = (globalDataContext.solicitud == null);
						if (esNuevaSolicitud) {
							globalDataContext.solicitud = new Solicitud();

						}

						globalDataContext.solicitud
								.setSupervisorComercial(globalDataContext.loggedUser);

						createOrderFrame.lTituloFrame.setText(bundle
								.getString("message.tituloIngresarSolicitud"));
						createOrderFrame.pEjecucionObra.setVisible(false);
						createOrderFrame.pMovimientoTanque.setVisible(false);
						createOrderFrame.pActivarMovimientoTanque
								.setVisible(false);

						if (globalDataContext.solicitud
								.getSupervisorComercial() != null) {
							createOrderFrame.tfSolicitadoPor
									.setText(globalDataContext.solicitud
											.getSupervisorComercial()
											.getFullName());
							createOrderFrame.tfSupervisorComercial
									.setText(globalDataContext.solicitud
											.getSupervisorComercial()
											.getFullName());
						}
						createOrderFrame.tfRUCoDNI
								.setText(globalDataContext.solicitud
										.getRucOdni());
						createOrderFrame.tfTitular
								.setText(globalDataContext.solicitud
										.getTitular());
						createOrderFrame.tfInstalacion
								.setText(globalDataContext.solicitud
										.getInstalacion());
						createOrderFrame.tfDireccionObra
								.setText(globalDataContext.solicitud
										.getDireccionObra());
						createOrderFrame.tfReferencia
								.setText(globalDataContext.solicitud
										.getReferencia());
						createOrderFrame.taDescripcionServico
								.setText(globalDataContext.solicitud
										.getDescripcionServicio());
						createOrderFrame.tfNumeroProyectoAFI
								.setText(globalDataContext.solicitud
										.getNumeroAFI());
						createOrderFrame.tfJustificacionAFI
								.setText(globalDataContext.solicitud
										.getJustificacionAFI());
						createOrderFrame.tfPersonaContacto
								.setText(globalDataContext.solicitud
										.getPersonaContactoNombre());
						createOrderFrame.tfEmail
								.setText(globalDataContext.solicitud
										.getPersonaContactoEmail());
						createOrderFrame.tfTelefono
								.setText(globalDataContext.solicitud
										.getPersonaContactoTelefono());

						updateTableTanques(globalDataContext.solicitud
								.getTanque());
						updateTableEquipos(globalDataContext.solicitud
								.getEquipo());
						updateTableMovimientoTanques(globalDataContext.solicitud
								.getMovimientosTanque());

						if (globalDataContext.solicitud.getFechaEmision() != null) {
							createOrderFrame.sFechaEmision
									.setValue(globalDataContext.solicitud
											.getFechaEmision());
						}
						if (globalDataContext.solicitud.getFechaInicioObra() != null) {
							createOrderFrame.sFechaInicioObra
									.setValue(globalDataContext.solicitud
											.getFechaInicioObra());
						}
						if (globalDataContext.solicitud.getFechaFinObra() != null) {
							createOrderFrame.sFechaFinObra
									.setValue(globalDataContext.solicitud
											.getFechaFinObra());
						}

						createOrderFrame.sHorizontal
								.setValue(globalDataContext.solicitud
										.getDistanciaHorizontal());
						createOrderFrame.sVertical
								.setValue(globalDataContext.solicitud
										.getDistanciaVertical());

						for (int i = 0; i < createOrderFrame.cbTipoSolicitud
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbTipoSolicitud
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getTipoSolicitud())) {
								createOrderFrame.cbTipoSolicitud
										.setSelectedIndex(i);
							}
						}

						for (int i = 0; i < createOrderFrame.cbCanal
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbCanal
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getCanal())) {
								createOrderFrame.cbCanal.setSelectedIndex(i);
							}
						}

						for (int i = 0; i < createOrderFrame.cbRazonSocial
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbRazonSocial
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getRazonSocial())) {
								createOrderFrame.cbRazonSocial
										.setSelectedIndex(i);
							}
						}

						for (int i = 0; i < createOrderFrame.cbRazonComercial
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbRazonComercial
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getRazonComercial())) {
								createOrderFrame.cbRazonComercial
										.setSelectedIndex(i);
							}
						}
						// COMBO INGENIERO DT
						for (int i = 0; i < createOrderFrame.cbIngenieroDT
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbIngenieroDT
									.getItemAt(i).toString();

							if (globalDataContext.solicitud.getIngenieroDT() != null
									&& itemText
											.equalsIgnoreCase(globalDataContext.solicitud
													.getIngenieroDT()
													.getFullName())) {
								createOrderFrame.cbIngenieroDT
										.setSelectedIndex(i);
							}
						}

						for (int i = 0; i < createOrderFrame.cbTipoPartida
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbTipoPartida
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getTipoPartida())) {
								createOrderFrame.cbTipoPartida
										.setSelectedIndex(i);
							}
						}

						for (int i = 0; i < createOrderFrame.cbPlanta
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbPlanta
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getCentroCosto())) {
								createOrderFrame.cbPlanta.setSelectedIndex(i);

							}
						}

						if (createOrderFrame.cbTipoPartida.getSelectedIndex() != -1) {
							createOrderFrame.tfCuentaContable
									.setText(((Partida) ((SelectItem) createOrderFrame.cbTipoPartida
											.getSelectedItem()).getData())
											.getCuentaContable());
						}

						if (esNuevaSolicitud) {
							globalDataContext.solicitud.setCodigoSolicitud(globalDataContext
									.generarCodigoSoporteTecnico(
											((TipoServicio) ((SelectItem) createOrderFrame.cbTipoSolicitud
													.getSelectedItem())
													.getData()).getInicial(),
											createOrderFrame.tfCentroCosto
													.getText()));

						}
						createOrderFrame.tfNumeroSolicitud
								.setText(globalDataContext.solicitud
										.getCodigoSolicitud());

					}
					/*
					 * Configurando la pantalla para los ususarios de tipo
					 * IngenieroDT
					 */
					else if (globalDataContext.loggedUser.getRole().equals(
							Role.RoleIngenieroDesarrolloTecnico.text)) {
						createOrderFrame.lTituloFrame.setText(bundle
								.getString("message.tituloIngresarOrdenServicio"));
						createOrderFrame.lNumero.setText(bundle
								.getString("message.numeroOrden"));
						globalDataContext.solicitud
								.setIngenieroDT(globalDataContext.loggedUser);

						createOrderFrame.pEjecucionObra.setVisible(true);
						createOrderFrame.pMovimientoTanque.setVisible(false);
						createOrderFrame.pActivarMovimientoTanque
								.setVisible(true);

						createOrderFrame.tfRUCoDNI
								.setText(globalDataContext.solicitud
										.getRucOdni());
						createOrderFrame.tfTitular
								.setText(globalDataContext.solicitud
										.getTitular());
						createOrderFrame.tfInstalacion
								.setText(globalDataContext.solicitud
										.getInstalacion());
						createOrderFrame.tfDireccionObra
								.setText(globalDataContext.solicitud
										.getDireccionObra());
						createOrderFrame.tfReferencia
								.setText(globalDataContext.solicitud
										.getReferencia());
						createOrderFrame.taDescripcionServico
								.setText(globalDataContext.solicitud
										.getDescripcionServicio());
						createOrderFrame.tfNumeroProyectoAFI
								.setText(globalDataContext.solicitud
										.getNumeroAFI());
						createOrderFrame.tfJustificacionAFI
								.setText(globalDataContext.solicitud
										.getJustificacionAFI());
						createOrderFrame.tfPersonaContacto
								.setText(globalDataContext.solicitud
										.getPersonaContactoNombre());
						createOrderFrame.tfEmail
								.setText(globalDataContext.solicitud
										.getPersonaContactoEmail());
						createOrderFrame.tfTelefono
								.setText(globalDataContext.solicitud
										.getPersonaContactoTelefono());

						if (globalDataContext.solicitud
								.getSupervisorComercial() != null) {
							createOrderFrame.tfSolicitadoPor
									.setText(globalDataContext.solicitud
											.getSupervisorComercial()
											.getFullName());
							createOrderFrame.tfSupervisorComercial
									.setText(globalDataContext.solicitud
											.getSupervisorComercial()
											.getFullName());
						}

						updateTableTanques(globalDataContext.solicitud
								.getTanque());
						updateTableEquipos(globalDataContext.solicitud
								.getEquipo());
						updateTableMovimientoTanques(globalDataContext.solicitud
								.getMovimientosTanque());

						if (globalDataContext.solicitud.getMovimientosTanque() != null
								&& globalDataContext.solicitud
										.getMovimientosTanque().size() > 0) {
							createOrderFrame.cbActivarMovimientoTanque
									.setSelected(true);
							createOrderFrame.pMovimientoTanque.setVisible(true);
						} else {
							createOrderFrame.cbActivarMovimientoTanque
									.setSelected(false);
							createOrderFrame.pMovimientoTanque
									.setVisible(false);
						}

						if (globalDataContext.solicitud.getFechaEmision() != null) {
							createOrderFrame.sFechaEmision
									.setValue(globalDataContext.solicitud
											.getFechaEmision());
						}
						if (globalDataContext.solicitud.getFechaInicioObra() != null) {
							createOrderFrame.sFechaInicioObra
									.setValue(globalDataContext.solicitud
											.getFechaInicioObra());
						}
						if (globalDataContext.solicitud.getFechaFinObra() != null) {
							createOrderFrame.sFechaFinObra
									.setValue(globalDataContext.solicitud
											.getFechaFinObra());
						}

						createOrderFrame.sHorizontal
								.setValue(globalDataContext.solicitud
										.getDistanciaHorizontal());
						createOrderFrame.sVertical
								.setValue(globalDataContext.solicitud
										.getDistanciaVertical());

						for (int i = 0; i < createOrderFrame.cbTipoSolicitud
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbTipoSolicitud
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getTipoSolicitud())) {
								createOrderFrame.cbTipoSolicitud
										.setSelectedIndex(i);
							}
						}

						for (int i = 0; i < createOrderFrame.cbCanal
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbCanal
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getCanal())) {
								createOrderFrame.cbCanal.setSelectedIndex(i);
							}
						}

						for (int i = 0; i < createOrderFrame.cbRazonSocial
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbRazonSocial
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getRazonSocial())) {
								createOrderFrame.cbRazonSocial
										.setSelectedIndex(i);
							}
						}

						for (int i = 0; i < createOrderFrame.cbRazonComercial
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbRazonComercial
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getRazonComercial())) {
								createOrderFrame.cbRazonComercial
										.setSelectedIndex(i);
							}
						}
						// COMBO INGENIERO DT
						for (int i = 0; i < createOrderFrame.cbIngenieroDT
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbIngenieroDT
									.getItemAt(i).toString();

							if (globalDataContext.loggedUser != null
									&& itemText
											.equalsIgnoreCase(globalDataContext.loggedUser
													.getFullName())) {
								createOrderFrame.cbIngenieroDT
										.setSelectedIndex(i);
							}
						}

						for (int i = 0; i < createOrderFrame.cbTipoPartida
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbTipoPartida
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getTipoPartida())) {
								createOrderFrame.cbTipoPartida
										.setSelectedIndex(i);
							}
						}

						for (int i = 0; i < createOrderFrame.cbPlanta
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbPlanta
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getCentroCosto())) {
								createOrderFrame.cbPlanta.setSelectedIndex(i);

							}
						}

						/*
						 * Campo llenado solo para ingeniero DT
						 */
						for (int i = 0; i < createOrderFrame.cbMotivoEjecucion
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbMotivoEjecucion
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getMotivoParaEjecucion())) {
								createOrderFrame.cbMotivoEjecucion
										.setSelectedIndex(i);
							}
						}
						/*
						 * Campo llenado solo para ingeniero DT
						 */
						for (int i = 0; i < createOrderFrame.cbNombreEECC
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbNombreEECC
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getEecc())) {
								createOrderFrame.cbNombreEECC
										.setSelectedIndex(i);
							}
						}

						if (createOrderFrame.cbTipoPartida.getSelectedIndex() != -1) {
							createOrderFrame.tfCuentaContable
									.setText(((Partida) ((SelectItem) createOrderFrame.cbTipoPartida
											.getSelectedItem()).getData())
											.getCuentaContable());
						}

						/*
						 * Campo llenado solo para ingeniero DT
						 */
						for (int i = 0; i < createOrderFrame.cbResponsableEECC
								.getItemCount(); i++) {
							String itemText = createOrderFrame.cbResponsableEECC
									.getItemAt(i).toString();

							if (itemText
									.equalsIgnoreCase(globalDataContext.solicitud
											.getResponsableEECC())) {
								createOrderFrame.cbResponsableEECC
										.setSelectedIndex(i);
							}
						}

						if (globalDataContext.solicitud.getEstado() == TipoEstadoSolicitud.PENDIENTE) {

							Planta plantaSeleccionado = (Planta) ((SelectItem) createOrderFrame.cbPlanta
									.getSelectedItem()).getData();
							createOrderFrame.tfCentroCosto
									.setText(plantaSeleccionado
											.getNombreCentroCosto());

						}

						// Al final para evitar q llame a los eventos
						createOrderFrame.tfNumeroSolicitud
								.setText(globalDataContext.solicitud
										.getCodigoSolicitud());

					}

					createFrameLoaded = true;
				}

				@Override
				public void windowClosing(WindowEvent arg0) {
					super.windowClosing(arg0);
					currentFrame.setVisible(false);
				}
			});

			{// Cambiar Responsable EECC
				createOrderFrame.cbNombreEECC.setAction(new AbstractAction() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (createOrderFrame.cbNombreEECC.getItemCount() > 0) {
							Eecc eecc = (Eecc) ((SelectItem) createOrderFrame.cbNombreEECC
									.getSelectedItem()).getData();

							createOrderFrame.cbResponsableEECC.removeAllItems();
							for (Contacto item : eecc.getResponsables()) {
								createOrderFrame.cbResponsableEECC
										.addItem(new SelectItem(item,
												Contacto.NOMBRE));
							}
						} else {
							createOrderFrame.cbResponsableEECC.removeAllItems();
						}

					}
				});

			}

			{// canales
				createOrderFrame.cbCanal.removeAllItems();

				globalDataContext.loadListaDeCanales();
				for (final Canal canal : globalDataContext.listaDeCanales) {
					createOrderFrame.cbCanal.addItem(new SelectItem(canal,
							"nombre"));
				}

			}
			{// Tipo Partida
				createOrderFrame.cbTipoPartida.removeAllItems();

				globalDataContext.loadListaDePartidas();
				for (final Partida item : globalDataContext.listaDePartidas) {
					createOrderFrame.cbTipoPartida.addItem(new SelectItem(item,
							"nombre"));
				}

				createOrderFrame.cbTipoPartida.setAction(new AbstractAction() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						if (createOrderFrame.cbTipoPartida.getItemCount() > 0) {
							if (createOrderFrame.cbTipoPartida
									.getSelectedItem() instanceof SelectItem) {
								Partida partida = (Partida) ((SelectItem) createOrderFrame.cbTipoPartida
										.getSelectedItem()).getData();
								createOrderFrame.tfCuentaContable
										.setText(partida.getCuentaContable());
							}
						}
					}
				});
			}
			{
				updateTableEquipos(new ArrayList<Equipo>());
				updateTableTanques(new ArrayList<Tanque>());
			}
			createOrderFrame.bAgregarEquipo
					.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							super.mouseClicked(e);
							agregarEquipoFrame.clearUI();
							showDialog(agregarEquipoFrame);
						}
					});
			createOrderFrame.bEliminarEquipo
					.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							super.mouseClicked(e);
							JTable jTable = createOrderFrame.tListaEquipos;

							if (jTable.getRowCount() > 0) {
								if (jTable.getSelectedRow() != -1) {
									Equipo equipo = (Equipo) jTable.getModel()
											.getValueAt(
													jTable.getSelectedRow(), 0);
									globalDataContext.solicitud.getEquipo()
											.remove(equipo);

									updateTableEquipos(globalDataContext.solicitud
											.getEquipo());
								}
							}
						}
					});

			createOrderFrame.bAgregarMovimientoTanque
					.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							super.mouseClicked(e);
							agregarEquipoFrame.clearUI();
							showDialog(agregarMoviemientoTanqueFrame);
						}
					});
			createOrderFrame.bEliminarMovimientoTanque
					.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							super.mouseClicked(e);
							JTable jTable = createOrderFrame.tListaMovimientoTanque;

							if (jTable.getRowCount() > 0) {
								if (jTable.getSelectedRow() != -1) {
									MotivoMovimientoTanque equipo = (MotivoMovimientoTanque) jTable
											.getModel().getValueAt(
													jTable.getSelectedRow(), 0);
									globalDataContext.solicitud
											.getMovimientosTanque().remove(
													equipo);

									updateTableMovimientoTanques(globalDataContext.solicitud
											.getMovimientosTanque());
								}
							}
						}
					});
			createOrderFrame.bAgregarTanque
					.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							super.mouseClicked(e);
							agregarEquipoFrame.clearUI();
							showDialog(agregarTanqueFrame);

						}
					});
			createOrderFrame.bEliminarTanque
					.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							super.mouseClicked(e);
							JTable jTable = createOrderFrame.tListaTanques;

							if (jTable.getRowCount() > 0) {
								if (jTable.getSelectedRow() != -1) {
									Tanque tanque = (Tanque) jTable.getModel()
											.getValueAt(
													jTable.getSelectedRow(), 0);
									globalDataContext.solicitud.getTanque()
											.remove(tanque);
									updateTableTanques(globalDataContext.solicitud
											.getTanque());
								}
							}
						}
					});

			createOrderFrame.cbCanal.setAction(new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					if (createOrderFrame.cbCanal.getSelectedItem() instanceof SelectItem) {
						SelectItem selectItem = ((SelectItem) createOrderFrame.cbCanal
								.getSelectedItem());
						if (selectItem.getData() instanceof Canal) {
							Canal canal = (Canal) selectItem.getData();
							createOrderFrame.cbPlanta.removeAllItems();

							if (canal.getPlantas() != null) {

								for (final Planta item : canal.getPlantas()) {
									createOrderFrame.cbPlanta
											.addItem(new SelectItem(item,
													Planta.NOMBREDEPLANTA));
								}
							}
						}
					}
				}
			});
			createOrderFrame.cbPlanta.setAction(new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					if (createOrderFrame.cbPlanta.getItemCount() > 0) {
						if (createOrderFrame.cbPlanta.getSelectedItem() instanceof SelectItem) {
							Planta plantaSeleccionado = (Planta) ((SelectItem) createOrderFrame.cbPlanta
									.getSelectedItem()).getData();
							createOrderFrame.tfCentroCosto
									.setText(plantaSeleccionado
											.getNombreCentroCosto());
							{
								if (createFrameLoaded) {

									if (globalDataContext.solicitud != null
											&& globalDataContext.solicitud
													.getEstado() == TipoEstadoSolicitud.PENDIENTE) {
										globalDataContext.solicitud
												.setCodigoOrdenSoporteTecnico(globalDataContext
														.generarCodigoSoporteTecnico(
																((TipoServicio) ((SelectItem) createOrderFrame.cbTipoSolicitud
																		.getSelectedItem())
																		.getData())
																		.getInicial(),
																plantaSeleccionado
																		.getCodigoDePlanta()));
										createOrderFrame.tfNumeroSolicitud
												.setText(globalDataContext.solicitud
														.getCodigoOrdenSoporteTecnico());
									}
								}
							}
						}
					}

				}
			});
			createOrderFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

			{// Llenando el combo de tipos de solicitud
				createOrderFrame.cbTipoSolicitud.removeAllItems();

				globalDataContext.loadListaDeTipoSolicitudes();
				for (TipoServicio tipoServicio : globalDataContext.listaDeTipoSolicitud) {
					createOrderFrame.cbTipoSolicitud.addItem(new SelectItem(
							tipoServicio, TipoServicio.NOMBRE));
				}

				createOrderFrame.cbTipoSolicitud
						.setAction(new AbstractAction() {

							@Override
							public void actionPerformed(ActionEvent e) {
								if (createOrderFrame.cbTipoSolicitud
										.getItemCount() > 0
										&& createOrderFrame.tfCentroCosto
												.getText().length() > 0
										&& globalDataContext.solicitud != null) {

									globalDataContext.solicitud
											.setCodigoSolicitud(globalDataContext
													.generarCodigoSoporteTecnico(
															((TipoServicio) ((SelectItem) createOrderFrame.cbTipoSolicitud
																	.getSelectedItem())
																	.getData())
																	.getInicial(),
															createOrderFrame.tfCentroCosto
																	.getText()));

									createOrderFrame.tfNumeroSolicitud
											.setText(globalDataContext.solicitud
													.getCodigoSolicitud());
								} 
							}
						});

			}

			{ // Llenado la data de canales
				createOrderFrame.cbCanal.removeAllItems();

				globalDataContext.loadListaDeCanales();
				for (Canal canal : globalDataContext.listaDeCanales) {
					createOrderFrame.cbCanal.addItem(new SelectItem(canal,
							Canal.NOMBRE));
				}
			}
			{
				createOrderFrame.cbActivarMovimientoTanque
						.setAction(new AbstractAction() {
							@Override
							public void actionPerformed(ActionEvent e) {
								createOrderFrame.pMovimientoTanque
										.setVisible(createOrderFrame.cbActivarMovimientoTanque
												.isSelected());
								globalDataContext.solicitud
										.setMovimientoTanque(createOrderFrame.cbActivarMovimientoTanque
												.isSelected());
							}
						});
				createOrderFrame.cbActivarMovimientoTanque
						.setText(bundle
								.getString("message.activarInformacionMovimientotanque"));
			}
			{// razon social

				createOrderFrame.cbRazonSocial.setAction(new AbstractAction() {

					@Override
					public void actionPerformed(ActionEvent ae) {
						JComboBox jComboBox = (JComboBox) ae.getSource();

						if (jComboBox.getItemCount() > 0) {

							createOrderFrame.cbRazonComercial.removeAllItems();

							if (jComboBox.getSelectedItem().getClass()
									.equals(String.class)) {

							} else {

								SelectItem selectItem = (SelectItem) jComboBox
										.getSelectedItem();

								Cliente tempCliente = (Cliente) selectItem
										.getData();

								for (String razonComercial : tempCliente
										.getRazonComercial()) {
									createOrderFrame.cbRazonComercial
											.addItem(razonComercial);
								}
							}
							if (createOrderFrame.cbRazonComercial
									.getItemCount() > 0) {
								createOrderFrame.cbRazonComercial
										.setSelectedIndex(0);
							}
						}
					}
				});
			}
			{// Ingeniero DT
				createOrderFrame.cbIngenieroDT.removeAllItems();

				for (User user : globalDataContext.listaDeIngenierosDesarrolloTecnico) {
					if (user.getRole().equals(
							Role.RoleIngenieroDesarrolloTecnico.text)) {
						createOrderFrame.cbIngenieroDT.addItem(new SelectItem(
								user, "fullName"));
					}
				}
			}
			createOrderFrame.bGuardarSolicitud
					.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent arg0) {

							{// Validaciones
								String ruc = createOrderFrame.tfRUCoDNI
										.getText();
								String instalacion = createOrderFrame.tfInstalacion
										.getText();

								try {
									globalDataContext.validateNumber(ruc);

								} catch (Exception e) {
									JOptionPane
											.showMessageDialog(
													currentFrame,
													bundle.getString("message.rucNoNumerico"),
													bundle.getString("message.applicationTitle"),
													JOptionPane.ERROR_MESSAGE);
									return;
								}

								try {
									globalDataContext
											.validateNumber(instalacion);
								} catch (Exception e) {
									JOptionPane
											.showMessageDialog(
													currentFrame,
													bundle.getString("message.instalacionNoNumerico"),
													bundle.getString("message.applicationTitle"),
													JOptionPane.ERROR_MESSAGE);
									return;
								}
							}

							/*
							 * Configurando la pantalla para los ususarios de
							 * tipo GerenteComercial
							 */
							if (globalDataContext.loggedUser.getRole().equals(
									Role.RoleGerenteComercial.text)) {
								globalDataContext.solicitud
										.setCodigoSolicitud(createOrderFrame.tfNumeroSolicitud
												.getText());
								globalDataContext.solicitud
										.setCanal(createOrderFrame.cbCanal
												.getSelectedItem().toString());

								globalDataContext.solicitud
										.setCentroCosto(createOrderFrame.tfCentroCosto
												.getText());

								globalDataContext.solicitud
										.setCuentaContable(createOrderFrame.tfCuentaContable
												.getText());
								globalDataContext.solicitud
										.setIngenieroDT(((User) (((SelectItem) createOrderFrame.cbIngenieroDT
												.getSelectedItem()).getData())));
								globalDataContext.solicitud
										.setCodigoPlanta(((Planta) ((SelectItem) createOrderFrame.cbPlanta
												.getSelectedItem()).getData())
												.getCodigoDePlanta());

								for (Canal canal : globalDataContext.listaDeCanales) {
									for (Planta centroCosto : canal
											.getPlantas()) {
										if (centroCosto
												.getCodigoDePlanta()
												.equals(globalDataContext.solicitud
														.getCodigoPlanta())) {
											globalDataContext.solicitud
													.setNombrePlanta(centroCosto
															.getNombreDePlanta());
											break;
										}
									}
								}

								globalDataContext.solicitud
										.setRazonComercial(createOrderFrame.cbRazonComercial
												.getSelectedItem().toString());

								globalDataContext.solicitud
										.setRazonSocial(createOrderFrame.cbRazonSocial
												.getSelectedItem().toString());

								globalDataContext.solicitud
										.setTipoPartida(createOrderFrame.cbTipoPartida
												.getSelectedItem().toString());
								// debe de estar antes del for (TipoSolicitud
								// tipoSolicitud
								globalDataContext.solicitud
										.setTipoSolicitud(createOrderFrame.cbTipoSolicitud
												.getSelectedItem().toString());
								globalDataContext.solicitud
										.setFechaEmision((Date) createOrderFrame.sFechaEmision
												.getValue());

								globalDataContext.solicitud
										.setFechaInicioObra((Date) createOrderFrame.sFechaInicioObra
												.getValue());

								globalDataContext.solicitud
										.setFechaFinObra((Date) createOrderFrame.sFechaFinObra
												.getValue());

								globalDataContext.solicitud
										.setDescripcionServicio(createOrderFrame.taDescripcionServico
												.getText());
								globalDataContext.solicitud
										.setDireccionObra(createOrderFrame.tfDireccionObra
												.getText());
								globalDataContext.solicitud
										.setEstado(TipoEstadoSolicitud.PENDIENTE);
								globalDataContext.solicitud
										.setSupervisorComercial(globalDataContext.loggedUser);

								// TODO: corregir
								globalDataContext.solicitud
										.setDistanciaHorizontal((Double) createOrderFrame.sHorizontal
												.getValue());
								// TODO: corregir
								globalDataContext.solicitud
										.setDistanciaVertical((Double) createOrderFrame.sVertical
												.getValue());
								globalDataContext.solicitud
										.setInstalacion(createOrderFrame.tfInstalacion
												.getText());
								globalDataContext.solicitud
										.setJustificacionAFI(createOrderFrame.tfJustificacionAFI
												.getText());
								globalDataContext.solicitud
										.setNumeroAFI(createOrderFrame.tfNumeroProyectoAFI
												.getText());
								globalDataContext.solicitud
										.setPersonaContactoNombre(createOrderFrame.tfPersonaContacto
												.getText());
								globalDataContext.solicitud
										.setPersonaContactoEmail(createOrderFrame.tfEmail
												.getText());
								globalDataContext.solicitud
										.setPersonaContactoTelefono(createOrderFrame.tfTelefono
												.getText());
								globalDataContext.solicitud
										.setRucOdni(createOrderFrame.tfRUCoDNI
												.getText());
								globalDataContext.solicitud
										.setReferencia(createOrderFrame.tfReferencia
												.getText());
								globalDataContext.solicitud
										.setTitular(createOrderFrame.tfTitular
												.getText());
							}
							/*
							 * Configurando la pantalla para los ususarios de
							 * tipo IngenieroDT
							 */
							else if (globalDataContext.loggedUser
									.getRole()
									.equals(Role.RoleIngenieroDesarrolloTecnico.text)) {
								globalDataContext.solicitud
										.setCodigoOrdenSoporteTecnico(createOrderFrame.tfNumeroSolicitud
												.getText());
								globalDataContext.solicitud
										.setCanal(createOrderFrame.cbCanal
												.getSelectedItem().toString());

								globalDataContext.solicitud
										.setCentroCosto(createOrderFrame.tfCentroCosto
												.getText());

								globalDataContext.solicitud
										.setCuentaContable(createOrderFrame.tfCuentaContable
												.getText());
								globalDataContext.solicitud
										.setIngenieroDT(globalDataContext.loggedUser);
								globalDataContext.solicitud
										.setCodigoPlanta(((Planta) ((SelectItem) createOrderFrame.cbPlanta
												.getSelectedItem()).getData())
												.getCodigoDePlanta());

								for (Canal canal : globalDataContext.listaDeCanales) {
									for (Planta planta : canal.getPlantas()) {
										if (planta.getCodigoDePlanta().equals(
												globalDataContext.solicitud
														.getCodigoPlanta())) {
											globalDataContext.solicitud.setNombrePlanta(planta
													.getNombreDePlanta());
											break;
										}
									}
								}

								globalDataContext.solicitud
										.setRazonComercial(createOrderFrame.cbRazonComercial
												.getSelectedItem().toString());

								globalDataContext.solicitud
										.setRazonSocial(createOrderFrame.cbRazonSocial
												.getSelectedItem().toString());

								globalDataContext.solicitud
										.setTipoPartida(createOrderFrame.cbTipoPartida
												.getSelectedItem().toString());
								// TODO: [Pregunta] el ingeniero puede cambiar
								// el tipo
								// de servicio
								globalDataContext.solicitud
										.setTipoSolicitud(createOrderFrame.cbTipoSolicitud
												.getSelectedItem().toString());
								globalDataContext.solicitud
										.setFechaEmision((Date) createOrderFrame.sFechaEmision
												.getValue());

								globalDataContext.solicitud
										.setFechaInicioObra((Date) createOrderFrame.sFechaInicioObra
												.getValue());

								globalDataContext.solicitud
										.setFechaFinObra((Date) createOrderFrame.sFechaFinObra
												.getValue());

								globalDataContext.solicitud
										.setDescripcionServicio(createOrderFrame.taDescripcionServico
												.getText());
								globalDataContext.solicitud
										.setDireccionObra(createOrderFrame.tfDireccionObra
												.getText());

								globalDataContext.solicitud
										.setEstado(TipoEstadoSolicitud.EJECUCION);

								{/*
								 * Guada la tabla de movimiento de tanque en
								 * caso esta tabla contenga datos
								 */
									if (createOrderFrame.tListaMovimientoTanque
											.getRowCount() > 0) {
										globalDataContext.solicitud
												.setMovimientosTanque(((BeanTableModel) createOrderFrame.tListaMovimientoTanque
														.getModel())
														.getDataVector());
									}
								}
								// TODO: corregir
								globalDataContext.solicitud
										.setDistanciaHorizontal((Double) createOrderFrame.sHorizontal
												.getValue());
								// TODO: corregir
								globalDataContext.solicitud
										.setDistanciaVertical((Double) createOrderFrame.sVertical
												.getValue());
								globalDataContext.solicitud
										.setInstalacion(createOrderFrame.tfInstalacion
												.getText());
								globalDataContext.solicitud
										.setJustificacionAFI(createOrderFrame.tfJustificacionAFI
												.getText());
								globalDataContext.solicitud
										.setNumeroAFI(createOrderFrame.tfNumeroProyectoAFI
												.getText());
								globalDataContext.solicitud
										.setPersonaContactoNombre(createOrderFrame.tfPersonaContacto
												.getText());
								globalDataContext.solicitud
										.setPersonaContactoEmail(createOrderFrame.tfEmail
												.getText());
								globalDataContext.solicitud
										.setPersonaContactoTelefono(createOrderFrame.tfTelefono
												.getText());
								globalDataContext.solicitud
										.setRucOdni(createOrderFrame.tfRUCoDNI
												.getText());
								globalDataContext.solicitud
										.setReferencia(createOrderFrame.tfReferencia
												.getText());
								globalDataContext.solicitud
										.setTitular(createOrderFrame.tfTitular
												.getText());

								if (createOrderFrame.cbMotivoEjecucion
										.getSelectedItem() != null) {
									globalDataContext.solicitud
											.setMotivoParaEjecucion(createOrderFrame.cbMotivoEjecucion
													.getSelectedItem()
													.toString());
								}
								if (createOrderFrame.cbNombreEECC
										.getSelectedItem() != null) {
									globalDataContext.solicitud
											.setEecc(createOrderFrame.cbNombreEECC
													.getSelectedItem()
													.toString());
								}

								if (createOrderFrame.cbResponsableEECC
										.getSelectedItem() != null) {
									globalDataContext.solicitud
											.setResponsableEECC(createOrderFrame.cbResponsableEECC
													.getSelectedItem()
													.toString());
								}

								if (createOrderFrame.cbActivarMovimientoTanque
										.isSelected()) {
									{// load values from internal tables
										if (createOrderFrame.tListaTanques
												.getRowCount() > 0) {

											globalDataContext.solicitud
													.setTanque(((BeanTableModel) createOrderFrame.tListaTanques
															.getModel())
															.getDataVector());

										}
										if (createOrderFrame.tListaEquipos
												.getRowCount() > 0) {

											globalDataContext.solicitud
													.setEquipo(((BeanTableModel) createOrderFrame.tListaEquipos
															.getModel())
															.getDataVector());
										}
									}
									globalDataContext.solicitud
											.setMotivoMovimientoTanqueDescripcion(createOrderFrame.tfMotivoMovimientoTanque
													.getText());
									globalDataContext.solicitud
											.setMovimientoTanqueComentario(createOrderFrame.taComentariosDetalleMovimiento
													.getText());

								}

								if (!createOrderFrame.cbActivarMovimientoTanque
										.isSelected()) {
									globalDataContext.solicitud
											.setMovimientosTanque(new ArrayList<MotivoMovimientoTanque>());
								}
							}

							if (globalDataContext.validateNumeroInstalacionNoRepetido(
									globalDataContext.solicitud.getId(),
									globalDataContext.solicitud
											.getInstalacion())) {
								globalDataContext
										.guadarSolicitud(globalDataContext.solicitud);
								globalDataContext.solicitud = null;

								globalDataContext
										.loadListaDeSolicitudes(buildXPathQuery());
								updateTableSolicitud(globalDataContext.listaDeSolicitudes);

								hideCurrentForm();
							} else {
								JOptionPane.showMessageDialog(
										currentFrame,
										bundle.getString("message.instalacionRepetita"),
										bundle.getString("message.applicationTitle"),
										JOptionPane.ERROR_MESSAGE);
							}

						}

					});

		}
	}

	private String buildXPathQuery() {
		isWaiting = true;
		String query = "/solicitud/@id!=''";

		String numeroSolicitud = visualizeSolicitudesFrame.tfOrderNumber
				.getText().trim();
		String numeroInstalacion = visualizeSolicitudesFrame.tfInstallationNumber
				.getText().trim();
		boolean mostrarTodo = visualizeSolicitudesFrame.cbShowAll.isSelected();
		int estado = -1;
		if (visualizeSolicitudesFrame.cbOrderStatus.getSelectedItem() != null) {
			estado = ((TipoEstadoSolicitud) ((SelectItem) visualizeSolicitudesFrame.cbOrderStatus
					.getSelectedItem()).getData()).getIndex();
		}

		query = MessageFormat
				.format("/solicitud[ starts-with("
						+ Solicitud.CODIGOSOLICITUD
						+ ",''{0}'') "
						+
						/**/
						"and starts-with("
						+ Solicitud.INSTALACION
						+ ",''{1}'') "
						+
						/**/
						"{2,choice,0# and starts-with("
						+ (globalDataContext.loggedUser.getRole().equals(
								Role.RoleGerenteComercial.text) ? Solicitud.SUPERVISORCOMERCIAL
								: Solicitud.INGENIERODT)
						+ "/@id,''"
						+ globalDataContext.loggedUser.getId()
						+ "'')|1#  } "
						+
						/**/
						"and ( starts-with("
						+ Solicitud.ESTADO
						+ ",''{3}'') "
						+
						/*
						 * Si el susuario logeado es comercial se considera como
						 * ordenes atendidas las ordenes en estado finalizadas
						 * (2)
						 */
						(globalDataContext.loggedUser.getRole().equals(
								Role.RoleGerenteComercial.text) ? " or starts-with("
								+ Solicitud.ESTADO + ",'2')"
								: "")
						//
						+ ")  ] !=''''   ",
				/**/
				numeroSolicitud, numeroInstalacion, (mostrarTodo ? 1 : 0),
						estado);

		return query;
	}

	private void initializeVisualizarSolicitudes() {
		{// VISUALIZAR SOLICITUDES COMERCIAL
			visualizeSolicitudesFrame = new VisualizeSolicitudesFrame();

			visualizeSolicitudesFrame.cbOrderStatus
					.setAction(new AbstractAction() {
						@Override
						public void actionPerformed(ActionEvent e) {
							globalDataContext
									.loadListaDeSolicitudes(buildXPathQuery());
							updateTableSolicitud(globalDataContext.listaDeSolicitudes);
						}
					});

			visualizeSolicitudesFrame.cbShowAll.setAction(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					globalDataContext.loadListaDeSolicitudes(buildXPathQuery());
					updateTableSolicitud(globalDataContext.listaDeSolicitudes);
				}
			});
			visualizeSolicitudesFrame.tfOrderNumber
					.setAction(new AbstractAction() {
						@Override
						public void actionPerformed(ActionEvent e) {
							globalDataContext
									.loadListaDeSolicitudes(buildXPathQuery());
							updateTableSolicitud(globalDataContext.listaDeSolicitudes);
						}
					});
			visualizeSolicitudesFrame.tfInstallationNumber
					.setAction(new AbstractAction() {
						@Override
						public void actionPerformed(ActionEvent e) {
							globalDataContext
									.loadListaDeSolicitudes(buildXPathQuery());
							updateTableSolicitud(globalDataContext.listaDeSolicitudes);
						}
					});

			visualizeSolicitudesFrame.bCreateOrder
					.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent arg0) {

							showDialog(createOrderFrame);
							globalDataContext
									.loadListaDeSolicitudes(buildXPathQuery());
							updateTableSolicitud(globalDataContext.listaDeSolicitudes);

							globalDataContext.solicitud = null;

							updateTableEquipos(new ArrayList<Equipo>());
							updateTableTanques(new ArrayList<Tanque>());
							createOrderFrame.clearUI();
						}

					});
			visualizeSolicitudesFrame.tOrders.getSelectionModel()
					.addListSelectionListener(new ListSelectionListener() {

						@Override
						public void valueChanged(ListSelectionEvent arg0) {
							int selectedRow = visualizeSolicitudesFrame.tOrders
									.getSelectedRow();
							if (selectedRow != -1) {

								Solicitud solicitud = (Solicitud) visualizeSolicitudesFrame.tOrders
										.getModel().getValueAt(selectedRow, -1);

								if (globalDataContext.loggedUser.getRole()
										.equals(Role.RoleGerenteComercial.text)) {
									visualizeSolicitudesFrame.bAttachFile
											.setEnabled(true);
									visualizeSolicitudesFrame.bFinalizarOrden
											.setEnabled(false);
									visualizeSolicitudesFrame.bEditar
											.setEnabled(solicitud.getEstado() == TipoEstadoSolicitud.PENDIENTE);
								} else if (globalDataContext.loggedUser
										.getRole()
										.equals(Role.RoleIngenieroDesarrolloTecnico.text)) {
									visualizeSolicitudesFrame.bAttachFile
											.setEnabled(true);
									visualizeSolicitudesFrame.bFinalizarOrden
											.setEnabled((solicitud.getEstado() == TipoEstadoSolicitud.EJECUCION || solicitud
													.getEstado() == TipoEstadoSolicitud.FINALIZADA));
									visualizeSolicitudesFrame.bEditar
											.setEnabled(solicitud.getEstado() == TipoEstadoSolicitud.PENDIENTE);
								} else {
									visualizeSolicitudesFrame.bAttachFile
											.setEnabled(false);
									visualizeSolicitudesFrame.bFinalizarOrden
											.setEnabled(false);
									visualizeSolicitudesFrame.bEditar
											.setEnabled(false);
								}

								String attachedLinks = "";

								{
									attachedLinks = "<table border='0'>";

									List<ArchivoAdjunto> listaDeArchivosAdjuntos = solicitud
											.getArchivosAdjuntos();
									Collections.sort(listaDeArchivosAdjuntos,
											new Comparator<ArchivoAdjunto>() {
												@Override
												public int compare(
														ArchivoAdjunto o1,
														ArchivoAdjunto o2) {
													return o2
															.getTitulo()
															.compareTo(
																	o1.getTitulo());
												}
											});

									for (ArchivoAdjunto archivoAdjunto : listaDeArchivosAdjuntos) {
										attachedLinks += "<tr><td>";
										attachedLinks += MessageFormat
												.format("<a href=\"{0}/{1}\">{2}</a><br />",
														solicitud.getId(),
														archivoAdjunto
																.getFileName(),
														archivoAdjunto
																.getTitulo());
										attachedLinks += "</td></tr>";
									}
									attachedLinks += "</table>";
								}

								String pageContent = MessageFormat.format(
										bundle.getString("message.plantillaDePrevisualizacionDeSolicitud"),
										//
										solicitud.getTipoSolicitud() == null ? ""
												: solicitud.getTipoSolicitud(),
										//
										solicitud.getCodigoSolicitud() == null ? ""
												: solicitud
														.getCodigoSolicitud(),
										//
										solicitud.getFechaEmision() == null ? ""
												: solicitud.getFechaEmision(),
										//
										solicitud.getFechaFinalizacion() == null ? ""
												: solicitud
														.getFechaFinalizacion(),
										//
										solicitud.getRazonSocial() == null ? ""
												: solicitud.getRazonSocial(),
										//
										solicitud.getRazonComercial() == null ? ""
												: solicitud.getRazonComercial(),
										//
										solicitud.getDescripcionServicio() == null ? ""
												: solicitud
														.getDescripcionServicio(),
										//
										solicitud.getSupervisorComercial()
												.getFullName() == null ? ""
												: solicitud
														.getSupervisorComercial()
														.getFullName(),
										//
										solicitud.getIngenieroDT()
												.getFullName() == null ? ""
												: solicitud.getIngenieroDT()
														.getFullName(),
										//
										attachedLinks);

								visualizeSolicitudesFrame.tpPreviewOrder
										.setText(pageContent);
							}

						}
					});

			visualizeSolicitudesFrame.tpPreviewOrder
					.addHyperlinkListener(new HyperlinkListener() {

						@Override
						public void hyperlinkUpdate(HyperlinkEvent e) {
							if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
								try {
									File tempFile = globalDataContext
											.crearArchivoTemporal(e
													.getDescription());
									Desktop.getDesktop().open(tempFile);
								} catch (IOException e1) {
									JOptionPane.showMessageDialog(currentFrame,
											"No se pudo abrir el archivo error:"
													+ e1.getMessage());
								}
							}

						}
					});

			visualizeSolicitudesFrame.bAttachFile
					.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {

							JTable table = visualizeSolicitudesFrame.tOrders;

							if (table.getRowCount() > 0) {
								if (table.getSelectedRow() != -1) {
									Solicitud solicitud = (Solicitud) table
											.getValueAt(table.getSelectedRow(),
													-1);

									JFileChooser chooser = new JFileChooser();
									chooser.setMultiSelectionEnabled(true);
									chooser.showDialog(currentFrame,
											"Seleccionar Archivo(s)");

									ArchivoAdjunto archivoAdjunto;

									for (File file : chooser.getSelectedFiles()) {
										archivoAdjunto = new ArchivoAdjunto();
										archivoAdjunto.setFileName(UUID
												.randomUUID().toString()
												+ "-"
												+ file.getName());
										archivoAdjunto.setFecha(Calendar
												.getInstance().getTime());
										archivoAdjunto.setTitulo(MessageFormat
												.format("{0,date,yy/MM/dd HH:mm.ss} - {1}",
														archivoAdjunto
																.getFecha(),
														file.getName()));

										globalDataContext.adjuntarArchivo(
												solicitud.getId(),
												archivoAdjunto.getFileName(),
												file);

										solicitud.getArchivosAdjuntos().add(
												archivoAdjunto);
									}
									globalDataContext
											.guadarSolicitud(solicitud);
								} else {
									JOptionPane
											.showMessageDialog(
													currentFrame,
													bundle.getString("mensage.seleccionarUnElementoDeLaLista"),
													bundle.getString("message.applicationTitle"),
													JOptionPane.WARNING_MESSAGE);
								}
							} else {
								JOptionPane.showMessageDialog(
										currentFrame,
										bundle.getString("mensage.seleccionarUnElementoDeLaLista"),
										bundle.getString("message.applicationTitle"),
										JOptionPane.WARNING_MESSAGE);
							}

						}
					});

			visualizeSolicitudesFrame.bFinalizarOrden
					.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							int selectedRow = visualizeSolicitudesFrame.tOrders
									.getSelectedRow();

							if (selectedRow != -1) {
								Solicitud solicitud = (Solicitud) visualizeSolicitudesFrame.tOrders
										.getModel().getValueAt(selectedRow, -1);

								globalDataContext.solicitud = solicitud;
								updateTableLiquidacion(globalDataContext.solicitud
										.getLiquidaciones());
								showDialog(finalizarSolicitudFrame);

								globalDataContext.solicitud = null;
							} else {
								JOptionPane.showMessageDialog(
										currentFrame,
										bundle.getString("mensage.seleccionarUnElementoDeLaLista"),
										bundle.getString("message.applicationTitle"),
										JOptionPane.WARNING_MESSAGE);
							}
							globalDataContext
									.loadListaDeSolicitudes(buildXPathQuery());
							updateTableSolicitud(globalDataContext.listaDeSolicitudes);
						}
					});

			visualizeSolicitudesFrame.bEditar
					.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {
							JTable table = visualizeSolicitudesFrame.tOrders;

							if (table.getRowCount() > 0) {
								if (table.getSelectedRow() != -1) {
									Solicitud solicitud = (Solicitud) table
											.getModel().getValueAt(
													table.getSelectedRow(), -1);
									globalDataContext.solicitud = solicitud;

									showDialog(createOrderFrame);
									globalDataContext
											.loadListaDeSolicitudes(buildXPathQuery());
									updateTableSolicitud(globalDataContext.listaDeSolicitudes);

									globalDataContext.solicitud = null;

									updateTableEquipos(new ArrayList<Equipo>());
									updateTableTanques(new ArrayList<Tanque>());
									createOrderFrame.clearUI();
								} else {
									JOptionPane
											.showMessageDialog(
													currentFrame,
													bundle.getString("mensage.seleccionarUnElementoDeLaLista"),
													bundle.getString("message.applicationTitle"),
													JOptionPane.WARNING_MESSAGE);
								}
							} else {
								JOptionPane.showMessageDialog(
										currentFrame,
										bundle.getString("mensage.seleccionarUnElementoDeLaLista"),
										bundle.getString("message.applicationTitle"),
										JOptionPane.WARNING_MESSAGE);
							}
						}
					});
		}
	}

	private void initializeHomeFrame() {
		{// HOME FRAME
			homeFrame = new HomeFrame();
			homeFrame.setExtendedState(homeFrame.getExtendedState()
					| JFrame.MAXIMIZED_BOTH);
			homeFrame.addWindowListener(new WindowAdapter() {
				@Override
				public void windowOpened(WindowEvent e) {
					// este panel es activado solo cuando la solicitud es una
					// grua
					createOrderFrame.pMovimientoTanque.setVisible(false);

					if (globalDataContext.loggedUser.getRole().equals(
							Role.RoleAdministrador.text)) {
						homeFrame.miCreateSolicitud.setVisible(false);
						homeFrame.mConsultarSolicitud.setVisible(false);
						homeFrame.mSystem.setVisible(true);

						reportesFrame.bExportarSolicitudesPorVencer
								.setVisible(true);
						reportesFrame.bExportarSolicitudesSupervisorComercial
								.setVisible(true);
						reportesFrame.bExportarOrdenesIngenieroDT
								.setVisible(true);

					} else if (globalDataContext.loggedUser.getRole().equals(
							Role.RoleGerenteComercial.text)) {
						visualizeSolicitudesFrame.bFinalizarOrden
								.setVisible(false);
						createOrderFrame.pEjecucionObra.setVisible(false);

						homeFrame.miCreateSolicitud.setAccelerator(KeyStroke
								.getKeyStroke(KeyEvent.VK_1,
										ActionEvent.CTRL_MASK));
						homeFrame.miComercialListarPendientes
								.setAccelerator(KeyStroke.getKeyStroke(
										KeyEvent.VK_2, ActionEvent.CTRL_MASK));
						homeFrame.miComercialListarAtendidas
								.setAccelerator(KeyStroke.getKeyStroke(
										KeyEvent.VK_3, ActionEvent.CTRL_MASK));
						homeFrame.miReporte.setAccelerator(KeyStroke
								.getKeyStroke(KeyEvent.VK_4,
										ActionEvent.CTRL_MASK));

						homeFrame.miIngenieroListarEnEjecucion
								.setVisible(false);
						homeFrame.miIngenieroListarFinalizadas
								.setVisible(false);
						homeFrame.miIngenieroListarPendientes.setVisible(false);
						homeFrame.mSystem.setVisible(false);

						reportesFrame.bExportarSolicitudesPorVencer
								.setVisible(false);
						reportesFrame.bExportarSolicitudesSupervisorComercial
								.setVisible(true);
						reportesFrame.bExportarOrdenesIngenieroDT
								.setVisible(false);

					} else if (globalDataContext.loggedUser.getRole().equals(
							Role.RoleIngenieroDesarrolloTecnico.text)) {
						visualizeSolicitudesFrame.bCreateOrder
								.setVisible(false);

						homeFrame.miIngenieroListarPendientes
								.setAccelerator(KeyStroke.getKeyStroke(
										KeyEvent.VK_1, ActionEvent.CTRL_MASK));
						homeFrame.miIngenieroListarEnEjecucion
								.setAccelerator(KeyStroke.getKeyStroke(
										KeyEvent.VK_2, ActionEvent.CTRL_MASK));
						homeFrame.miIngenieroListarFinalizadas
								.setAccelerator(KeyStroke.getKeyStroke(
										KeyEvent.VK_3, ActionEvent.CTRL_MASK));
						homeFrame.miReporte.setAccelerator(KeyStroke
								.getKeyStroke(KeyEvent.VK_4,
										ActionEvent.CTRL_MASK));

						homeFrame.miCreateSolicitud.setVisible(false);
						homeFrame.miComercialListarAtendidas.setVisible(false);
						homeFrame.miComercialListarPendientes.setVisible(false);
						homeFrame.mSystem.setVisible(false);

						reportesFrame.bExportarSolicitudesPorVencer
								.setVisible(false);
						reportesFrame.bExportarSolicitudesSupervisorComercial
								.setVisible(false);
						reportesFrame.bExportarOrdenesIngenieroDT
								.setVisible(true);
					} else {
						homeFrame.miCreateSolicitud.setVisible(false);
						homeFrame.mConsultarSolicitud.setVisible(false);
						homeFrame.miIngenieroListarEnEjecucion
								.setVisible(false);
						homeFrame.miIngenieroListarFinalizadas
								.setVisible(false);
						homeFrame.miIngenieroListarPendientes.setVisible(false);
						homeFrame.mSystem.setVisible(false);
						homeFrame.miComercialListarAtendidas.setVisible(false);
						homeFrame.miComercialListarPendientes.setVisible(false);
					}
				}
			});

			homeFrame.miClose.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					int confirmDialogResult = JOptionPane.showConfirmDialog(
							currentFrame,
							bundle.getString("message.confirmClose"),
							bundle.getString("message.applicationTitle"),
							JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE);

					if (confirmDialogResult == JOptionPane.YES_OPTION) {
						System.exit(0);
					}
				}
			});
			homeFrame.miVisualizeUsers.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					globalDataContext.loadListaDeUsuarios();
					updateTableUsers(globalDataContext.listaDeUsuarios);
					homeFrame.setContentPane(visualizeUsers.getContentPane());
					homeFrame.setVisible(true);
				}

			});
			homeFrame.miCreateSolicitud.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					actualizarUIVisualizarSolicitudes();
					globalDataContext.loadListaDeSolicitudes(buildXPathQuery());
					updateTableSolicitud(globalDataContext.listaDeSolicitudes);
					homeFrame.setContentPane(visualizeSolicitudesFrame
							.getContentPane());
					homeFrame.setVisible(true);
					visualizeSolicitudesFrame.bCreateOrder.doClick();
				}
			});
			homeFrame.miComercialListarAtendidas
					.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent arg0) {
							actualizarUIVisualizarSolicitudes();
							for (int i = 0; i < visualizeSolicitudesFrame.cbOrderStatus
									.getItemCount(); i++) {
								SelectItem item = (SelectItem) visualizeSolicitudesFrame.cbOrderStatus
										.getItemAt(i);

								if (((TipoEstadoSolicitud) item.getData())
										.getIndex() == TipoEstadoSolicitud.EJECUCION) {
									visualizeSolicitudesFrame.cbOrderStatus
											.setSelectedIndex(i);
									break;
								}
							}

							homeFrame.setContentPane(visualizeSolicitudesFrame
									.getContentPane());
							homeFrame.setVisible(true);
						}
					});

			homeFrame.miComercialListarPendientes
					.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent arg0) {
							actualizarUIVisualizarSolicitudes();
							for (int i = 0; i < visualizeSolicitudesFrame.cbOrderStatus
									.getItemCount(); i++) {
								SelectItem item = (SelectItem) visualizeSolicitudesFrame.cbOrderStatus
										.getItemAt(i);

								if (((TipoEstadoSolicitud) item.getData())
										.getIndex() == TipoEstadoSolicitud.PENDIENTE) {
									visualizeSolicitudesFrame.cbOrderStatus
											.setSelectedIndex(i);
									break;
								}
							}

							homeFrame.setContentPane(visualizeSolicitudesFrame
									.getContentPane());
							homeFrame.setVisible(true);
						}
					});

			homeFrame.miReporte.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {

					homeFrame.setContentPane(reportesFrame.getContentPane());

					DAOSolicitud daoSolicitud = new DAOSolicitud();
					List<Solicitud> solicitudes = daoSolicitud.list();

					if (globalDataContext.loggedUser.getRole().equals(
							Role.RoleGerenteComercial.text)) {
						List<User> users = new ArrayList<User>();
						users.add(globalDataContext.loggedUser);
						globalDataContext.buildReport = new BuildReportConteoSolicitudesPorGerenteComercial(
								solicitudes,
								users,
								globalDataContext.listaDeIngenierosDesarrolloTecnico);

					} else if (globalDataContext.loggedUser.getRole().equals(
							Role.RoleIngenieroDesarrolloTecnico.text)) {
						List<User> users = new ArrayList<User>();
						users.add(globalDataContext.loggedUser);
						globalDataContext.buildReport = new BuildReportConteoSolicitudesPorGerenteComercial(
								solicitudes,
								globalDataContext.listaDeSupervisorComercial,
								users);

					} else {
						globalDataContext.buildReport = new BuildReportConteoSolicitudesPorGerenteComercial(
								solicitudes,
								globalDataContext.listaDeSupervisorComercial,
								globalDataContext.listaDeIngenierosDesarrolloTecnico);

					}

					homeFrame.setVisible(true);

				}
			});

			homeFrame.miIngenieroListarPendientes
					.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent arg0) {
							actualizarUIVisualizarSolicitudes();
							for (int i = 0; i < visualizeSolicitudesFrame.cbOrderStatus
									.getItemCount(); i++) {
								SelectItem item = (SelectItem) visualizeSolicitudesFrame.cbOrderStatus
										.getItemAt(i);

								if (((TipoEstadoSolicitud) item.getData())
										.getIndex() == TipoEstadoSolicitud.PENDIENTE) {
									visualizeSolicitudesFrame.cbOrderStatus
											.setSelectedIndex(i);
									break;
								}
							}

							homeFrame.setContentPane(visualizeSolicitudesFrame
									.getContentPane());
							homeFrame.setVisible(true);
						}
					});

			homeFrame.miIngenieroListarEnEjecucion
					.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent arg0) {
							actualizarUIVisualizarSolicitudes();
							for (int i = 0; i < visualizeSolicitudesFrame.cbOrderStatus
									.getItemCount(); i++) {
								SelectItem item = (SelectItem) visualizeSolicitudesFrame.cbOrderStatus
										.getItemAt(i);

								if (((TipoEstadoSolicitud) item.getData())
										.getIndex() == TipoEstadoSolicitud.EJECUCION) {
									visualizeSolicitudesFrame.cbOrderStatus
											.setSelectedIndex(i);
									break;
								}
							}

							homeFrame.setContentPane(visualizeSolicitudesFrame
									.getContentPane());
							homeFrame.setVisible(true);
						}
					});
			homeFrame.miIngenieroListarFinalizadas
					.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent arg0) {
							actualizarUIVisualizarSolicitudes();
							for (int i = 0; i < visualizeSolicitudesFrame.cbOrderStatus
									.getItemCount(); i++) {
								SelectItem item = (SelectItem) visualizeSolicitudesFrame.cbOrderStatus
										.getItemAt(i);

								if (((TipoEstadoSolicitud) item.getData())
										.getIndex() == TipoEstadoSolicitud.FINALIZADA) {
									visualizeSolicitudesFrame.cbOrderStatus
											.setSelectedIndex(i);
									break;
								}
							}

							homeFrame.setContentPane(visualizeSolicitudesFrame
									.getContentPane());
							homeFrame.setVisible(true);
						}

					});
			homeFrame.setLocationRelativeTo(null);
		}
	}

	private void actualizarUIVisualizarSolicitudes() {
		// TODO Auto-generated method stub
		DAOTipoEstadoSolicitud daoTipoEstadoSolicitud = new DAOTipoEstadoSolicitud();
		List<TipoEstadoSolicitud> list = null;

		if (globalDataContext.loggedUser.getRole().equals(
				Role.RoleGerenteComercial.text)) {
			list = daoTipoEstadoSolicitud
					.search("/tipoEstadoSolicitud/nombre[text()='message_estado0' or text()='message_estado1']!=''");
			visualizeSolicitudesFrame.bFinalizarOrden.setVisible(false);

		} else if (globalDataContext.loggedUser.getRole().equals(
				Role.RoleIngenieroDesarrolloTecnico.text)) {
			list = daoTipoEstadoSolicitud
					.search("/tipoEstadoSolicitud/nombre[text()='message_estado0' or text()='message_estado2' or text()='message_estado3']!=''");
			visualizeSolicitudesFrame.bCreateOrder.setVisible(false);
			visualizeSolicitudesFrame.bFinalizarOrden.setVisible(true);
		}

		Collections.sort(list, new Comparator<TipoEstadoSolicitud>() {
			@Override
			public int compare(TipoEstadoSolicitud o1, TipoEstadoSolicitud o2) {
				return ("" + o1.getIndex()).compareTo("" + o2.getIndex());
			}
		});

		visualizeSolicitudesFrame.cbOrderStatus.removeAllItems();
		for (TipoEstadoSolicitud tipoEstadoSolicitud : list) {
			tipoEstadoSolicitud.setNombre(bundle.getString(tipoEstadoSolicitud
					.getNombre()));
			visualizeSolicitudesFrame.cbOrderStatus.addItem(new SelectItem(
					tipoEstadoSolicitud, "nombre"));
		}
	}

	private void initilizeLoginFrame() {
		{// LOGIN FRAME
			loginFrame = new LoginFrame();
			loginFrame.tfUserName.setAction(new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					loginFrame.bLogin.doClick();
				}
			});
			loginFrame.pfPassword.setAction(new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					loginFrame.bLogin.doClick();
				}
			});

			loginFrame.bLogin.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					boolean valido = globalDataContext.loginUser(
							loginFrame.tfUserName.getText(), new String(
									loginFrame.pfPassword.getPassword()));
					if (valido) {
						homeFrame.setTitle(homeFrame.getTitle() + " - "
								+ globalDataContext.loggedUser.getFullName());
						currentFrame.setVisible(false);
					} else {
						loginFrame.lErrorMessage.setText(bundle
								.getString("message.wrongCredentials"));
					}

				}
			});
			loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
	}

	public void Start() {
		logger.entering(logger.getName(), "Start()");
		showDialog(loginFrame);

		currentFrame = homeFrame;
		homeFrame.setVisible(true);

		System.out.println("ApplicationContext.Start()");
		/*
		 * currentFrame = frameVisualizeEmpresa;
		 * 
		 * currentFrame.setVisible(true);
		 */
		logger.exiting(logger.getName(), "Start()");
	}

	private void updateTableUsers(List<User> users) {
		int currentRowSelected = visualizeUsers.tUsers.getSelectedRow();

		if (visualizeUsers.tUsers.getSelectedRow() != -1) {
			visualizeUsers.tUsers.removeRowSelectionInterval(
					currentRowSelected, currentRowSelected);
		}

		if (visualizeUsers.tUsers.getColumnModel().getColumnCount() == 0) {
			TableColumn column;

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("message.userName"));
			column.setCellRenderer(new BeanTableCellRenderer(User.class,
					User.USERNAME));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			visualizeUsers.tUsers.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("message.fullName"));
			column.setCellRenderer(new BeanTableCellRenderer(User.class,
					User.FULLNAME));
			column.setPreferredWidth(200);

			visualizeUsers.tUsers.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("message.role"));
			column.setCellRenderer(new BeanTableCellRenderer(User.class,
					User.ROLE));
			column.setPreferredWidth(100);

			visualizeUsers.tUsers.getColumnModel().addColumn(column);

		}

		((BeanTableModel) visualizeUsers.tUsers.getModel())
				.setDataVector(new Vector<User>(users));
		visualizeUsers.tUsers.updateUI();

		if (currentRowSelected != -1) {
			if (visualizeUsers.tUsers.getRowCount() == 0) {

			} else if (visualizeUsers.tUsers.getRowCount() == currentRowSelected) {
				currentRowSelected--;
			}

			if (currentRowSelected < visualizeUsers.tUsers.getRowCount()) {

				visualizeUsers.tUsers.setRowSelectionInterval(
						currentRowSelected, currentRowSelected);
			}
		}
	}

	private void updateTableMovimientoTanques(List<MotivoMovimientoTanque> items) {
		// obtener la tabla a modificar
		JTable table = createOrderFrame.tListaMovimientoTanque;
		int currentRowSelected = table.getSelectedRow();

		if (table.getSelectedRow() != -1) {
			table.removeRowSelectionInterval(currentRowSelected,
					currentRowSelected);
		}

		if (table.getColumnModel().getColumnCount() == 0) {
			TableColumn column;

			column = new TableColumn();
			column.setHeaderValue(bundle
					.getString("cabeceraColumna_instalacionReal"));
			column.setCellRenderer(new BeanTableCellRenderer(
					MotivoMovimientoTanque.class,
					MotivoMovimientoTanque.INSTALACIONREAL));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle
					.getString("cabeceraColumna_descripcion"));
			column.setCellRenderer(new BeanTableCellRenderer(
					MotivoMovimientoTanque.class,
					MotivoMovimientoTanque.DESCRIPCION));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle
					.getString("cabeceraColumna_numeroGrafo"));
			column.setCellRenderer(new BeanTableCellRenderer(
					MotivoMovimientoTanque.class,
					MotivoMovimientoTanque.NUMEROGRAFO));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle
					.getString("cabeceraColumna_numeroReserva"));
			column.setCellRenderer(new BeanTableCellRenderer(
					MotivoMovimientoTanque.class,
					MotivoMovimientoTanque.NUMERORESERVA));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("cabeceraColumna_cantidad"));
			column.setCellRenderer(new BeanTableCellRenderer(
					MotivoMovimientoTanque.class,
					MotivoMovimientoTanque.CANTIDAD));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("cabeceraColumna_capacidad"));
			column.setCellRenderer(new BeanTableCellRenderer(
					MotivoMovimientoTanque.class,
					MotivoMovimientoTanque.CAPACIDAD));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle
					.getString("cabeceraColumna_serieCodigo"));
			column.setCellRenderer(new BeanTableCellRenderer(
					MotivoMovimientoTanque.class,
					MotivoMovimientoTanque.SERIECODIGO));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

		}
		// Fuente de datos
		((BeanTableModel) table.getModel())
				.setDataVector(new Vector<MotivoMovimientoTanque>(items));
		table.updateUI();

		if (currentRowSelected != -1) {
			if (table.getRowCount() == 0) {

			} else if (table.getRowCount() == currentRowSelected) {
				currentRowSelected--;
			}

			if (currentRowSelected < table.getRowCount()) {

				table.setRowSelectionInterval(currentRowSelected,
						currentRowSelected);
			}
		}

	}

	private void updateTableTanques(List<Tanque> items) {
		// obtener la tabla a modificar
		JTable table = createOrderFrame.tListaTanques;
		int currentRowSelected = table.getSelectedRow();

		if (table.getSelectedRow() != -1) {
			table.removeRowSelectionInterval(currentRowSelected,
					currentRowSelected);
		}

		if (table.getColumnModel().getColumnCount() == 0) {
			TableColumn column;

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("cabeceraColumna_cantidad"));
			column.setCellRenderer(new BeanTableCellRenderer(Tanque.class,
					Tanque.CANTIDAD));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("cabeceraColumna_capacidad"));
			column.setCellRenderer(new BeanTableCellRenderer(Tanque.class,
					Tanque.CAPACIDAD));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle
					.getString("cabeceraColumna_orientacion"));
			column.setCellRenderer(new BeanTableCellRenderer(Tanque.class,
					Tanque.ORIENTACION));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle
					.getString("cabeceraColumna_instalacion"));
			column.setCellRenderer(new BeanTableCellRenderer(Tanque.class,
					Tanque.INSTALACION));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

		}
		// Fuente de datos
		((BeanTableModel) table.getModel()).setDataVector(new Vector<Tanque>(
				items));
		table.updateUI();

		if (currentRowSelected != -1) {
			if (table.getRowCount() == 0) {

			} else if (table.getRowCount() == currentRowSelected) {
				currentRowSelected--;
			}

			if (currentRowSelected < table.getRowCount()) {

				table.setRowSelectionInterval(currentRowSelected,
						currentRowSelected);
			}
		}
		{
			int conteoDeTanque = 0;
			for (Tanque item : items) {
				conteoDeTanque += item.getCantidad();
			}

			createOrderFrame.tfCantidadTotalTanques
					.setText("" + conteoDeTanque);
		}
	}

	private void updateTableEquipos(List<Equipo> items) {
		// obtener la tabla a modificar
		JTable table = createOrderFrame.tListaEquipos;
		int currentRowSelected = table.getSelectedRow();

		if (table.getColumnModel().getColumnCount() == 0) {
			TableColumn column;

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("cabeceraColumna_cantidad"));
			column.setCellRenderer(new BeanTableCellRenderer(Equipo.class,
					Equipo.CANTIDAD));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("cabeceraColumna_equipo"));
			column.setCellRenderer(new BeanTableCellRenderer(Equipo.class,
					Equipo.EQUIPO));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("cabeceraColumna_marca"));
			column.setCellRenderer(new BeanTableCellRenderer(Equipo.class,
					Equipo.MARCA));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("cabeceraColumna_modelo"));
			column.setCellRenderer(new BeanTableCellRenderer(Equipo.class,
					Equipo.MODELO));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

		}
		// Fuente de datos
		((BeanTableModel) table.getModel()).setDataVector(new Vector<Equipo>(
				items));
		table.updateUI();

		if (currentRowSelected != -1) {
			if (table.getRowCount() == 0) {

			} else if (table.getRowCount() == currentRowSelected) {
				currentRowSelected--;
			}

			if (currentRowSelected < table.getRowCount()) {

				table.setRowSelectionInterval(currentRowSelected,
						currentRowSelected);
			}
		}

		{
			int conteoDeEquipo = 0;
			for (Equipo item : items) {
				conteoDeEquipo += item.getCantidad();
			}

			createOrderFrame.tfCantidadTotalEquipos
					.setText("" + conteoDeEquipo);
		}
	}

	private void updateTableLiquidacion(List<LiquidacionItem> items) {
		// obtener la tabla a modificar
		JTable table = finalizarSolicitudFrame.tLiquidacion;
		int currentRowSelected = table.getSelectedRow();

		if (table.getColumnModel().getColumnCount() == 0) {
			TableColumn column;

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("message.cantidadEjecutada"));
			column.setCellRenderer(new BeanTableCellRenderer(
					LiquidacionItem.class, LiquidacionItem.CANTIDAD));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("message.TipoMaterial"));
			column.setCellRenderer(new BeanTableCellRenderer(
					LiquidacionItem.class, LiquidacionItem.TIPO));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("message.descripcion"));
			column.setCellRenderer(new BeanTableCellRenderer(
					LiquidacionItem.class, LiquidacionItem.DESCRIPCION));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

			column = new TableColumn();
			column.setHeaderValue(bundle.getString("message.montoEjecutado"));
			column.setCellRenderer(new BeanTableCellRenderer(
					LiquidacionItem.class, LiquidacionItem.MONTO));
			column.setCellEditor(null);
			column.setPreferredWidth(100);

			table.getColumnModel().addColumn(column);

		}
		// Fuente de datos
		((BeanTableModel) table.getModel())
				.setDataVector(new Vector<LiquidacionItem>(items));
		table.updateUI();

		if (currentRowSelected != -1) {
			if (table.getRowCount() == 0) {

			} else if (table.getRowCount() == currentRowSelected) {
				currentRowSelected--;
			}

			if (currentRowSelected < table.getRowCount()) {

				table.setRowSelectionInterval(currentRowSelected,
						currentRowSelected);
			}
		}

	}

	private void updateTableSolicitud(List<Solicitud> items) {
		try {

			JTable table = visualizeSolicitudesFrame.tOrders;
			int currentRowSelected = table.getSelectedRow();

			if (table.getSelectedRow() != -1) {
				table.removeRowSelectionInterval(currentRowSelected,
						currentRowSelected);
			}

			if (table.getColumnModel().getColumnCount() == 0) {
				TableColumn column;

				column = new TableColumn();
				column.setHeaderValue(bundle
						.getString("cabeceraColumna_numeroSolicitud"));
				column.setCellRenderer(new BeanTableCellRenderer(
						Solicitud.class, Solicitud.CODIGOSOLICITUD));
				column.setCellEditor(null);
				column.setPreferredWidth(100);

				table.getColumnModel().addColumn(column);

				column = new TableColumn();
				column.setHeaderValue(bundle
						.getString("cabeceraColumna_creador"));
				column.setCellRenderer(new BeanTableCellRenderer(
						Solicitud.class, Solicitud.SUPERVISORCOMERCIAL + "."
								+ User.FULLNAME));
				column.setCellEditor(null);
				column.setPreferredWidth(100);

				table.getColumnModel().addColumn(column);

				column = new TableColumn();
				column.setHeaderValue(bundle
						.getString("cabeceraColumna_ingenieroAsignado"));
				column.setCellRenderer(new BeanTableCellRenderer(
						Solicitud.class, Solicitud.INGENIERODT + "."
								+ User.FULLNAME));
				column.setCellEditor(null);
				column.setPreferredWidth(100);

				table.getColumnModel().addColumn(column);

				column = new TableColumn();
				column.setHeaderValue(bundle
						.getString("cabeceraColumna_tipoObra"));
				column.setCellRenderer(new BeanTableCellRenderer(
						Solicitud.class, Solicitud.TIPOSOLICITUD));
				column.setCellEditor(null);
				column.setPreferredWidth(100);

				table.getColumnModel().addColumn(column);

				column = new TableColumn();
				column.setHeaderValue(bundle
						.getString("cabeceraColumna_instalacion"));
				column.setCellRenderer(new BeanTableCellRenderer(
						Solicitud.class, Solicitud.INSTALACION));
				column.setCellEditor(null);
				column.setPreferredWidth(100);

				table.getColumnModel().addColumn(column);

				column = new TableColumn();
				column.setHeaderValue(bundle
						.getString("cabeceraColumna_razonSocial"));
				column.setCellRenderer(new BeanTableCellRenderer(
						Solicitud.class, Solicitud.RAZONSOCIAL));
				column.setCellEditor(null);
				column.setPreferredWidth(100);

				table.getColumnModel().addColumn(column);
				column = new TableColumn();
				column.setHeaderValue(bundle
						.getString("cabeceraColumna_razonComercial"));
				column.setCellRenderer(new BeanTableCellRenderer(
						Solicitud.class, Solicitud.RAZONCOMERCIAL));
				column.setCellEditor(null);
				column.setPreferredWidth(100);

				table.getColumnModel().addColumn(column);
			}

			((BeanTableModel) table.getModel())
					.setDataVector(new Vector<Solicitud>(items));
			table.updateUI();

			if (currentRowSelected != -1) {
				if (table.getRowCount() == 0) {

				} else if (table.getRowCount() == currentRowSelected) {
					currentRowSelected--;
				}

				if (currentRowSelected < table.getRowCount()) {

					table.setRowSelectionInterval(currentRowSelected,
							currentRowSelected);
				}
			}

		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			isWaiting = false;
		}
	}

	private void showDialog(JFrame frame) {
		// fram anterior

		JDialog dialog = new JDialog(currentFrame);

		dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		dialog.setModal(true);
		dialog.setTitle(frame.getTitle());
		dialog.setContentPane(frame.getContentPane());
		dialog.setSize(frame.getSize());
		dialog.setLocationRelativeTo(currentFrame);
		dialog.setResizable(frame.isResizable());

		if (frame.getDefaultCloseOperation() == JFrame.EXIT_ON_CLOSE) {
			dialog.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
			});
		}
		if (frame.getWindowListeners().length > 0) {
			for (int i = 0; i < frame.getWindowListeners().length; i++) {
				WindowListener listener = frame.getWindowListeners()[i];
				dialog.addWindowListener(listener);
			}
		}
		addEscapeToClose(dialog);

		currentFrame = dialog;
		dialog.setVisible(true);
		currentFrame = dialog.getOwner();
	}

	public static void main(String[] args) throws IOException {
		logger.info("Inicio la aplicacion");
		logger.info("Configurando entorno visual.");
		{// Establece el tema de Windows
			try {
				JFrame.setDefaultLookAndFeelDecorated(false);
				// Set cross-platform Java L&F (also called "Metal")
				UIManager.setLookAndFeel(UIManager
						.getSystemLookAndFeelClassName());
			} catch (UnsupportedLookAndFeelException e) {
				// handle exception
			} catch (ClassNotFoundException e) {
				// handle exception
			} catch (InstantiationException e) {
				// handle exception
			} catch (IllegalAccessException e) {
				// handle exception
			}

		}

		logger.info("Configurando manejador de excepciones.");
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {

			@Override
			public void uncaughtException(Thread t, Throwable e) {
				try {

					File file = new File(MessageFormat.format(
							"{0,date,yyyy_MM_dd_HH_mm_S}.log", Calendar
									.getInstance().getTime()));
					RandomAccessFile randomAccessFile = new RandomAccessFile(
							file, "rw");

					PrintWriter printWriter = new PrintWriter(file);

					e.printStackTrace(printWriter);
					printWriter.flush();
					printWriter.close();

					e.printStackTrace();

				} catch (Exception e2) {
					System.out.println("########### Error: ");
					e.printStackTrace();
				}
			}
		});

		logger.info("Cargando ApplicationContext");
		ApplicationContext applicationContext = new ApplicationContext();

		logger.info("Iniciando ApplicationContext");
		applicationContext.Start();

	}

	private void hideCurrentForm() {
		if (currentFrame != null) {

			currentFrame.setVisible(false);
		}
	}

	private void addEscapeToClose(JDialog frame) {
		String CANCEL_ACTION_KEY = "CANCEL_ACTION_KEY";
		int noModifiers = 0;
		KeyStroke escapeKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,
				noModifiers, false);
		InputMap inputMap = frame.getRootPane().getInputMap(
				JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(escapeKey, CANCEL_ACTION_KEY);
		AbstractAction cancelAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				JDialog frame = ((JDialog) ((JRootPane) e.getSource())
						.getParent());
				if (frame.getWindowListeners().length > 0) {
					int length = frame.getWindowListeners().length;
					for (int i = 0; i < length; i++) {
						WindowListener windowListener = frame
								.getWindowListeners()[i];
						try {
							windowListener.windowClosing(new WindowEvent(frame,
									0));
						} catch (Exception e2) {
						}
					}
				} else {
					frame.setVisible(false);
				}

			}
		};
		frame.getRootPane().getActionMap().put(CANCEL_ACTION_KEY, cancelAction);

	}

}
