/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventario.view.util;

import java.util.List;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

/**
 * 
 * 
 */
public class BeanTableModel extends DefaultTableModel {

    public BeanTableModel() {
        super.dataVector = new Vector();

    }
    
    

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    @Override
    public Object getValueAt(int row, int column) {
        return dataVector.get(row);
    }

    public void setDataVector(Vector dataVector) {
        if (this.dataVector == null) {
            this.dataVector = new Vector();
        }
        this.dataVector.clear();
        if (dataVector != null) {
            this.dataVector.addAll(dataVector);
            this.dataVector = dataVector;
        }else{
            setRowCount(0);
        }
    }

    @Override
    public void setDataVector(Object[][] dataVector, Object[] columnIdentifiers) {
        super.setDataVector(dataVector, columnIdentifiers);
    }

    @Override
    public void setDataVector(Vector dataVector, Vector columnIdentifiers) {
        super.setDataVector(dataVector, columnIdentifiers);
    }
}
