package com.baiken.data.xml.test;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestDAOBase {

	@Test
	public void testSave() {
		DAOUser daoUser = new DAOUser();
		User user = new User();
		user.setNombreDeUsuario("admin");
		user.setClave("secure� � <<< >><user>>>");

		daoUser.save(user);

		User temp = daoUser.find(user.getId());
		System.out.println(temp.getClave());

		assertEquals(user.getId(), temp.getId());
		assertEquals(user.getNombreDeUsuario(), temp.getNombreDeUsuario());
		assertEquals(user.getClave(), temp.getClave());
	}
	
	@Test
	public void list() throws Exception {
		DAOUser daoUser=new DAOUser();
		System.out.println(daoUser.list().size());
	}

}
