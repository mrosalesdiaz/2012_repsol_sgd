package com.inventario.dao;

import java.util.List;

import com.baiken.data.xml.DAOBase;
import com.inventario.dto.User;

public class DAOUser extends DAOBase<User> {

    public User validateUser(String userName, String password) {
        List<User> foundElements = list();

        User returnValue = null;

        for (User user : foundElements) {
            if (user.getUserName().equalsIgnoreCase(userName) && user.getPassword().equalsIgnoreCase(password)) {
                returnValue = user;
                break;
            }
        }

        return returnValue;
    }

    public boolean userNameIsValid(String userName) {
        List<User> list = list();
        boolean returnValue = true;
        for (User user : list) {
            if (user.getUserName() != null && user.getUserName().equalsIgnoreCase(userName)) {
                returnValue = false;
                break;
            }
        }
        return returnValue;
    }

}
