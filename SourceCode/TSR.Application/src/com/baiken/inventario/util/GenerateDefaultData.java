package com.baiken.inventario.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.inventario.dao.DAOCanal;
import com.inventario.dao.DAOCliente;
import com.inventario.dao.DAOConfiguracion;
import com.inventario.dao.DAOContacto;
import com.inventario.dao.DAOCuentaContable;
import com.inventario.dao.DAOEECC;
import com.inventario.dao.DAOMotivoEjecucion;
import com.inventario.dao.DAOPartida;
import com.inventario.dao.DAOSolicitud;
import com.inventario.dao.DAOUser;
import com.inventario.dao.tipo.DAOTipoEquipo;
import com.inventario.dao.tipo.DAOTipoEstadoSolicitud;
import com.inventario.dao.tipo.DAOTipoInstalacion;
import com.inventario.dao.tipo.DAOTipoMarca;
import com.inventario.dao.tipo.DAOTipoOrientacion;
import com.inventario.dao.tipo.DAOTipoSolicitud;
import com.inventario.dto.Canal;
import com.inventario.dto.Planta;
import com.inventario.dto.Cliente;
import com.inventario.dto.Configuraciones;
import com.inventario.dto.Contacto;
import com.inventario.dto.CuentaContable;
import com.inventario.dto.Eecc;
import com.inventario.dto.MotivoEjecucion;
import com.inventario.dto.Partida;
import com.inventario.dto.Solicitud;
import com.inventario.dto.User;
import com.inventario.dto.tipo.TipoEquipo;
import com.inventario.dto.tipo.TipoEstadoSolicitud;
import com.inventario.dto.tipo.TipoInstalacion;
import com.inventario.dto.tipo.TipoMarca;
import com.inventario.dto.tipo.TipoOrientacion;
import com.inventario.dto.tipo.TipoServicio;
import com.inventario.view.util.Role;

public class GenerateDefaultData {
	public static void main(String[] args) throws IOException {
		deleteDir(new File("data"));

		createUsers();
		crearDataClientes();
		crearDataCanal_centroCosto_planta();
		crearDataPartida();
		crearDataCuentaContable();
		crearDataContactos();
		crearDataConfiguraciones();

		crearDataTipoOrientacion();
		crearDataTipoInstalacion();
		crearDataTipoMarca();
		crearDataTipoEquipo();

		crearDataTipoSolicitud();
		crearDataTipoEstadoSolicitud();

		crearMotivoEjecucion();
		crearEECC();
		if (args.length > 0) {
			crearSolicitud();
		}
	}

	private static void crearEECC() {
		Eecc item = null;
		Contacto contacto;
		String[][] data = new String[][] {
				// Lima
				{ "MAGOL(Lima)", "Marco Gonzales",
						"cmagol@gmail.com; mgonzales@cmagol.com;",
						"Vanesa Sanchez", "vsanchez@cmagol.com",
						"Otilia Huaman", "ohuaman@cmagol.com" },
				//
				{ "VEA HOME(Lima)", "Ricardo Bautista",
						"ricardo.bautista@veahome.com", "Ana Trujillo",
						"ana.trujillo@veahome.com" },
				//
				{ "G&S", "Gustavo Sanchez", "gs_serviceglp@yahoo.es" },
				//
				{ "COBRA(Lima)", "Edgar Pumaricra",
						"epumaricra@cobraperu.com.pe", "Dashiel Aguirre",
						"daguirre@cobraperu.com.pe" },
				//
				{ "C&M(Lima)", "Ananda Bianchi",
						"abianchi@cmtecnic.com; abianchip@gmail.com",
						"Ingrid Bianchi", "ibianchi@cmtecnic.com" },
				//
				{ "CSI(Lima)", "Jorge Epiquien", "jepiquien@csi.com.pe",
						"Sabas Bacilio", "sbacilio@csi.com.pe" },
				//
				// Trujillo
				{ "TOTAL GAS(Trujillo)", "Julio Palomino",
						"juliototalgas@hotmail.com; julio.palomino@totalgassac.com" },
				// Chiclayo
				{ "INGINOR(Chiclayo)", "Freddy Rivas",
						"frivas@inginor.com; inginor_eirl@hotmail.com" },
				{ "TOTAL GAL", "Julio Palomino",
						"juliototalgas@hotmail.com; julio.palomino@totalgassac.com" },
				// Piura
				{ "INGINOR(Piura)", "Freddy Rivas",
						"frivas@inginor.com; inginor_eirl@hotmail.com;" },
				// Cuzco
				{ "O&M(Cuzco)", "Omar Incaroca",
						"oincaroca@oym-instalaciones.com; om_contratistas_sac@yahoo.es" },
				// Arequipa
				{ "CSI(Arequipa)", "Juan Valencia", "jvalencia@csi.com.pe",
						"Alex Rossell", "arosello@csi.com.pe" },
				// Tacna
				{ "CSI(Tacna)", "Juan Valencia", "jvalencia@csi.com.pe",
						"Alex Rossell", "arosello@csi.com.pe" },
				// Moquegua
				{ "CSI(Moquegua)", "Juan Valencia", "jvalencia@csi.com.pe",
						"Alex Rossell", "arosello@csi.com.pe" },
				// Puno
				{ "CSI(Puno)", "Juan Valencia", "jvalencia@csi.com.pe",
						"Alex Rossell", "arosello@csi.com.pe" } };
		DAOEECC dao = new DAOEECC();
		for (String row[] : data) {
			item = new Eecc();
			item.setNombre(row[0]);

			item.setResponsables(new ArrayList<Contacto>());
			for (int i = 1; i < row.length; i++) {
				contacto = new Contacto();
				contacto.setNombre(row[i++]);
				contacto.setEmail(row[i]);
				item.getResponsables().add(contacto);
			}

			dao.save(item);
		}
	}

	private static void crearMotivoEjecucion() {
		MotivoEjecucion item = null;

		String[] data = new String[] {
				//
				"Motivo Ejecucion Tipo 1",
				//
				"Motivo Ejecucion Tipo 2",
				//
				"Motivo Ejecucion Tipo 3",
				//
				"Motivo Ejecucion Tipo 4" };
		DAOMotivoEjecucion dao = new DAOMotivoEjecucion();
		for (String row : data) {
			item = new MotivoEjecucion();
			item.setNombre(row);
			dao.save(item);
		}
	}

	private static void crearSolicitud() {

		DAOSolicitud daoSolicitud = new DAOSolicitud();

		DAOUser dao = new DAOUser();
		DAOCliente daoCliente = new DAOCliente();
		DAOCanal daoCanal = new DAOCanal();
		DAOConfiguracion daoConfiguracion = new DAOConfiguracion();
		DAOTipoSolicitud daoTipoSolicitud = new DAOTipoSolicitud();
		List<User> comerciales = dao.search("/user/role/text()='"
				+ Role.RoleGerenteComercial.text + "'");
		List<User> ingenieros = dao.search("/user/role/text()='"
				+ Role.RoleIngenieroDesarrolloTecnico.text + "'");
		List<Cliente> clientes = daoCliente.search("/cliente/@id!=''");
		List<Canal> canales = daoCanal.list();
		List<TipoServicio> tipoServicios = daoTipoSolicitud.list();

		Solicitud solicitud;

		Calendar fechaEmision = Calendar.getInstance();
		for (int i = 0; i < 100; i++) {
			fechaEmision = Calendar.getInstance();
			User ingeniero = ingenieros.get(createRandom(0,
					ingenieros.size() - 1));
			User comercial = comerciales.get(createRandom(0,
					comerciales.size() - 1));
			Cliente cliente = clientes
					.get(createRandom(0, clientes.size() - 1));

			solicitud = new Solicitud();
			solicitud.setSupervisorComercial(comercial);
			solicitud.setIngenieroDT(ingeniero);
			solicitud.setRazonSocial(cliente.getRazonSocial());
			solicitud.setRazonComercial(cliente.getRazonComercial().get(
					createRandom(0, cliente.getRazonComercial().size() - 1)));
			{
				TipoServicio servicio = tipoServicios.get(createRandom(0,
						tipoServicios.size() - 1));
				Canal canal = canales.get(createRandom(0, canales.size() - 1));
				Planta centroDeCosto = canal.getPlantas().get(
						createRandom(0, canal.getPlantas().size() - 1));

				solicitud.setTipoSolicitud(servicio.getNombre());
				solicitud.setCanal(canal.getNombre());
				solicitud.setCentroCosto(centroDeCosto.getNombreCentroCosto());

				solicitud.setCodigoSolicitud(daoConfiguracion
						.generarNumeroParaTipoSolicitud(servicio.getInicial(),
								centroDeCosto.getCodigoDePlanta()));
			}
			solicitud.setInstalacion("1000" + i);
			solicitud.setRucOdni("100066" + i);

			fechaEmision.add(Calendar.DAY_OF_YEAR, -createRandom(0, 600));

			solicitud.setFechaEmision(fechaEmision.getTime());

			fechaEmision.add(Calendar.DAY_OF_YEAR, createRandom(3, 10));

			solicitud.setFechaInicioObra(fechaEmision.getTime());

			fechaEmision.add(Calendar.DAY_OF_YEAR, createRandom(20, 150));

			solicitud.setFechaFinObra(fechaEmision.getTime());

			daoSolicitud.save(solicitud);

		}

	}

	private static int createRandom(int min, int max) {
		return (int) ((max - min + 1) * Math.random() + min);
	}

	private static void crearDataTipoEstadoSolicitud() {
		TipoEstadoSolicitud item = null;

		DAOTipoEstadoSolicitud dao = new DAOTipoEstadoSolicitud();

		item = new TipoEstadoSolicitud();
		item.setNombre("message_estado0");
		item.setIndex(0);
		dao.save(item);

		item = new TipoEstadoSolicitud();
		item.setNombre("message_estado1");
		item.setIndex(1);
		dao.save(item);

		item = new TipoEstadoSolicitud();
		item.setNombre("message_estado2");
		item.setIndex(1);
		dao.save(item);

		item = new TipoEstadoSolicitud();
		item.setNombre("message_estado3");
		item.setIndex(2);
		dao.save(item);

	}

	private static void crearDataTipoEquipo() {
		TipoEquipo item = null;

		String[] data = new String[] {
				//
				"Regulador",
				//
				"Vaporizador",
				//
				"Valvula",
				//
				"Conector" };
		DAOTipoEquipo dao = new DAOTipoEquipo();
		for (String row : data) {
			item = new TipoEquipo();
			item.setNombre(row);
			dao.save(item);
		}
	}

	private static void crearDataTipoMarca() {
		TipoMarca item = null;

		String[] data = new String[] {
				//
				"Ransome",
				//
				"Tatsa",
				//
				"Triniti" };
		DAOTipoMarca dao = new DAOTipoMarca();
		for (String row : data) {
			item = new TipoMarca();
			item.setNombre(row);
			dao.save(item);
		}
	}

	private static void crearDataTipoSolicitud() {
		TipoServicio item = null;

		String[][] data = new String[][] {
				//
				{ "INSPECCION", "I" },
				//
				{ "OBREA NUEVA/AMPLIACION", "O" },
				//
				{ "MANTENIMIENTO/REINSTALACION", "M" },
				//
				{ "RETIRO", "R" },
				//
				{ "FORMALIZACION", "F" } };
		DAOTipoSolicitud dao = new DAOTipoSolicitud();
		for (String row[] : data) {
			item = new TipoServicio();
			item.setNombre(row[0]);
			item.setInicial(row[1]);
			dao.save(item);
		}
	}

	private static void crearDataTipoInstalacion() {
		TipoInstalacion item = null;

		String[] data = new String[] {
				//
				"Soterrado",
				//
				"Aereo" };
		DAOTipoInstalacion dao = new DAOTipoInstalacion();
		for (String row : data) {
			item = new TipoInstalacion();
			item.setNombre(row);
			dao.save(item);
		}
	}

	private static void crearDataTipoOrientacion() {
		TipoOrientacion item = null;

		String[] data = new String[] {
				//
				"Horizontal",
				//
				"Vertical" };
		DAOTipoOrientacion dao = new DAOTipoOrientacion();
		for (String row : data) {
			item = new TipoOrientacion();
			item.setNombre(row);
			dao.save(item);
		}
	}

	// Deletes all files and subdirectories under dir.
	// Returns true if all deletions were successful.
	// If a deletion fails, the method stops attempting to delete and returns
	// false.
	public static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}

		// The directory is now empty so delete it
		return dir.delete();
	}

	private static void crearDataConfiguraciones() {

	}

	private static void crearDataContactos() {
		Contacto item = null;

		String[] data = new String[] {
				//
				"Contacto 1",
				//
				"Contacto 2",
				//
				"Contacto 3" };
		DAOContacto daoContacto = new DAOContacto();
		for (String row : data) {
			item = new Contacto();
			item.setId(UUID.randomUUID().toString());
			item.setNombre(row);

			daoContacto.save(item);
		}

	}

	private static void crearDataPartida() {
		{// PARTIDA
			Partida item = null;

			String[][] data = new String[][] {
					//
					{ "FACTURABLE", "6220207000" },
					//
					{ "INVERSI�N (AFI)", "9930000001" },
					//
					{ "GASTO", "6220207080" } };
			DAOPartida daoPartida = new DAOPartida();
			for (String row[] : data) {
				item = new Partida();
				item.setId(UUID.randomUUID().toString());
				item.setNombre(row[0]);
				item.setCuentaContable(row[1]);
				daoPartida.save(item);

			}

		}
	}

	private static void crearDataCuentaContable() {
		{// Cuentas Contables
			CuentaContable item = null;

			String[] data = new String[] {
					//
					"6220207000 - MANTENIMIENTO Y REPARACION DE INSTALACIONES DE GAS",
					//
					"6220207080 - MANTENIMIENTO Y REPARACION DEPOSITOS EN CLIENTES",
					//
					"6220800000 - MANTENIMIENTO Y REPACION DE UNIDADES DE TRANSPORTE",
					//
					"6230000000 - SERVICIOS JURIDICOS CONTENCIOSOS",
					//
					"6230000400 - SERVICIOS DE PERSONAL CONTRATADO A TERCEROS",
					//
					"6240290000 - OTROS TRANSPORTES Y FLETES DE SUMINISTRO Y OTROS SERV.",
					//
					"6299999000 - OTROS SERVICIOS CON VALOR TRIBUTARIO",
					//
					"6299999080 - OTROS SERVICIOS SIN VALOR TRIBUTARIO",
					//
					"6280002000 - SUMINISTROS DE CARBURANTES Y CONBUSTIBLES",
					//
					"6280200000 - TELECOMUNICACIONES",
					//
					"6280300000 - MATERIAL DE OFICINA",
					//
					"6280399000 - SUMINISTROS DIVERSOS DE MATERIAL",
					//
					"6230099900 - OTROS SERVICIOS PROFESIONALES INDEPENDIENTES",
					//
					"6220100000 - MANTENIMIENTO Y REPARACION DE EDIFICIOS Y OTRAS CONSTRUCCIONES",
					//
					"6490000000 - ALIMENTACION",
					//
					"6270699000 - GASTOS DE REPRESENTACION",
					//
					"6440000000 - TRANSPORTE DE PERSONAL, LOCOMOCION KILOMETRAJE",
					//
					"6440001000 - GASTOS DE VIAJE, TRANSPORTE MEDIOS AJENOS",
					//
					"6441000000 - GASTOS DE VIAJE, ALOJAMIENTO Y ALIMENTACION",
					//
					"6449000000 - OTROS GASTOS DE VIAJE" };
			DAOCuentaContable daoCuentaContable = new DAOCuentaContable();
			for (String row : data) {
				item = new CuentaContable();
				item.setId(UUID.randomUUID().toString());
				item.setNombre(row);

				daoCuentaContable.save(item);
			}

		}
	}

	private static void crearDataCanal_centroCosto_planta() {
		{// Creando lista de canales
			Canal canal = null;

			String[][][] data = {
					//
					{ { "DC" }, { "201101", "AQP", "Planta Arequipa" },
							{ "201151", "CHI", "Planta Chicayo" },
							{ "201351", "CUS", "Planta Cuzco" },
							{ "201401", "HUA", "Planta Huancayo" },
							{ "201201", "PIU", "Planta Piura" },
							{ "201001", "PUL", "Planta Pucallpa" },
							{ "201751", "TAR", "Planta Tarapoto" },
							{ "201301", "TRU", "Planta Trujillo" },
							{ "201705", "L", "Planta Ventanilla" }, },
					//
					{ { "SI" }, { "202101", "AQP", "Planta Arequipa" },
							{ "202151", "CHI", "Planta Chicayo" },
							{ "202351", "CUS", "Planta Cuzco" },
							{ "202401", "HUA", "Planta Huancayo" },
							{ "202201", "PIU", "Planta Piura" },
							{ "202001", "PUL", "Planta Pucallpa" },
							{ "202251", "TAR", "Planta Tarapoto" },
							{ "202301", "TRU", "Planta Trujillo" },
							{ "202705", "L", "Planta Ventanilla" } },
					//
					{ { "CA" }, { "203101", "AQP", "Planta Arequipa" },
							{ "203151", "CHI", "Planta Chicayo" },
							{ "203351", "CUS", "Planta Cuzco" },
							{ "203201", "PIU", "Planta Piura" },
							{ "203001", "PUL", "Planta Pucallpa" },
							{ "203301", "TRU", "Planta Trujillo" },
							{ "203705", "L", "Planta Ventanilla" } } };
			DAOCanal daoCanal = new DAOCanal();
			for (String[][] row : data) {
				canal = new Canal();
				canal.setId(UUID.randomUUID().toString());
				canal.setNombre(row[0][0]);
				canal.setPlantas(new ArrayList<Planta>());
				Planta centroDeCosto;

				for (int i = 1; i < row.length; i++) {
					centroDeCosto = new Planta();
					centroDeCosto.setNombreCentroCosto(row[i][0]);
					centroDeCosto.setCodigoDePlanta(row[i][1]);
					centroDeCosto.setNombreDePlanta(row[i][1] + " - "
							+ row[i][2]);
					canal.getPlantas().add(centroDeCosto);
				}

				daoCanal.save(canal);
			}

		}
	}

	private static void crearDataClientes() {
		Cliente cliente = null;
		{

			String[][] data = new String[][] {
					//
					{ "100 PRE FELIZ S.A.", "100 PRE FELIZ S.A." },
					{ "ACTLABS SKYLINE PERU SAC", "ACTLABS SKYLINE PERU SAC" },
					{ "ADELA'S PRODUCTION", "HERBERT ALCANTARA RAMIREZ" },
					{ "ADMINISTRADORA DE FRANQUICIAS PERU",
							"ADMINISTRADORA DE FRANQUICIAS PERU" },
					{ "AGRO LA FLORESTA S.A.C. ", "AGRO LA FLORESTA S.A.C. " },
					{ "AGROINDUSTRIA CAMPOY SRL", "AGROINDUSTRIA CAMPOY SRL" },
					{ "AGROINDUSTRIA SANTA MARIA SAC",
							"AGROINDUSTRIA SANTA MARIA SAC" },
					{ "AGROINDUSTRIAS FLORIS", "SANTA NATURA" },
					{ "AGROPECUARIA RIO BRAVO HERMANOS SAC", "GRANJA TOPARA" },
					{ "ALBERTO VENEGAS SALCEDO", "ALBERTO VENEGAS SALCEDO" },
					{ "ALDO LUIGI CICCIA PIEROBON",
							"ALDO LUIGI CICCIA PIEROBON" },
					{ "ALFONSO BUSTAMANTE CANNY", "ALFONSO BUSTAMANTE CANNY" },
					{ "ALFREDO CARLOS PLENGUE THORNE",
							"ALFREDO CARLOS PLENGUE THORNE" },
					{ "ALFREDO PEREZ GUBBINS", "ALFREDO PEREZ GUBBINS" },
					{ "ALMERIZ S.A.", "ALMERIZ S.A." },
					{ "ANA CECILIA JOHNSON DE JORQUIERA ",
							"ANA CECILIA JOHNSON DE JORQUIERA " },
					{ "ANA DE LOS MILAGROS SALAVERRY BARR",
							"INTERCONTINENTAL DE NEGOCIOS" },
					{ "ANA MARIA COX ALVAREZ", "ANA MARIA COX ALVAREZ" },
					{ "ANDRE FRITZ FIGUEROLA BOESH",
							"ANDRE FRITZ FIGUEROLA BOESH" },
					{ "AQUA XPRESS S.A.C.", "AQUA XPRESS S.A.C." },
					{ "ARRIETA VICTORIO JULIO", "ARRIETA VICTORIO JULIO" },
					{ "ARTESANIAS MON REPOS S.A.", "ALPACA MON REPOS" },
					{ "ASA ALIMENTOS", "ASA ALIMENTOS" },
					{ "ASOCIACION CIVIL SANTA MARIA",
							"ASOCIACION CIVIL SANTA MARIA" },
					{ "ASOCIACION MUSEO DE ARTE LIMA",
							"ASOCIACION MUSEO DE ARTE LIMA" },
					{ "ASOCIACION ORGANIZACION INTERNACIONAL",
							"NUEVA ACROPOLIS" },
					{
							"ASOCIACION PARA LA FORMCION HUMANA Y CAPACIDAD TECNICA PROHUMTEC",
							"ASOCIACION PARA LA FORMCION HUMANA Y CAPACIDAD TECNICA PROHUMTEC" },
					{ "ATALA MITRI SHEHADEH", "ATALA MITRI SHEHADEH" },
					{ "AVICOLA RIO AZUL", "GRANJA DAVILA", "GRANJA PACHACAMAC" },
					{ "AVINKA", "GRANJA JF3", "GRANJA JF4" },
					{ "AVINKA - GRANJA GALESA 1", "AVINKA - GRANJA GALESA 1" },
					{ "AVINKA - GRANJA GALESA 2", "AVINKA - GRANJA GALESA 2" },
					{ "AVINKA - GRANJA GALESA 3", "AVINKA - GRANJA GALESA 3" },
					{ "AVINKA - GRANJA GALESA 4", "AVINKA - GRANJA GALESA 4" },
					{ "AVINKA - GRANJA JF01", "AVINKA - GRANJA JF01" },
					{ "AVINKA - GRANJA JF02", "AVINKA - GRANJA JF02" },
					{ "AVINKA - GRANJA RIO GRANDE",
							"AVINKA - GRANJA RIO GRANDE" },
					{ "AVINKA - GRANJA ROCIO 1(EX PL-239)",
							"AVINKA - GRANJA ROCIO 1(EX PL-239)" },
					{ "AVINKA - GRANJA ROCIO 2(EX PL-240)",
							"AVINKA - GRANJA ROCIO 2(EX PL-240)" },
					{ "AVINKA - GRANJA ROCIO 2(EX PL-246)",
							"AVINKA - GRANJA ROCIO 2(EX PL-246)" },
					{ "BARRIOS DE SARDON LILY", "BARRIOS DE SARDON LILY" },
					{ "BEMBOS S.A.C.", "BEMBOS - SAN BORJA", "BEMBOS S.A.C." },
					{ "BITUMENES DEL PERU SAC", "BITUPER SAC" },
					{ "BLAS CANO VICTOR NOLASCO", "BLAS CANO VICTOR NOLASCO" },
					{ "BODEGA SOTELO SAC", "BODEGA SOTELO SAC" },
					{ "BODEGAS VISTA ALEGRE", "BODEGAS VISTA ALEGRE" },
					{ "BRAVO RESTOBAR SAC", "BRAVO RESTOBAR " },
					{ "BRUNO ORLANDINI ALVAREZ CALDERON",
							"BRUNO ORLANDINI ALVAREZ CALDERON" },
					{ "CACERES LLANTOY DONATO", "CACERES LLANTOY DONATO" },
					{ "CALIXTO FLORES ACU�A", "CALIXTO FLORES ACU�A" },
					{ "CAMAL CONCHUCOS", "CAMAL CONCHUCOS" },
					{ "CAMPO REAL S.A.C.", "CAMPO REAL S.A.C." },
					{ "CARLOS HUGO AGUIRRE GONZALES",
							"CARLOS HUGO AGUIRRE GONZALES" },
					{ "CAROLINA AMPARO SANCHEZ ROJAS",
							"CAROLINA AMPARO SANCHEZ ROJAS" },
					{ "CARTONES VILLA MARINA", "CARTONES VILLA MARINA" },
					{ "CATERING BUS", "CATERING BUS" },
					{ "CELINDA SUSANA WONG KIT CHING",
							"CELINDA SUSANA WONG KIT CHING" },
					{ "CENTRO DE REFORMA DE VIDA SANA S.R.",
							"CENTRO DE REFORMA DE VIDA SANA S.R." },
					{ "CEREALES DEL SUR SRL", "CEREALES DEL SUR SRL" },
					{ "CESAR HUARIPATA HUACCHA", "CESAR HUARIPATA HUACCHA" },
					{ "CHEIRO VERDE EIRL", "CHEIRO VERDE EIRL" },
					{ "CHIFA HOO WA S.A.", "CHIFA HOO WA S.A." },
					{ "CIA MINAS BUENAVENTURA", "CIA MINAS BUENAVENTURA" },
					{ "CIA MINERA AGREGADOS CALCAREOS S.A.", "COMACSA" },
					{ "CINDEL TAIPE", "CHILIS" },
					{ "CLINICA INTERNACIONAL", "CLINICA INTERNACIONAL" },
					{ "CLINICA INTERNACIONAL S.A.",
							"CLINICA INTERNACIONAL S.A." },
					{ "CLUB DE REGATAS LIMA", "FOOD COURT",
							"LA DIVINA SENSACION", "SEDE CANTUTA",
							"SEDE CHORRILLOS", "SEDE LA CANTUTA" },
					{ "COMFER S.A.", "COMFER S.A." },
					{ "COMPA��A MINERA CONDESTABLE S,A,A,", "APC CORPORACION" },
					{ "CONCESIONES BBUEN SABOR S.A.C.",
							"CONCESIONES BBUEN SABOR S.A.C." },
					{ "COQUIS CHIOZA EDUARDO", "COQUIS CHIOZA EDUARDO" },
					{ "CORPORACION DE INVERSIONES COMERCIALES",
							"CORPORACION DE INVERSIONES COMERCIALES" },
					{ "CORPORACION PERUANA DE RESTAURANTES", "PAPA JHONS" },
					{ "CORPORACION PLEYADES SAC", "COLISEO LA PUNTA" },
					{ "CORPORACION SYZARD", "TORTAS GABY" },
					{ "CORPORACION TEXTIL S.A.", "CORTESA" },
					{ "CORPORACION TURISTICA PERUANA ", "CASINO ATLANTIC CITY" },
					{ "COUNTRY CLUB VILLA", "ASIA", "CHORRILLO" },
					{ "CREDITO PUENTE S,A,", "EDIFICIO CERROS DE CAMACHO 655" },
					{ "CREMATORIO ECOLOGICO EL MANANTIAL SRL",
							"CREMATORIO ECOLOGICO EL MANANTIAL SRL" },
					{ "CRISTIAN LAUB BENAVIDES", "CRISTIAN LAUB BENAVIDES" },
					{ "DANICA SAC", "DANICA SAC" },
					{ "DAVID PRADO MOSCOSO", "DAVID PRADO MOSCOSO" },
					{ "DAVID SAETTONE WATMOUGH", "DAVID SAETTONE WATMOUGH" },
					{ "DE COSSIO DE ASIN GONZALO CARLOS",
							"DE COSSIO DE ASIN GONZALO CARLOS" },
					{ "DELOSI S.A.", "BENAVIDES", "CALLAO",
							"KFC/PH - SAN LUIS", "KFC LA PLANICIE", "TELLO" },
					{ "DIEGO ROSALES", "DIROSE SAC" },
					{ "DINA ROSA QUISPE VASGAS DE GALIANO",
							"DINA ROSA QUISPE VASGAS DE GALIANO" },
					{ "DON PAN SAC", "DON PAN SAC" },
					{ "DYP CONTRATISTAS SAC", "DYP CONTRATISTAS SAC" },
					{ "EBA S.A.", "EBA S.A." },
					{ "EDGARD LOAYZA FIGUEROA", "EDGARD LOAYZA FIGUEROA" },
					{ "EDIFICIO ALFREDO SALAZAR", "EDIFICIO ALFREDO SALAZAR" },
					{ "EDIFICIO ALVAREZ CALDERON 650",
							"EDIFICIO ALVAREZ CALDERON 650" },
					{ "EDIFICIO BALTAMAR", "EDIFICIO BALTAMAR" },
					{ "EDIFICIO BERTOLOTTO", "EDIFICIO BERTOLOTTO" },
					{ "EDIFICIO BUENOS AIRES 244 ",
							"EDIFICIO BUENOS AIRES 244 " },
					{ "EDIfICIO CARLOS SANCHEZ  EX CALLE 3 MZ. G  LT. 10 ",
							"EDIfICIO CARLOS SANCHEZ  EX CALLE 3 MZ. G  LT. 10 " },
					{ "EDIFICIO CERROS DE CAMACHO 481",
							"EDIFICIO CERROS DE CAMACHO 481" },
					{ "EDIFICIO DEL SUR - URIEL", "EDIFICIO DEL SUR - URIEL" },
					{ "EDIFICIO DEL SUR ESQ CON MONTE REAL",
							"EDIFICIO DEL SUR ESQ CON MONTE REAL" },
					{ "EDIFICIO EL SOL", "EDIFICIO EL SOL" },
					{
							"EDIFICIO ISLAS DE SAN PEDRO EDIFICIO ISLAS DE SAN PEDRO ",
							"EDIFICIO POMPEYA" },
					{ "EDIFICIO JUNIN 205", "EDIFICIO JUNIN 205 (AL MAR)" },
					{ "EDIFICIO LA PRADERA", "EDIFICIO LA PRADERA" },
					{ "EDIFICIO LAS DALIAS 180", "EDIFICIO LAS DALIAS 180" },
					{ "EDIFICIO LOS TIAMOS 260", "EDIFICIO LOS TIAMOS 260" },
					{ "EDIFICIO MALECON DE LA MARINA",
							"EDIFICIO MALECON DE LA MARINA" },
					{ "EDIFICIO MANUEL TOVAR", "EDIFICIO MANUEL TOVAR" },
					{ "EDIFICIO MONTEFLOR 200", "EDIFICIO MONTEFLOR 200" },
					{ "EDIFICIO PAUL HARRIS ", "EDIFICIO PAUL HARRIS " },
					{ "EDIFICIO QUI�ONES 141", "EDIFICIO QUI�ONES 141" },
					{ "EDIFICIO SAN BORJA SUR 863", "RUDY MANUEL WONG YONG" },
					{ "EDIFICIO TORRES DE SAN BORJA",
							"EDIFICIO TORRES DE SAN BORJA" },
					{ "EDIFICIOS Y CONST SANTA PATRICIA S.",
							"HOTEL SOL DE ORO " },
					{ "EDO SUSHI BAR SAC", "EDO SUSHI BAR SAC" },
					{ "EDUARDO RODRIGO CARDENAS", "EDUARDO RODRIGO CARDENAS" },
					{ "EFADA EXPORT SAC", "INDUSTRIA ALIMENTARIA" },
					{ "EL GRAN ROSEDAL EIRL", "EL GRAN ROSEDAL EIRL" },
					{ "EL PACIFICO PERUANO SUIZA CIA SEG. ",
							"EMPRESA DE SEGUROS" },
					{ "EL PADRINO CHICKEN SRL", "EL PADRINO 4" },
					{ "EL PATIO S.A.C.", "EL PATIO S.A.C." },
					{ "EL SOL DE SAYAN", "EL SOL DE SAYAN" },
					{ "ELSA CRUZ SILVA", "ELSA CRUZ SILVA" },
					{ "EMPRESA DE INVERSIONES RENACER 2005",
							"EMPRESA DE INVERSIONES RENACER 2005" },
					{ "EMTUR EL SOL SRL", "EMTUR EL SOL SRL" },
					{ "ENOTRIA SA", "EMILIO LLOSA GARCIA", "ENOTRIA S.A." },
					{ "ENRRIQUE CALMET", "LAVANDERIA CAMINOS DEL INCA" },
					{ "EUROGROUP S.A.C.", "EUROGROUP S.A.C." },
					{ "F & PATRIES Y SAVOURIES", "F & PATRIES Y SAVOURIES" },
					{ "F.V. MINERA S.A.C. ", "F.V. MINERA S.A.C. " },
					{ "FABRICA DE TEJIDOS SAN CARLOS SAC",
							"FABRICA DE TEJIDOS SAN CARLOS SAC" },
					{ "FARID EISSA HAS ABDELMASIO HAMIDEH",
							"FARIO EISSA HAS ABDELMASIO HAMIDEH" },
					{ "FELICIANO CUBA VALERIANO", "FELICIANO CUBA VALERIANO" },
					{ "FERNANDO BRUNO CHIAPPE BARCHI",
							"FERNANDO BRUNO CHIAPPE BARCHI" },
					{ "FERNANDO DASSO MONTERO", "FERNANDO DASSO MONTERO" },
					{ "FLORMIRA ROJAS SALAZAR", "FLORMIRA ROJAS SALAZAR" },
					{ "FORSAC PERU SA.", "FORSAC S.A." },
					{ "FRANCISCA FIDENCIA SEGUNDO JULCA",
							"FRANCISCA FIDENCIA SEGUNDO JULCA" },
					{ "FRANCISCO JOSE SAN MARTIN TUDELA",
							"FRANCISCO JOSE SAN MARTIN TUDELA" },
					{ "FREDY MARCIAL MATURRANO MEZA",
							"FREDY MARCIAL MATURRANO MEZA" },
					{ "FUERZA AEREA DEL PERU- CMONT",
							"FUERZA AEREA DEL PERU- CMONT" },
					{ "FUNDACION PARA EL DESARROLLO AGRARIO",
							"FUNDACION PARA EL DESARROLLO AGRARIO" },
					{
							"FUNDACION PARA EL DESARROLLO INTEGRAL DE NUEVO PACHACUTEC",
							"FUNDACION PARA EL DESARROLLO INTEGRAL DE NUEVO PACHACUTEC" },
					{ "GALLETERA PATARO S.A.", "GALLETERA PATARO S.A." },
					{ "GARRIAZO CHALCO DONANCIANO JUAN",
							"GARRIAZO CHALCO DONANCIANO JUAN" },
					{ "GATE GOURMET PERU", "GATE GOURMET PERU" },
					{ "GIMNASIO LARCOMAR", "GIMNASIO LARCOMAR" },
					{ "GLADYS ANTONIA PE�A APARCANA",
							"GLADYS ANTONIA PE�A APARCANA" },
					{ "GLADYS MALUYE MUSIRIS SIMON",
							"GLADYS MALUYE MUSIRIS SIMON" },
					{ "GLICERIO SEGUNDO ALTAMIRANO COLAN",
							"GLICERIO SEGUNDO ALTAMIRANO COLAN" },
					{ "GOBESA INGENIEROS SAC", "GOBESA INGENIEROS SAC" },
					{ "GONZALO DE LAS CASAS DIEZ CANSECO",
							"GONZALO DE LAS CASAS DIEZ CANSECO" },
					{ "GONZALO JOSE GULMAN MARTINEZ",
							"GONZALO JOSE GULMAN MARTINEZ" },
					{ "GONZALO RIOS BARRETO", "CONDOMINIO ORION 230" },
					{ "GRAFICA PUBLI INDUSTRIA EIRL",
							"GRAFICA PUBLI INDUSTRIA EIRL" },
					{ "GUELMYS ROJAS TERRE", "RESTAURANT CAFETERIA ACHE CUBA" },
					{ "GUIDA GASTELUMENDI", "GUIDA GASTELUMENDI" },
					{ "GUIDO DEL CASTILLO ECHEGARAY",
							"GUIDO DEL CASTILLO ECHEGARAY" },
					{ "GUILLERMO LI SAC", "GUILLERMO LI SAC" },
					{ "HECTOR REVILLA E HIJOS S.A.C.",
							"HECTOR REVILLA E HIJOS S.A.C." },
					{ "HELATONY'S S.A.C.", "FABRICA DE HELADOS" },
					{ "HENRY FRITZ SPRINCKMOLLER", "HENRY FRITZ SPRINCKMOLLER" },
					{ "HILDA ELIZABETH ALVAREZ CHACA",
							"HILDA ELIZABETH ALVAREZ CHACA" },
					{ "HOTELES SHERATON DEL PERU S.A.",
							"HOTELES SHERATON DEL PERU S.A." },
					{ "HUAMAN SOLIS MARIA HELENA", "HUAMAN SOLIS MARIA HELENA" },
					{ "HUAMAN YUCRA BENANCIO", "HUAMAN YUCRA BENANCIO" },
					{ "HUARAZ SOLANO WALTER MAXIMO",
							"HUARAZ SOLANO WALTER MAXIMO" },
					{ "I.E. FRANKLIN DELANO ROOVELT",
							"I.E. FRANKLIN DELANO ROOVELT" },
					{ "IGNACIO DE LOYOLA LESCANO FRANCIA",
							"IGNACIO DE LOYOLA LESCANO FRANCIA" },
					{ "INDUSTRIAS ALIMENTARIAS D' JULIA",
							"INDUSTRIAS ALIMENTARIAS D' JULIA" },
					{ "INDUSTRIAS NATURALES DEL CAMPO", "FRICA",
							"INDUSTRIAS NATURALES DEL CAMPO" },
					{ "INDUSTRIAS SG SAC", "FABRICA DE METALES" },
					{ "INGENIERIA DE LA CONSTRUCCION - INGECO",
							"EDIFICIO SAN BORJA NORTE 459" },
					{ "INMOBILIARIA BENSA S.A.", "EDIFICIO GOLF PLAZA" },
					{ "INMOBILIARIA CAMACHO S.A.", "EDIFICIO LA FLORESTA II" },
					{ "INMOBILIARIA PILCAM S.A.", "CECILIA CLIMPLER" },
					{ "INSTITUTO ORG. NEUROLOGICO TO�ITO SILVA",
							"INSTITUTO ORG. NEUROLOGICO TO�ITO SILVA" },
					{ "INTIAGRO SAC", "INTIAGRO SAC" },
					{ "INVERSIONES EIVISSA", "INVERSIONES EIVISSA" },
					{ "INVERSIONES FEPAK", "RESTAURANTE LA ROMANTICA" },
					{ "INVERSIONES JIALSA S.A.C.", "INVERSIONES JIALSA S.A.C." },
					{ "INVERSIONES KIYASHI", "INVERSIONES KIYASHI" },
					{ "INVERSIONES LA BRE�A S.A.C.",
							"INVERSIONES LA BRE�A S.A.C." },
					{ "INVERSIONES MAJORO SAC", "HOTEL MAJORO" },
					{ "INVERSIONES MOSCOL SAC", "INVERSIONES MOSCOL SAC" },
					{ "INVERSIONES PELENGUE S.A.C.",
							"INVERSIONES PELENGUE S.A.C." },
					{ "INVERSIONES RAFAELA S.A.C.",
							"EDIFICIO JOSE QUI�ONES 141" },
					{ "INVERSIONES ROMA SAC", "PIZZERIA DON ROSALIO" },
					{ "INVERSIONES SAN BORJA", "MOLINA PLAZA" },
					{ "INVERSIONES VALMAR ", "EDIFICIO MANUEL VILLARAN 284" },
					{ "INVERSIONES Y TURISMO S.A.", "HOTEL LAS DUNAS",
							"LAS DUNAS HOTEL" },
					{ "ISAAC CARLOS MO�OA MONTOYA",
							"ISAAC CARLOS MO�OA MONTOYA" },
					{ "ISMAEL OMAR QUINTANA CHAVEZ PUELLES",
							"ISMAEL OMAR QUINTANA CHAVEZ PUELLES" },
					{ "ITA CELESTINA ACEVEDO BERRIO",
							"ITA CELESTINA ACEVEDO BERRIO" },
					{ "ITALA MARIANA ROJAS ALVAREZ",
							"ITALA MARIANA ROJAS ALVAREZ" },
					{ "IZQUIERDO GONZALES TERESA AIDA",
							"IZQUIERDO GONZALES TERESA AIDA" },
					{ "J. CARRASCO & CIA SAC ", "J. CARRASCO & CIA SAC " },
					{ "JACK COHEN GRIMBERG", "JACK COHEN GRIMBERG" },
					{ "JACOB RICHARD SUZANNE URTECHO",
							"JACOB RICHARD SUZANNE URTECHO" },
					{ "JEAN PAUL CROQUET URBINA", "JEAN PAUL CROQUET URBINA" },
					{ "JERI QUISPE CESAREO", "JERI QUISPE CESAREO" },
					{ "JHONNY SALVADOR ANGULO RUEGGEBERG",
							"JHONNY SALVADOR ANGULO RUEGGEBERG" },
					{ "JOHN EVANS GUERRA", "JOHN EVANS GUERRA" },
					{ "JORGE JOSE SIMON KASSIS", "JORGE JOSE SIMON KASSIS" },
					{ "JORGE LUIS MURGURUZA GUERRERO",
							"JORGE LUIS MURGURUZA GUERRERO" },
					{ "JORGE MAXIMO DEL BUSTO HERRERA",
							"JORGE MAXIMO DEL BUSTO HERRERA" },
					{ "JOSCELYNE HOYOS SULLCAHUAMAN",
							"JOSCELYNE HOYOS SULLCAHUAMAN" },
					{ "JOSE AUGUSTO ACKER FLORES", "JOSE AUGUSTO ACKER FLORES" },
					{ "JOSE CHUWQUILLANQUI YAMAMOTO",
							"JOSE CHUWQUILLANQUI YAMAMOTO" },
					{ "JOSE GONZALO GUERRERO CACERES", "PANADERIA  " },
					{ "JOSE SAM CAM ", "JOSE SAM CAM" },
					{ "JUAN CARLOS COJAL TRINIDAD",
							"JUAN CARLOS COJAL TRINIDAD" },
					{ "JULIAN VILLAFUERTE", "PANADERIA PASTELERIA LUPE" },
					{ "JUNTA DE PROPIETARIOS EDIFICIO LA EMBAJADA",
							"EDIFICIO LAS MAGNOLIAS 252" },
					{ "K.J. QUINN S.A.", "INDUSTRIA DE CUERO" },
					{ "KOTRAPERU SAC", "KOTRAPERU SAC" },
					{ "L.H. FOOD S.A.C.", "L.H. FOOD S.A.C." },
					{ "L.H. SAN MIGUEL S.A.C.", "L.H. SAN MIGUEL S.A.C." },
					{ "LA CANCHA WEBERBAUER S.A.C.",
							"LA CANCHA WEBERBAUER S.A.C." },
					{ "LA ESQUINA DE SAN ANTONIO SAC",
							"LA ESQUINA DE SAN ANTONIO SAC" },
					{ "LA LIGURIA", "LA LIGURIA" },
					{ "LA MEMORIA DEL SABOR SAC", "LA MEMORIA DEL SABOR SAC" },
					{ "LABORATORIOS KAITA DEL PERU S.A.",
							"LABORATORIOS KAITA DEL PERU S.A." },
					{ "LAP - HOTEL COSTA DEL SOL", "HOTEL COSTA DEL SOL" },
					{ "LAS LOMAS SAC", "EDIFICIO BOLOGNESI 364" },
					{ "LAZARO DASMACENO CANO", "LAZARO DASMACENO CANO" },
					{ "LEONIDAS JORGE ENRIQUE PONCE ESPINOZA",
							"LEONIDAS JORGE ENRIQUE PONCE ESPINOZA" },
					{ "LIQUID COMNPANY SAC", "LIQUID COMNPANY SAC" },
					{ "LUIS EDUARDO ROMERO ANTOLA",
							"LUIS EDUARDO ROMERO ANTOLA" },
					{ "LUIS JOSE GIOVE MU�OZ", "LUIS JOSE GIOVE MU�OZ" },
					{ "LUIS MARTIN CHIOK GONZALES",
							"LUIS MARTIN CHIOK GONZALES" },
					{ "LUIS RENAN MOYANO MENDOZA", "LUIS RENAN MOYANO MENDOZA" },
					{ "LUZ MARIELA PE�A GARGATE", "LUZ MARIELA PE�A GARGATE" },
					{ "MADERERA TRANSFORESTAL ", "MADERERA TRANSFORESTAL " },
					{ "MALASQUEZ PALOMINO JAIME RAUL",
							"MALASQUEZ PALOMINO JAIME RAUL" },
					{ "MAPECSA", "PANADERIA TUTO DELIZE" },
					{ "MARIA INES DUCASSI", "MARIA INES DUCASSI" },
					{ "MARIO BARRIOS FERRERO", "MARIO BARRIOS FERRERO" },
					{ "MARROQUIN POZO WILFREDO", "MARROQUIN POZO WILFREDO" },
					{ "MARTHA ISABEL PEREZ REYES MOYANO",
							"MARTHA ISABEL PEREZ REYES MOYANO" },
					{ "MARVA SAC", "INDUSTRIA" },
					{ "MAURO EUGENIO PAUCAR RIVERA",
							"MAURO EUGENIO PAUCAR RIVERA" },
					{ "MAYRA CORINA QUEVEDO ESPINOZA DE DEVIDE",
							"MAYRA CORINA QUEVEDO ESPINOZA DE DEVIDE" },
					{ "MELANIA ABUSADA MATTAR", "MILLTEX PERU S.A.C." },
					{ "MERLIN DE ORO SRL", "MERLIN DE ORO SRL" },
					{ "MICHELLE MARIA NAVARRO GRAU DYEV",
							"MICHELLE MARIA NAVARRO GRAU DYEV" },
					{ "MIGUEL ANGEL MORALES ROMERO",
							"MIGUEL ANGEL MORALES ROMERO" },
					{ "MIGUEL TEO LEON ITURRITEGUI",
							"MIGUEL TEO LEON ITURRITEGUI" },
					{ "MINING GROUP S.A.", "MINING GROUP S.A." },
					{ "MINISOL CHOSICA", "MINISOL CHOSICA" },
					{ "MINISOL DE SAN JUAN DE MIRAFLORES",
							"MINISOL DE SAN JUAN DE MIRAFLORES" },
					{ "MOLDEX SAC", "MOLDEX SAC" },
					{ "MOLINERA DEL CENTRO", "MOLINERA DEL CENTRO" },
					{ "MOLITALIA S.A.", "MOLITALIA S.A." },
					{ "MP & SERVICIOS CONEXOS EIRL",
							"MP & SERVICIOS CONEXOS EIRL" },
					{ "MU�OZ ABANTO MARIA ELENA", "MU�OZ ABANTO MARIA ELENA" },
					{ "NANCY ESTELA FERNANDEZ RODRIGUEZ",
							"NANCY ESTELA FERNANDEZ RODRIGUEZ" },
					{ "NAVARRO NEYRA PEDRO SIMON", "NAVARRO NEYRA PEDRO SIMON" },
					{ "NEGOCIACIONES INDUSTRIALES Y ACABADOS",
							"NEGOCIACIONES INDUSTRIALES Y ACABADOS" },
					{ "NEWS CAF�", "MOLINA PLAZA - PLAZA VEA" },
					{ "NORIS ROSSINA CORNEJO DE BONA",
							"NORIS ROSSINA CORNEJO DE BONA" },
					{ "OBREGON MILLA GUILLERMO PROSPERO",
							"OBREGON MILLA GUILLERMO PROSPERO" },
					{ "OLIVER & MICHELLE RESTAURANTES SAC",
							"OLIVER & MICHELLE RESTAURANTES SAC" },
					{ "ONASA  ", "SAN PEDRO" },
					{ "ONASA SANTA ROSA", "ONASA SANTA ROSA" },
					{ "OPERACIONES ARCOS DORADOS DEL PERU SAC",
							"MC DONALDS CAMACHO" },
					{ "ORIENT EXPRESS PERU S,A,", "MIRAFLORES PARK HOTEL" },
					{ "OSCAR DAMIAN BARRENECHEA", "OSCAR DAMIAN BARRENECHEA" },
					{ "OSCAR GONZALES MOIX", "OSCAR GONZALES MOIX" },
					{ "PABLO JAVIER TAPIA PUENTE ARNAO",
							"PABLO JAVIER TAPIA PUENTE ARNAO" },
					{ "PACCU S.A.", "PACCU S.A." },
					{ "PAMOLSA S.A.", "PAMOLSA S.A." },
					{ "PANADERIA ADDERLY EIRL", "PANADERIA ADDERLY EIRL" },
					{ "PANADERIA PASTELERIA Y BODEGA NI�O JESUS DE PRAGA SRL",
							"PANADERIA PASTELERIA Y BODEGA NI�O JESUS DE PRAGA SRL" },
					{ "PANADERIA SAN JORGE PLANTA NUEVA",
							"PANADERIA SAN JORGE PLANTA NUEVA" },
					{ "PANADERIA Y PASTELERIA MARITZA",
							"PANADERIA Y PASTELERIA MARITZA" },
					{ "PANADERIA Y PASTELERIA YULY EIRL",
							"PANADERIA Y PASTELERIA YULY EIRL" },
					{ "PANIFICADORA A&A SAC", "PANIFICADORA A&A SAC" },
					{ "PARQUE CANEPA FOOD COURT",
							"INVERSIONES LA SETIMA DE GAMARRA" },
					{ "PATIO DE COMIDAS ICA", "PATIO DE COMIDAS ICA" },
					{ "PAULINA YARANGA MAMANI", "PAULINA YARANGA MAMANI" },
					{ "PAULINE FERRERO DELGADO", "PAULINE FERRERO DELGADO" },
					{ "PEDRO BRESCIA", "PEDRO BRESCIA" },
					{ "PEDRO BUSTAMANTE TESTINO", "PEDRO BUSTAMANTE TESTINO" },
					{ "PEDRO MARTIN PUENTE ARNAO", "PEDRO MARTIN PUENTE ARNAO" },
					{ "PESQUERA HAYDUK", "PESQUERA HAYDUK" },
					{ "PESQUERA JADA", "PESQUERA JADA" },
					{ "PLANTONES CAJAMARQUILLA", "PLANTONES CAJAMARQUILLA" },
					{ "PLASMIAGRO THE PERU", "PLASMIAGRO THE PERU" },
					{ "POLDI MOISES WAISMAN MOLINA",
							"POLDI MOISES WAISMAN MOLINA" },
					{ "POLLOS SERVICE EXPRESS - GRANJA WEREQUEQUE",
							"POLLOS SERVICE EXPRESS - GRANJA WEREQUEQUE" },
					{ "PRODUCTOS NATURALES DE LA SOYA SAC", "PRONASOYA" },
					{ "PRODUCTOS QUIMICOS ESENCIALES S.A.C.",
							"PRODUCTOS QUIMICOS ESENCIALES S.A.C." },
					{ "PROMOTORA MIRAFLORES", "LE CORDON BLEU" },
					{ "PUNTO BLANCO ACUATICO SAC", "PUNTO BLANCO ACUATICO SAC" },
					{ "RADIADORES FORTALEZA", "RADIADORES FORTALEZA" },
					{ "RAFAEL CORZO DE LA COLINA", "RAFAEL CORZO DE LA COLINA" },
					{ "RAFAEL FRANCISCO VILLANUEVA MERINO",
							"RAFAEL FRANCISCO VILLANUEVA MERINO" },
					{ "RAUL SALVADOR FRANCO CASTILLA",
							"RAUL SALVADOR FRANCO CASTILLA" },
					{ "REDONDOS", "GRANJA GRAMADAL" },
					{ "REDONDOS  S.A.", "GRANJA TRES PALOS" },
					{ "REDONDOS CESAR 2 (GRANJA PEQUE�A)",
							"REDONDOS CESAR 2 (GRANJA PEQUE�A)" },
					{ "REDONDOS ESPERANZA", "REDONDOS ESPERANZA" },
					{ "REDONDOS S.A.", "ALBUFERA A", "ALBUFERA B",
							"ALBUFERA C", "BERMEJO 4", "CESAR 2", "CLAUDIA 1",
							"CLAUDIA 2", "ESPERANZA", "GRAMADAL",
							"GRANJA CESAR 2", "GRANJA ESPERANZA",
							"GRANJA GEMINIS", "GRANJA LEO", "GRANJA ROCCO" },
					{ "REDONDOS S.A.  ", "GRANJA JFA" },
					{ "REDONDOS S.A. - GRANJA JF B",
							"REDONDOS S.A. - GRANJA JF B" },
					{ "RENAN MENESES ARCINIEGA", "RENAN MENESES ARCINIEGA" },
					{ "RESTAURANT MC GRILL EIRL", "RESTAURANT MC GRILL EIRL" },
					{ "RICARDO YNGA PAIPAY", "RICARDO YNGA PAIPAY" },
					{ "RIVEROS TACO ALBERTO", "RIVEROS TACO ALBERTO" },
					{ "ROBERTO GALVEZ PIZARRO", "ROBERTO GALVEZ PIZARRO" },
					{ "ROMULO TRIVE�O PINTO", "ROMULO TRIVE�O PINTO" },
					{ "ROSEMARY EVELYN REYES ALVA",
							"ROSEMARY EVELYN REYES ALVA" },
					{ "ROSYBET", "PANADERIA", "ROSYBET" },
					{ "ROTOMOLDEADOS PERUANOS S.A.C.",
							"ROTOMOLDEADOS PERUANOS S.A.C." },
					{ "RUBEN GAMARRA POMA", "RUBEN GAMARRA POMA" },
					{ "RVV GERENCIA Y CONSTRUCCION SAC",
							"EDIFICIO JACINTO LARA" },
					{ "RYC CONSTRUCTORES", "EDIFICIO ALVAREZ CALDERON 209" },
					{ "SACOS PISCO", "SACOS PISCO" },
					{ "SALOMON NIEGO BRAUN", "SALOMON NIEGO BRAUN" },
					{ "SAN FERNADO S.A.", "PLANTEL 211" },
					{ "SAN FERNANDO", "PLANTEL 154", "PLANTEL 210",
							"PLANTEL 260", "CABEZA DE TORO", "PLANTEL 211",
							"PLANTEL 254", "PLANTEL 120", "PLANTEL 125",
							"PLANTEL 128", "PLANTEL 205", "PLANTEL 206",
							"PLANTEL 208", "PLANTEL 212", "PLANTEL 259",
							"PLANTEL 261", "PLANTEL 262", "PLANTEL 264",
							"PLANTEL 265", "PLANTEL 277", "PLANTEL 298" },
					{ "SAN FERNANDO PLANTEL 208", "SAN FERNANDO PLANTEL 208" },
					{ "SAN FERNANDO S.A.", "PAVI 04", "PLANTEL 131" },
					{ "SAN IGNACIO S.A.", "SAN IGNACIO S.A." },
					{ "SANTIAGO GAMARRA CASTA�EDA",
							"SANTIAGO GAMARRA CASTA�EDA" },
					{ "SEA FOOD TRAIDING S.A.", "SEA FOOD TRAIDING S.A." },
					{ "SEMINARIO PIZZORNI CARLOS", "SEMINARIO PIZZORNI CARLOS" },
					{ "SEVERINO CALZADO JUSTO", "SEVERINO CALZADO JUSTO" },
					{ "SF ALMACIGOS", "SF ALMACIGOS" },
					{ "SILVERIO PABLO GALLARDO ", "SILVERIO PABLO GALLARDO " },
					{ "SINERGIA SOLUCIONES ADMINISTRATIVAS", "SINERGIA" },
					{ "SOLOGUREN MARTINEZ WILFREDO ",
							"SOLOGUREN MARTINEZ WILFREDO " },
					{ "STUDIO BUZZ S.A.C.", "STUDIO BUZZ S.A.C." },
					{ "SUPERMERCADOS PERUANOS", "CHOSICA", "COMAS",
							"LIMATAMBO - PAPA JHONS", "PLAZA VEA COMAS",
							"PLAZA VEA LA BOLICHERA - KFC",
							"PLAZA VEA LIMATAMBO", "SANTA ISABEL CORPAC" },
					{ "SUSANA MILAGROS CUADROS VIDAL",
							"SUSANA MILAGROS CUADROS VIDAL" },
					{ "TECNOLOGIA EN POLIMEROS SAC",
							"FABRICA DE PRODUCTOS PLASTICOS" },
					{ "TEMPESTA MAGUI�A RENZO MICHELE",
							"TEMPESTA MAGUI�A RENZO MICHELE" },
					{ "TEXTIL ISSA S.A.", "TEXTIL ISSA S.A." },
					{ "TEXTILES DEL SUR ", "TEXTILES DEL SUR " },
					{ "TRANSACCIONES ECONOMICAS SRL",
							"TRANSACCIONES ECONOMICAS SRL" },
					{ "TUBOS Y QUIMICOS INDUSTRIALES EIRL",
							"TUBOS Y QUIMICOS INDUSTRIALES EIRL" },
					{ "TUDELA SALAS GABRIEL", "TUDELA SALAS GABRIEL" },
					{ "TUSA COTTON S.A.", "TUSA COTTON S.A." },
					{ "UNIVERSIDAD  DE PIURA", "UNIVERSIDAD  DE PIURA" },
					{ "UNIVERSIDAD MAYOR DE SAN MARCOS",
							"UNIVERSIDAD MAYOR DE SAN MARCOS" },
					{ "UNIVERSIDAD PERUANA DE CIENCIAS APLICADAS", "UPC" },
					{ "UNIVERSIDAD SAN IGNACIO DE LOYOLA", "USIL" },
					{ "VEGETALIA SAC", "VEGETALIA SAC" },
					{ "VERA RONCALLA INGENIEROS S.A.",
							"VERA RONCALLA INGENIEROS S.A." },
					{ "vfg", "sssssssssssss" },
					{ "VICTOR PALOMARES JUSTINIANO",
							"VICTOR PALOMARES JUSTINIANO" },
					{ "VICTORIANO MARCE CHAMBE", "VICTORIANO MARCE CHAMBE" },
					{ "YING KIT SAC", "YING KIT SAC" },
					{ "Z ADITIVOS S.A.", "FABRICA DE PRODUCTOS QUIMICOS" },
					{ "zxzx", "tttttttttttttttttttttttttt" }

			//
			};
			DAOCliente daoCliente = new DAOCliente();
			for (String[] row : data) {
				cliente = new Cliente();
				cliente.setId(UUID.randomUUID().toString());
				cliente.setRazonSocial(row[0]);
				cliente.setRazonComercial(new ArrayList<String>());
				for (int j = 1; j < row.length; j++) {
					cliente.getRazonComercial().add(row[j]);
				}

				daoCliente.save(cliente);
			}

		}
	}

	private static void createUsers() {
		// id,username,fullname,role,clave
		String[][] users = {

				// AutoGenerado
				{ "Administrator", "admin", "not24get",
						Role.RoleAdministrador.text, "adm@repsol.com" },

				{ "Comercial", "com", "secure", Role.RoleGerenteComercial.text,
						"com@repsol.com" },

				{ "Ingeniero", "ing", "secure",
						Role.RoleIngenieroDesarrolloTecnico.text,
						"ing@repsol.com" },
				// Supervisores comerciales
				{ "Cristina Arenas", "carenas", "secure",
						Role.RoleGerenteComercial.text,
						"cristina.arenasm@repsol.com", },
				{ "Guido Echevarria", "gechevarria", "secure",
						Role.RoleGerenteComercial.text,
						"gechevarriam@repsol.com", },
				{ "Paul Rivera", "privera", "secure",
						Role.RoleGerenteComercial.text,
						"paulroger.rivera@repsol.com", },
				{ "Julissa Manco", "jmanco", "secure",
						Role.RoleGerenteComercial.text, "jmancoo@repsol.com", },
				{ "Rodrigo de Madalengoitia", "rmadalengoitia", "secure",
						Role.RoleGerenteComercial.text,
						"r.demadalengoitia@repsol.com", },
				{ "Juan Jose Holguin", "jholguin", "secure",
						Role.RoleGerenteComercial.text,
						"juanjose.holguin@repsol.com", },
				{ "Jean Pierre Petit", "jpierre", "secure",
						Role.RoleGerenteComercial.text,
						"jean.petit@repsol.com", },

				// Ingeniero DT

				{ "Martin Medina", "mmedina", "secure",
						Role.RoleIngenieroDesarrolloTecnico.text,
						" mmedinam@repsol.com", },
				{ "Gonzalo Sevillano", "gsevillano", "secure",
						Role.RoleIngenieroDesarrolloTecnico.text,
						"gonzalo.sevillano@repsol.com", },
				{ "Jose Luis Quiroz", "jquiroz", "secure",
						Role.RoleIngenieroDesarrolloTecnico.text,
						"jquirozg@repsol.com", },
				{ "Maximo Flores", "mflores", "secure",
						Role.RoleIngenieroDesarrolloTecnico.text,
						"mdfloress@repsol.com", },
				{ "Miguel Roggero", "mroggero", "secure",
						Role.RoleIngenieroDesarrolloTecnico.text,
						"mroggerov@repsol.com", },
				{ "Kelly Vela", "kvela", "secure",
						Role.RoleIngenieroDesarrolloTecnico.text,
						"gestor.ingdt@yahoo.es", },
		/*
		 * 
		 * { "CARMEN CANELO", "ccanelo", "secure", "Gerente Comercial" }, // {
		 * "DANIEL DAVALOS ROSAS", "ddavalos", "secure", "Gerente Comercial" },
		 * // { "DIANA LEON", "dleon", "secure", "Gerente Comercial" }, // {
		 * "DIEGO VALENCIA", "dvalencia", "secure", "Gerente Comercial" }, // {
		 * "ETHEL GUTIERREZ", "egutierrez", "secure", "Gerente Comercial" }, //
		 * { "FRANCO QUEIROLO", "fqueirolo", "secure", "Gerente Comercial" }, //
		 * { "FRANZ SHABAUER", "fshabauer", "secure", "Gerente Comercial" }, //
		 * { "GUIDO ECHEVARRIA", "gechevarria", "secure", "Gerente Comercial" },
		 * // { "MIGUEL CALLE", "mcalle", "secure", "Gerente Comercial" }, // {
		 * "MARIA ESTHER TRUJILLO", "mesther", "secure", "Gerente Comercial" },
		 * // { "MAURICE RAFAEL", "mrafael", "secure", "Gerente Comercial" }, //
		 * { "PAUL RIVERA", "privera", "secure", "Gerente Comercial" }, // {
		 * "ATILIO SIVIRICHI", "asivirichi", "secure", "Gerente Comercial" }, //
		 * { "Jos� Luis Quiroz Gonzales", "jquiroz", "secure",
		 * "Ingeniero Desarrollo Tecnico" }, // {
		 * "Marcos Christer Gonzales Loarte ", "mgonzales", "secure",
		 * "Ingeniero Desarrollo Tecnico" }, // { "Mart�n Medina Masia ",
		 * "mmedina", "secure", "Ingeniero Desarrollo Tecnico" }, // {
		 * "Miguel Roggero Valdivieso", "mroggero", "secure",
		 * "Ingeniero Desarrollo Tecnico" }, // {
		 * "Marco Antonio V�squez - Sol�s Flores ", "mvasques", "secure",
		 * "Ingeniero Desarrollo Tecnico" }, // { "Ricardo Bautista Cepeda ",
		 * "rbautista", "secure", "Ingeniero Desarrollo Tecnico" }
		 */
		//
		};

		DAOUser daoUser = new DAOUser();
		User user;
		for (String[] strings : users) {
			user = new User();
			user.setId(strings[1]);

			user.setUserName(strings[1]);

			user.setFullName(strings[0]);
			user.setRole(strings[3]);
			user.setPassword(strings[2]);
			user.setEmail(strings[4]);

			daoUser.save(user);
		}
	}
}
