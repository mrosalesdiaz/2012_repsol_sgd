package com.baiken.inventario.util;

import java.util.Calendar;

public class DateUtil {
    public static int DateDiffInHours(Calendar from, Calendar to) {
        long milliseconds1 = from.getTimeInMillis();
        long milliseconds2 = to.getTimeInMillis();

        long diff = milliseconds2 - milliseconds1;
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000);
        long diffHours = diff / (60 * 60 * 1000);
        long diffDays = diff / (24 * 60 * 60 * 1000);

        return (int) diffHours;
    }
}
