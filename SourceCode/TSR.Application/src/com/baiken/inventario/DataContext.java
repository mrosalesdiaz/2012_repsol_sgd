package com.baiken.inventario;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.Vector;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import com.baiken.data.xml.FileUtil;
import com.baiken.framework.report.excel.IReporteableClass;
import com.baiken.framework.report.excel.ReportFactory;
import com.baiken.inventario.util.BuildReportConteoSolicitudesPorGerenteComercial;
import com.baiken.inventario.util.BuildReportConteoSolicitudesPorGerenteComercial.TipoReporte;
import com.baiken.inventario.util.EmailUtil;
import com.baiken.inventario.util.ExportComercialCellBuilder;
import com.baiken.inventario.util.ExportIngenieroDTCellBuilder;
import com.baiken.inventario.util.ExportSolicitudesNoAtendidasEnDosDias;
import com.baiken.inventario.util.MotivoMovimientoRowBuilder;
import com.baiken.inventario.util.TanqueEquipoRowBuilder;
import com.inventario.dao.DAOCanal;
import com.inventario.dao.DAOCliente;
import com.inventario.dao.DAOConfiguracion;
import com.inventario.dao.DAOCuentaContable;
import com.inventario.dao.DAOEECC;
import com.inventario.dao.DAOMotivoEjecucion;
import com.inventario.dao.DAOPartida;
import com.inventario.dao.DAOSolicitud;
import com.inventario.dao.DAOUser;
import com.inventario.dao.tipo.DAOTipoEquipo;
import com.inventario.dao.tipo.DAOTipoInstalacion;
import com.inventario.dao.tipo.DAOTipoMarca;
import com.inventario.dao.tipo.DAOTipoOrientacion;
import com.inventario.dao.tipo.DAOTipoSolicitud;
import com.inventario.dto.ArchivoAdjunto;
import com.inventario.dto.Canal;
import com.inventario.dto.Cliente;
import com.inventario.dto.Contacto;
import com.inventario.dto.CuentaContable;
import com.inventario.dto.Eecc;
import com.inventario.dto.Equipo;
import com.inventario.dto.MotivoEjecucion;
import com.inventario.dto.Partida;
import com.inventario.dto.Solicitud;
import com.inventario.dto.Tanque;
import com.inventario.dto.User;
import com.inventario.dto.tipo.TipoEquipo;
import com.inventario.dto.tipo.TipoEstadoSolicitud;
import com.inventario.dto.tipo.TipoInstalacion;
import com.inventario.dto.tipo.TipoMarca;
import com.inventario.dto.tipo.TipoOrientacion;
import com.inventario.dto.tipo.TipoServicio;
import com.inventario.view.util.Role;

class DataContext {
	private Logger logger = Logger
			.getLogger(ApplicationContext.class.getName());

	private IReporteableClass generadorReportsolicitud;
	private IReporteableClass generadorReportOrden;
	private IReporteableClass generadorReportOrdenGrua;
	private IReporteableClass generadorExportarSolicitudesGerenteComercial;

	User loggedUser;

	Solicitud solicitud;

	List<Canal> listaDeCanales;
	List<Cliente> listaDeClientes;
	List<User> listaDeIngenierosDesarrolloTecnico;
	List<User> listaDeSupervisorComercial;
	List<Partida> listaDePartidas;
	List<CuentaContable> listaDeCuentaContables;
	List<User> listaDeUsuarios;

	List<TipoInstalacion> listaDeTipoInstalacion;
	List<TipoOrientacion> listaDeTipoOrientacion;
	List<TipoMarca> listaDeTipoMarca;
	List<TipoEquipo> listaDeTipoEquipo;

	List<TipoServicio> listaDeTipoSolicitud;
	List<Solicitud> listaDeSolicitudes;

	private DAOUser daoUser;
	private DAOCliente daoCliente;
	private DAOCanal daoCanal;
	private DAOPartida daoPartida;
	private DAOCuentaContable daoCuentaContable;

	private DAOConfiguracion daoConfiguracion;
	private DAOSolicitud daoSolicitud;

	private DAOTipoEquipo daoTipoEquipo;
	private DAOTipoInstalacion daoTipoInstalacion;
	private DAOTipoMarca daoTipoMarca;
	private DAOTipoOrientacion daoTipoOrientacion;
	private DAOTipoSolicitud daoTipoSolicitud;

	// TODO: Eliminar
	BuildReportConteoSolicitudesPorGerenteComercial buildReport;
	List<Eecc> listaDeEECC;
	List<MotivoEjecucion> listaDeMotivoEjecucion;
	private DAOEECC daoEECC;
	private DAOMotivoEjecucion daoMotivoEjecucion;
	private IReporteableClass generadorExportarOrdenesPorVencer;

	public DataContext() {
		logger.info("Loading Excel Templates");
		generadorReportsolicitud = ReportFactory.createReporteable(new File(
				"templates", "formatoSolicitud.xlsx"));
		generadorReportOrden = ReportFactory.createReporteable(new File(
				"templates", "formatoOrdenTrabajo.xlsx"));
		generadorReportOrdenGrua = ReportFactory.createReporteable(new File(
				"templates", "formatoOrdenTrabajoGrua.xlsx"));
		generadorExportarSolicitudesGerenteComercial = ReportFactory
				.createReporteable(new File("templates",
						"formatoExportarDatosGerenteComercial.xlsx"));
		generadorExportarOrdenesPorVencer = ReportFactory
				.createReporteable(new File("templates",
						"formatoSolicitudesPorVencer.xlsx"));

		logger.info("Init DAO classes");
		this.daoUser = new DAOUser();
		this.daoCliente = new DAOCliente();
		this.daoCanal = new DAOCanal();
		this.daoPartida = new DAOPartida();
		this.daoCuentaContable = new DAOCuentaContable();
		this.daoConfiguracion = new DAOConfiguracion();
		this.daoSolicitud = new DAOSolicitud();

		daoTipoEquipo = new DAOTipoEquipo();
		daoTipoInstalacion = new DAOTipoInstalacion();
		daoTipoMarca = new DAOTipoMarca();
		daoTipoOrientacion = new DAOTipoOrientacion();
		daoTipoSolicitud = new DAOTipoSolicitud();
		this.daoEECC = new DAOEECC();
		this.daoMotivoEjecucion = new DAOMotivoEjecucion();

		{// Lauching in other threads
			Thread[] threads=new Thread[4];
			logger.info("Starting to load initial data");
			threads[0]=new Thread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					loadListaDeCuentaContables();
					logger.info("Loaded CuentasContables data");
					
				}
			});
			threads[1]=new Thread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
				loadListaDeCanales();
				logger.info("Loaded Canales data");
					
				}
			});
			threads[2]=new Thread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
				loadListaDeClientes();
				logger.info("Loaded Clientes data");
					
				}
			});
			threads[3]=new Thread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
				loadListaDePartidas();
				logger.info("Loaded Partidas data");
					
				}
			});
			for (int i = 0; i < threads.length; i++) {
				threads[i].start();
			}
		}

		logger.info("Loading IngenieroDt data");
		loadListaDeIngenierosDesarrolloTecnico();
		logger.info("Loading SupervisorComercial data");
		loadListaDeSupervisorComercial();
	}

	void removeUsuario(String id) {
		daoUser.delete(id);
	}

	void loadListaDeUsuarios() {
		if (listaDeUsuarios == null) {
			listaDeUsuarios = new ArrayList<User>();
		}
		listaDeUsuarios.clear();
		listaDeUsuarios.addAll(daoUser.list());
	}

	void loadListaDeCuentaContables() {
		if (listaDeCuentaContables == null) {
			listaDeCuentaContables = new ArrayList<CuentaContable>();
		}
		listaDeCuentaContables.clear();
		listaDeCuentaContables.addAll(daoCuentaContable.list());
	}

	void loadListaDeSolicitudes(String xPath) {

		if (listaDeSolicitudes == null) {
			listaDeSolicitudes = new ArrayList<Solicitud>();
		}
		listaDeSolicitudes.clear();

		List<Solicitud> solicitudes = daoSolicitud.search(xPath);

		listaDeSolicitudes.addAll(solicitudes);
	}

	void loadListaDePartidas() {
		if (listaDePartidas == null) {
			listaDePartidas = new ArrayList<Partida>();
		}
		listaDePartidas.clear();
		listaDePartidas.addAll(daoPartida.list());
		Collections.sort(listaDePartidas, new Comparator<Partida>() {
			@Override
			public int compare(Partida o1, Partida o2) {
				return o1.getNombre().compareToIgnoreCase(o2.getNombre());
			}
		});
	}

	void loadListaDeIngenierosDesarrolloTecnico() {
		if (listaDeIngenierosDesarrolloTecnico == null) {
			listaDeIngenierosDesarrolloTecnico = new ArrayList<User>();
		}
		listaDeIngenierosDesarrolloTecnico.clear();

		listaDeIngenierosDesarrolloTecnico.addAll(daoUser
				.search("/user/role/text()='"
						+ Role.RoleIngenieroDesarrolloTecnico.text + "'"));

		Collections.sort(listaDeIngenierosDesarrolloTecnico,
				new Comparator<User>() {
					@Override
					public int compare(User o1, User o2) {
						return o1.getFullName().compareToIgnoreCase(
								o2.getFullName());
					}
				});
	}

	void loadListaDeSupervisorComercial() {
		if (listaDeSupervisorComercial == null) {
			listaDeSupervisorComercial = new ArrayList<User>();
		}
		listaDeSupervisorComercial.clear();
		// TODO: llenar con lista de ingenieros
		listaDeSupervisorComercial.addAll(daoUser.search("/user/role/text()='"
				+ Role.RoleGerenteComercial.text + "'"));
		Collections.sort(listaDeSupervisorComercial, new Comparator<User>() {
			@Override
			public int compare(User o1, User o2) {
				return o1.getFullName().compareToIgnoreCase(o2.getFullName());
			}
		});
	}

	void loadListaDeClientes() {
		if (listaDeClientes == null) {
			listaDeClientes = new ArrayList<Cliente>();
		}
		listaDeClientes.clear();
		listaDeClientes.addAll(daoCliente.list());

		Collections.sort(listaDeClientes, new Comparator<Cliente>() {
			@Override
			public int compare(Cliente o1, Cliente o2) {
				return o1.getRazonSocial().compareToIgnoreCase(
						o2.getRazonSocial());
			}
		});
	}

	void loadListaDeCanales() {
		if (listaDeCanales == null) {
			listaDeCanales = new ArrayList<Canal>();
		}
		listaDeCanales.clear();
		listaDeCanales.addAll(daoCanal.list());

		Collections.sort(listaDeCanales, new Comparator<Canal>() {
			@Override
			public int compare(Canal o1, Canal o2) {
				return o1.getNombre().compareToIgnoreCase(o2.getNombre());
			}
		});
	}

	boolean loginUser(String userName, String password) {
		boolean returnValue = false;
		try {
			loggedUser = daoUser.validateUser(userName, password);
			returnValue = loggedUser != null;
		} catch (Exception e) {
		}
		return returnValue;
	}

	public void guardarUsuario(User user) {
		daoUser.save(user);
	}

	public boolean validateUserName(String userName) {
		boolean returnValue = daoUser.userNameIsValid(userName);
		return returnValue;
	}

	public boolean validateNumeroInstalacionNoRepetido(String idSolicitud,
			String numeroInstalacion) {
		boolean returnValue = true;
		DAOSolicitud dao = new DAOSolicitud();
		List<Solicitud> solicitudesEncontradas = dao.search("//"
				+ Solicitud.INSTALACION + "[text() = '" + numeroInstalacion
				+ "']/text()!=''  ");

		if (solicitudesEncontradas.size() == 1) {
			returnValue = solicitudesEncontradas.get(0).getId()
					.equals(idSolicitud);
		}

		return returnValue;
	}

	public void guadarSolicitud(Solicitud dto) {
		// !!!! Importante para enviarle el correo
		Contacto contactoEECC = null;
		{// Guardando la data si es un nuevo ususario
			List<Cliente> clientes = daoCliente.search("/cliente/"
					+ Cliente.RAZON_SOCIAL + "[1]/text()='"
					+ dto.getRazonSocial() + "'");
			Cliente cliente;
			if (clientes.size() == 1) {
				cliente = clientes.get(0);

				boolean found = false;
				for (String razonComercial : cliente.getRazonComercial()) {
					if (razonComercial
							.equalsIgnoreCase(dto.getRazonComercial())) {
						found = true;
					}
				}
				if (!found) {
					cliente.getRazonComercial().add(dto.getRazonComercial());
					daoCliente.save(cliente);
				}
			} else {
				cliente = new Cliente();
				cliente.setRazonSocial(dto.getRazonSocial());
				cliente.setRazonComercial(new ArrayList<String>());
				cliente.getRazonComercial().add(dto.getRazonComercial());

				daoCliente.save(cliente);
			}

		}

		if (solicitud != null && solicitud.getEecc() != null
				&& !solicitud.getEecc().equals("")) {
			/*
			 * Guardando la data si es un motivo, EECC o responsable nuevo
			 */
			List<Eecc> eeccs = daoEECC.search("/eecc/" + Eecc.NOMBRE
					+ "[1]/text()='" + dto.getEecc().trim() + "'");
			if (eeccs.size() > 0) {
				boolean found = false;
				for (Eecc tempEecc : eeccs) {
					if (tempEecc.getNombre().equalsIgnoreCase(dto.getEecc())) {
						boolean foundResponsable = false;
						for (Contacto tempContacto : tempEecc.getResponsables()) {
							if (tempContacto.getNombre().equalsIgnoreCase(
									solicitud.getResponsableEECC())) {
								foundResponsable = true;
								contactoEECC = tempContacto;
								break;
							}
						}
						if (!foundResponsable) {
							Contacto contacto = new Contacto();
							contacto.setNombre(solicitud.getResponsableEECC());
							String correo = "";
							do {
								correo = JOptionPane
										.showInputDialog("Ingrese una direccion de correo correcta para este nuevo responsable de EECC.\n(Este dato no podra ser modificado posteriormente)");
								if (correo == null || correo.equals("")) {
									break;
								}
							} while (!EmailUtil.isValidEmailAddress(correo));

							contacto.setEmail(correo);

							tempEecc.getResponsables().add(contacto);
							contactoEECC = contacto;
						}
						found = true;
						daoEECC.save(tempEecc);
						break;
					}
				}
				if (!found) {
					Eecc newEECC = new Eecc();
					newEECC.setNombre(solicitud.getEecc());
					newEECC.setResponsables(new ArrayList<Contacto>());

					Contacto newContacto = new Contacto();
					newContacto.setNombre(solicitud.getResponsableEECC());

					contactoEECC = newContacto;

					newEECC.getResponsables().add(newContacto);
					daoEECC.save(newEECC);
				}
			} else {
				Eecc newEECC = new Eecc();
				newEECC.setNombre(solicitud.getEecc());
				newEECC.setResponsables(new ArrayList<Contacto>());

				Contacto newContacto = new Contacto();
				newContacto.setNombre(solicitud.getResponsableEECC());
				contactoEECC = newContacto;

				newEECC.getResponsables().add(newContacto);
				daoEECC.save(newEECC);
			}

		}

		if (solicitud.getMotivoParaEjecucion() != null
				&& !solicitud.getMovimientosTanque().equals("")) {
			/*
			 * Guardando la data si es un motivo, EECC o responsable nuevo
			 */
			List<MotivoEjecucion> motivosDeEjecucion = daoMotivoEjecucion
					.search("/motivoEjecucion/" + MotivoEjecucion.NOMBRE
							+ "[1]/text()='"
							+ dto.getMotivoParaEjecucion().trim() + "'");
			if (motivosDeEjecucion.size() > 0) {
				boolean found = false;
				for (MotivoEjecucion tempEecc : motivosDeEjecucion) {
					if (tempEecc.getNombre().equalsIgnoreCase(
							dto.getMotivoParaEjecucion())) {
						found = true;
						break;
					}
				}
				if (!found) {
					MotivoEjecucion newEECC = new MotivoEjecucion();
					newEECC.setNombre(solicitud.getMotivoParaEjecucion());

					daoMotivoEjecucion.save(newEECC);
				}
			} else {
				MotivoEjecucion newEECC = new MotivoEjecucion();
				newEECC.setNombre(solicitud.getMotivoParaEjecucion());

				daoMotivoEjecucion.save(newEECC);
			}

		}

		if (solicitud.getEstado() == TipoEstadoSolicitud.PENDIENTE) {
			/*
			 * Generando archivo de excel
			 */
			File reportFile = createFormatoSolicitudFile(solicitud);
			String fileName = "Solicitud.xlsx";

			ArchivoAdjunto archivoAdjunto = new ArchivoAdjunto();
			archivoAdjunto.setFileName(UUID.randomUUID().toString() + "-"
					+ fileName);
			archivoAdjunto.setFecha(Calendar.getInstance().getTime());
			archivoAdjunto.setTitulo(MessageFormat.format(
					"{0,date,yy/MM/dd HH:mm}: {1}", archivoAdjunto.getFecha(),
					fileName));

			solicitud.getArchivosAdjuntos().add(archivoAdjunto);

			adjuntarArchivo(solicitud.getId(), archivoAdjunto.getFileName(),
					reportFile);
			{
				try {
					File beforeFile = new File(reportFile.getAbsolutePath());
					reportFile = new File(System.getProperty("java.io.tmpdir"),
							solicitud.getCodigoSolicitud() + "-" + fileName);
					if (reportFile.exists()) {
						reportFile.delete();
					}
					reportFile.createNewFile();
					FileUtil.copyfile(beforeFile, reportFile);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}

			}

			enviarCorreo("[Solicitud][" + solicitud.getCodigoSolicitud() + "]",
					solicitud.getIngenieroDT().getFullName(), solicitud
							.getIngenieroDT().getEmail(), "", reportFile);
		} else if (solicitud.getEstado() == TipoEstadoSolicitud.EJECUCION) {
			// Si la seccion de motivo movimiento esta activa se cambia de
			// plantilla
			File reportOrden;
			{
				// imprimir c grua
				/*
				 * Generando archivo de excel
				 */
				reportOrden = createFormatoOrdenFile(solicitud);
				String fileName = "Orden.xlsx";

				ArchivoAdjunto archivoAdjunto = new ArchivoAdjunto();
				archivoAdjunto.setFileName(UUID.randomUUID().toString() + "-"
						+ fileName);
				archivoAdjunto.setFecha(Calendar.getInstance().getTime());
				archivoAdjunto.setTitulo(MessageFormat.format(
						"{0,date,yy/MM/dd HH:mm}: {1}",
						archivoAdjunto.getFecha(), fileName));

				solicitud.getArchivosAdjuntos().add(archivoAdjunto);

				adjuntarArchivo(solicitud.getId(),
						archivoAdjunto.getFileName(), reportOrden);
				{
					try {
						File beforeFile = new File(
								reportOrden.getAbsolutePath());
						reportOrden = new File(
								System.getProperty("java.io.tmpdir"),
								solicitud.getCodigoSolicitud() + "-" + fileName);
						if (reportOrden.exists()) {
							reportOrden.delete();
						}
						reportOrden.createNewFile();
						FileUtil.copyfile(beforeFile, reportOrden);
					} catch (IOException e) {
						throw new RuntimeException(e);
					}

				}
			}
			File gruaFile = null;
			if (solicitud.isMovimientoTanque()) {
				// Imprimir tipo normal
				/*
				 * Generando archivo de excel
				 */
				gruaFile = createFormatoOrdenGruaFile(solicitud);
				String fileName = "OrdenGrua.xlsx";

				ArchivoAdjunto archivoAdjunto = new ArchivoAdjunto();
				archivoAdjunto.setFileName(UUID.randomUUID().toString() + "-"
						+ fileName);
				archivoAdjunto.setFecha(Calendar.getInstance().getTime());
				archivoAdjunto.setTitulo(MessageFormat.format(
						"{0,date,yy/MM/dd HH:mm.ss} {1}",
						archivoAdjunto.getFecha(), fileName));

				solicitud.getArchivosAdjuntos().add(archivoAdjunto);

				adjuntarArchivo(solicitud.getId(),
						archivoAdjunto.getFileName(), gruaFile);
				{
					try {
						File beforeFile = new File(gruaFile.getAbsolutePath());
						gruaFile = new File(
								System.getProperty("java.io.tmpdir"),
								solicitud.getCodigoSolicitud() + "-" + fileName);
						if (gruaFile.exists()) {
							gruaFile.delete();
						}
						gruaFile.createNewFile();
						FileUtil.copyfile(beforeFile, gruaFile);
					} catch (IOException e) {
						throw new RuntimeException(e);
					}

				}
			}
			if (gruaFile == null) {
				enviarCorreo(
						"[Orden de Servicio]["
								+ solicitud.getCodigoOrdenSoporteTecnico()
								+ "]", contactoEECC.getNombre(),
						contactoEECC.getEmail(), "", reportOrden);
			} else {
				enviarCorreo(
						"[Orden de Servicio]["
								+ solicitud.getCodigoOrdenSoporteTecnico()
								+ "]", contactoEECC.getNombre(),
						contactoEECC.getEmail(), "", reportOrden, gruaFile);
			}
		} else if (solicitud.getEstado() == TipoEstadoSolicitud.FINALIZADA) {

		}
		daoSolicitud.save(dto);
	}

	private void enviarCorreo(String subject, String fullName, String email,
			String body, File... attachedFile) {
		try {

			String[] mails = email.split(";");

			String otherMail = "";

			if (mails.length > 1) {
				otherMail = "&otherMail=";
				for (int i = 1; i < mails.length; i++) {
					otherMail += ";" + mails[i];
				}
			}

			String attachString = "";
			for (File file : attachedFile) {
				attachString += "&attach=" + file.getAbsolutePath();
			}
			Runtime.getRuntime().exec(
					new String[] {
							"outlookSend/sendmail.exe",
							MessageFormat.format(
									" mailto:{0}?subject={1}{4}&body={2}{3}",
									mails[0], subject, body, attachString,
									otherMail) }, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private File createFormatoSolicitudFile(Solicitud dto) {
		File returnValue;
		try {
			returnValue = File.createTempFile("solicitud", ".xlsx");
			generadorReportsolicitud.setVariable("canal", solicitud.getCanal());
			
			{
				int conteoDeEquipo = 0;
				for (Equipo itemEquipo : solicitud.getEquipo()) {
					conteoDeEquipo += itemEquipo.getCantidad();
				}

				generadorReportsolicitud.setVariable("cantidadEquipos",
						conteoDeEquipo);

				int conteoDeTanques = 0;
				for (Tanque itemTanque : solicitud.getTanque()) {
					conteoDeTanques += itemTanque.getCantidad();
				}
				generadorReportsolicitud.setVariable("cantidadTanques",
						conteoDeTanques);
			}
			
			generadorReportsolicitud.setVariable("centroCosto",
					solicitud.getCentroCosto());
			generadorReportsolicitud.setVariable("contacto",
					solicitud.getPersonaContactoNombre());
			generadorReportsolicitud.setVariable("cuentaContable",
					solicitud.getCuentaContable());
			generadorReportsolicitud.setVariable("descripcionServicio",
					solicitud.getDescripcionServicio());
			generadorReportsolicitud.setVariable("direccion",
					solicitud.getDireccionObra());
			generadorReportsolicitud.setVariable("email",
					solicitud.getPersonaContactoEmail());
			generadorReportsolicitud.setVariable(
					"fechaEmision",
					MessageFormat.format("{0,date,dd/MM/yyyy}",
							solicitud.getFechaEmision()));
			generadorReportsolicitud.setVariable(
					"fechaFin",
					MessageFormat.format("{0,date,dd/MM/yyyy}",
							solicitud.getFechaFinObra()));
			generadorReportsolicitud.setVariable(
					"fechaInicio",
					MessageFormat.format("{0,date,dd/MM/yyyy}",
							solicitud.getFechaInicioObra()));
			generadorReportsolicitud.setVariable("horizontal",
					solicitud.getDistanciaHorizontal());
			generadorReportsolicitud.setVariable("ingenieroDT", solicitud
					.getIngenieroDT().getFullName());
			generadorReportsolicitud.setVariable("instalacion",
					solicitud.getInstalacion());
			generadorReportsolicitud.setVariable("justificacionAFI",
					solicitud.getJustificacionAFI());
			generadorReportsolicitud.setVariable("numeroAFI",
					solicitud.getNumeroAFI());
			generadorReportsolicitud.setVariable("numeroSolicitud",
					solicitud.getCodigoSolicitud());
			generadorReportsolicitud.setVariable("planta",
					solicitud.getCodigoPlanta());
			generadorReportsolicitud.setVariable("razonComercial",
					solicitud.getRazonComercial());
			generadorReportsolicitud.setVariable("razonSocial",
					solicitud.getRazonSocial());
			generadorReportsolicitud.setVariable("referencia",
					solicitud.getReferencia());
			generadorReportsolicitud.setVariable("rucODni",
					solicitud.getRucOdni());
			generadorReportsolicitud.setVariable("solicitadoPor", solicitud
					.getSupervisorComercial().getFullName());
			generadorReportsolicitud.setVariable("supervisorComercial",
					solicitud.getSupervisorComercial().getFullName());
			generadorReportsolicitud.setVariable("telefono",
					solicitud.getPersonaContactoTelefono());
			generadorReportsolicitud.setVariable("tipoPartida",
					solicitud.getTipoPartida());
			generadorReportsolicitud.setVariable("titular",
					solicitud.getTitular());
			generadorReportsolicitud.setVariable("tituloReporte", MessageFormat
					.format("SOLICITUD DE SOPORTE T�CNICO - {0}", solicitud
							.getTipoSolicitud().toUpperCase()));
			generadorReportsolicitud.setVariable("vertical",
					solicitud.getDistanciaVertical());
			generadorReportsolicitud.setVariable("nombrePlanta",
					solicitud.getNombrePlanta());

			int row = 21;
			int maxRows = Math.max(solicitud.getTanque() == null ? 0
					: solicitud.getTanque().size(),
					solicitud.getEquipo() == null ? 0 : solicitud.getEquipo()
							.size());

			if (maxRows > 0) {
				TanqueEquipoRowBuilder rowBuilder = new TanqueEquipoRowBuilder(
						dto);
				generadorReportsolicitud.addRowsBelow(row, maxRows, rowBuilder);

			}

			generadorReportsolicitud.save(returnValue);
		} catch (Exception e) {
			returnValue = null;
		}

		return returnValue;
	}

	private File createFormatoOrdenGruaFile(Solicitud dto) {
		File returnValue;
		try {
			returnValue = File.createTempFile("orden", ".xlsx");

			generadorReportOrdenGrua.setVariable("codigoOrden",
					solicitud.getCodigoOrdenSoporteTecnico());
			generadorReportOrdenGrua.setVariable("razonSocial",
					solicitud.getRazonSocial());
			generadorReportOrdenGrua.setVariable(
					"fechaEmision",
					MessageFormat.format("{0,date,dd/MM/yyyy}",
							solicitud.getFechaEmision()));
			generadorReportOrdenGrua.setVariable("razonComercial",
					solicitud.getRazonComercial());
			generadorReportOrdenGrua.setVariable("rucODni",
					solicitud.getRucOdni());
			generadorReportOrdenGrua.setVariable("direccion",
					solicitud.getDireccionObra());
			generadorReportOrdenGrua.setVariable("referencia",
					solicitud.getReferencia());
			generadorReportOrdenGrua.setVariable("canal", solicitud.getCanal());
			generadorReportOrdenGrua.setVariable("tipoPartida",
					solicitud.getTipoPartida());
			generadorReportOrdenGrua.setVariable("nombrePlanta",
					solicitud.getNombrePlanta());
			generadorReportOrdenGrua.setVariable("centroCosto",
					solicitud.getCentroCosto());
			generadorReportOrdenGrua.setVariable("cuentaContable",
					solicitud.getCuentaContable());
			generadorReportOrdenGrua.setVariable("numeroAFI",
					solicitud.getNumeroAFI());

			generadorReportOrdenGrua.setVariable("motivoMovimientoTanque",
					solicitud.getMotivoMovimientoTanqueDescripcion());
			generadorReportOrdenGrua.setVariable("comentariosDeMotivoMovimientoTanque",
					solicitud.getMovimientoTanqueComentario());
			generadorReportOrdenGrua.setVariable("supervisorComercial",
					solicitud.getSupervisorComercial().getFullName());
			generadorReportOrdenGrua.setVariable("eecc", solicitud.getEecc());
			generadorReportOrdenGrua.setVariable("ingenieroDT", solicitud
					.getIngenieroDT().getFullName());
			generadorReportOrdenGrua.setVariable("responsableEECC",
					solicitud.getResponsableEECC());
			generadorReportOrdenGrua.setVariable("personaContacto",
					solicitud.getPersonaContactoNombre());
			generadorReportOrdenGrua.setVariable("telefonosContacto",
					solicitud.getPersonaContactoTelefono());

			// generadorReportOrdenGrua.setVariable("fechaDeSalida",
			// MessageFormat.format("{0,date,dd/MM/yyyy}",
			// solicitud.getFechaSalida()));
			// generadorReportOrdenGrua.setVariable("numeroGuiaRemision",
			// solicitud.getNumeroGuia());
			// generadorReportOrdenGrua.setVariable("fechaRecepcion",
			// MessageFormat.format("{0,date,dd/MM/yyyy}",
			// solicitud.getFechaRecepcion()));

			if (solicitud.getMovimientosTanque() != null) {
				int row = 19;
				int maxRows = solicitud.getMovimientosTanque().size();
				if (maxRows > 0) {
					MotivoMovimientoRowBuilder rowBuilder = new MotivoMovimientoRowBuilder(
							dto);
					generadorReportOrdenGrua.addRowsBelow(row, maxRows,
							rowBuilder);
				}

			}
			generadorReportOrdenGrua.save(returnValue);
		} catch (Exception e) {
			returnValue = null;
		}

		return returnValue;
	}

	private File createFormatoOrdenFile(Solicitud dto) {
		File returnValue;
		try {
			returnValue = File.createTempFile("orden", ".xlsx");
			generadorReportOrden.setVariable("numeroOrden",
					solicitud.getCodigoOrdenSoporteTecnico());

			generadorReportOrden.setVariable(
					"fechaEmision",
					MessageFormat.format("{0,date,dd/MM/yyyy}",
							solicitud.getFechaEmision()));
			generadorReportOrden.setVariable("solicitadoPor", solicitud
					.getSupervisorComercial().getFullName());
			generadorReportOrden.setVariable("razonSocial",
					solicitud.getRazonSocial());
			generadorReportOrden.setVariable("rucOdni", solicitud.getRucOdni());
			generadorReportOrden.setVariable("razonComercial",
					solicitud.getRazonComercial());
			generadorReportOrden.setVariable("canel", solicitud.getCanal());
			generadorReportOrden.setVariable("direccionObra",
					solicitud.getDireccionObra());
			generadorReportOrden.setVariable("referencia",
					solicitud.getReferencia());
			generadorReportOrden.setVariable("descripcionServicio",
					solicitud.getDescripcionServicio());
			{
				int conteoDeEquipo = 0;
				for (Equipo itemEquipo : solicitud.getEquipo()) {
					conteoDeEquipo += itemEquipo.getCantidad();
				}

				generadorReportOrden.setVariable("cantidadEquipos",
						conteoDeEquipo);

				int conteoDeTanques = 0;
				for (Tanque itemTanque : solicitud.getTanque()) {
					conteoDeTanques += itemTanque.getCantidad();
				}
				generadorReportOrden.setVariable("cantidadTanques",
						conteoDeTanques);
			}
			
			generadorReportOrden.setVariable("ingenieroDT", solicitud
					.getIngenieroDT().getFullName());
			generadorReportOrden.setVariable("supervisorComercial", solicitud
					.getSupervisorComercial().getFullName());
			generadorReportOrden.setVariable("titular", solicitud.getTitular());
			generadorReportOrden.setVariable("instalacion",
					solicitud.getInstalacion());
			generadorReportOrden.setVariable(
					"fechaInicio",
					MessageFormat.format("{0,date,dd/MM/yyyy}",
							solicitud.getFechaInicioObra()));
			generadorReportOrden.setVariable(
					"fechaFin",
					MessageFormat.format("{0,date,dd/MM/yyyy}",
							solicitud.getFechaFinObra()));
			generadorReportOrden.setVariable("motivoEjecucion",
					solicitud.getMotivoParaEjecucion());
			generadorReportOrden.setVariable("eeccAsignada",
					solicitud.getEecc());
			generadorReportOrden.setVariable("responsableEECC",
					solicitud.getResponsableEECC());
			generadorReportOrden.setVariable("tipoTrabajoServicio",
					solicitud.getTipoSolicitud());
			generadorReportOrden.setVariable("tipoPartida",
					solicitud.getTipoPartida());
			generadorReportOrden.setVariable("centroCosto",
					solicitud.getCentroCosto());
			generadorReportOrden.setVariable("cuentaContable",
					solicitud.getCuentaContable());
			generadorReportOrden.setVariable("numeroAFI",
					solicitud.getNumeroAFI());
			generadorReportOrden.setVariable("justificacionAFI",
					solicitud.getJustificacionAFI());
			generadorReportOrden.setVariable("montoPresupuestado",
					solicitud.getMontoPresupuestado());
			generadorReportOrden.setVariable("montoFacturadoSolesSinIGV",
					solicitud.getMontoFacturado());
			generadorReportOrden.setVariable("personaContacto",
					solicitud.getPersonaContactoNombre());
			generadorReportOrden.setVariable("telefonoContacto",
					solicitud.getPersonaContactoTelefono());
			generadorReportOrden.setVariable("emailContacto",
					solicitud.getPersonaContactoEmail());
			generadorReportOrden.setVariable("firmaIngenieroDT", solicitud
					.getIngenieroDT().getFullName());
			generadorReportOrden.setVariable("firmaEECC", solicitud.getEecc());

			int row = 20;
			int maxRows = Math.max(solicitud.getTanque() == null ? 0
					: solicitud.getTanque().size(),
					solicitud.getEquipo() == null ? 0 : solicitud.getEquipo()
							.size());

			if (maxRows > 0) {
				TanqueEquipoRowBuilder rowBuilder = new TanqueEquipoRowBuilder(
						dto);
				generadorReportOrden.addRowsBelow(row, maxRows, rowBuilder);

			}

			generadorReportOrden.save(returnValue);
		} catch (Exception e) {
			returnValue = null;
		}

		return returnValue;
	}

	public void loadListaDeTipoInstalacion() {
		if (listaDeTipoInstalacion == null) {
			listaDeTipoInstalacion = new ArrayList<TipoInstalacion>();
		}
		listaDeTipoInstalacion.clear();
		listaDeTipoInstalacion.addAll(daoTipoInstalacion.list());
	}

	public void loadListaDeTipoOrientacion() {
		if (listaDeTipoOrientacion == null) {
			listaDeTipoOrientacion = new ArrayList<TipoOrientacion>();
		}
		listaDeTipoOrientacion.clear();
		listaDeTipoOrientacion.addAll(daoTipoOrientacion.list());
	}

	public void loadListaDeTipoEquipo() {
		if (listaDeTipoEquipo == null) {
			listaDeTipoEquipo = new ArrayList<TipoEquipo>();
		}
		listaDeTipoEquipo.clear();
		listaDeTipoEquipo.addAll(daoTipoEquipo.list());
	}

	public void loadListaDeTipoMarca() {
		if (listaDeTipoMarca == null) {
			listaDeTipoMarca = new ArrayList<TipoMarca>();
		}
		listaDeTipoMarca.clear();
		listaDeTipoMarca.addAll(daoTipoMarca.list());
	}

	public void loadListaDeTipoSolicitudes() {

		if (listaDeTipoSolicitud == null) {
			listaDeTipoSolicitud = new ArrayList<TipoServicio>();
		}
		listaDeTipoSolicitud.clear();
		listaDeTipoSolicitud.addAll(daoTipoSolicitud.list());
	}

	public void adjuntarArchivo(String solicitudId, String nombreArchivo,
			File file) {
		daoSolicitud.adjuntarArchivo(solicitudId, nombreArchivo, file);
	}

	public File crearArchivoTemporal(String description) {

		return daoSolicitud.obtenerCopiaTemprarlDelArchivo(description);
	}

	public void generateReport(File file, Integer selectedYear,
			TipoReporte tipoReport, String formatedTitle) {
		buildReport.setReporte(tipoReport);
		buildReport.setSelectedYear(selectedYear);
		// generadorReporteConteoSolicitudesPorComercial.setVariable("formatedTitle",
		// MessageFormat.format(formatedTitle, "" + selectedYear));
		// generadorReporteConteoSolicitudesPorComercial.addRowsBelow(3,
		// buildReport.getRowCount(), buildReport);

		// generadorReporteConteoSolicitudesPorComercial.save(file);
		try {
			Desktop.getDesktop().open(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void exportarSolicitudesDeGerenteComercial(File file) {
		String query;

		if (loggedUser.getRole().equals(Role.RoleAdministrador.text)) {
			query = "/solicitud/@id != ''";
		} else {
			query = "/solicitud/supervisorComercial[@id = '"
					+ loggedUser.getId() + "']/@id!=''";
		}

		loadListaDeSolicitudes(query);
		ExportComercialCellBuilder cellBuilder = new ExportComercialCellBuilder(
				listaDeSolicitudes);

		this.generadorExportarSolicitudesGerenteComercial.addRowsBelow(5,
				cellBuilder.getRowCount(), cellBuilder);
		this.generadorExportarSolicitudesGerenteComercial.save(file);
		try {
			Desktop.getDesktop().open(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void exportarOrdenesDeIngenieroDT(File file) {
		String query;

		if (loggedUser.getRole().equals(Role.RoleAdministrador.text)) {
			query = "/solicitud/@id != ''";
		} else {
			query = "/solicitud/ingenieroDT[@id = '" + loggedUser.getId()
					+ "']/@id!=''";
		}

		loadListaDeSolicitudes(query);

		ExportIngenieroDTCellBuilder cellBuilder = new ExportIngenieroDTCellBuilder(
				listaDeSolicitudes);

		this.generadorExportarSolicitudesGerenteComercial.addRowsBelow(5,
				cellBuilder.getRowCount(), cellBuilder);
		this.generadorExportarSolicitudesGerenteComercial.save(file);
		try {
			Desktop.getDesktop().open(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void exportarSolicitudesPorVencer(File file) {
		loadListaDeSolicitudes("/solicitud/@id != ''");
		ExportSolicitudesNoAtendidasEnDosDias cellBuilder = new ExportSolicitudesNoAtendidasEnDosDias(
				listaDeSolicitudes);

		this.generadorExportarOrdenesPorVencer.addRowsBelow(5,
				cellBuilder.getRowCount(), cellBuilder);
		this.generadorExportarOrdenesPorVencer.save(file);
		try {
			Desktop.getDesktop().open(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String generarCodigoSoporteTecnico(String inicialTipoServicio,
			String planta) {
		return daoConfiguracion.generarNumeroParaTipoSolicitud(
				inicialTipoServicio, planta);
	}

	public void loadListaDeEECC() {
		if (listaDeEECC == null) {
			listaDeEECC = new ArrayList<Eecc>();
		}
		listaDeEECC.clear();
		listaDeEECC.addAll(daoEECC.list());
		Collections.sort(listaDeEECC, new Comparator<Eecc>() {
			@Override
			public int compare(Eecc o1, Eecc o2) {
				return o1.getNombre().compareToIgnoreCase(o2.getNombre());
			}
		});
	}

	public void loadListaDeMotivoEjecucion() {
		if (listaDeMotivoEjecucion == null) {
			listaDeMotivoEjecucion = new ArrayList<MotivoEjecucion>();
		}
		listaDeMotivoEjecucion.clear();
		listaDeMotivoEjecucion.addAll(daoMotivoEjecucion.list());
		Collections.sort(listaDeMotivoEjecucion,
				new Comparator<MotivoEjecucion>() {
					@Override
					public int compare(MotivoEjecucion o1, MotivoEjecucion o2) {
						return o1.getNombre().compareToIgnoreCase(
								o2.getNombre());
					}
				});
	}

	public void validateNumber(String string) {
		string = string.trim();
		Pattern pattern = Pattern.compile("[0-9]*");
		boolean valid = pattern.matcher(string).matches();
		if (!valid || string.equals("")) {
			throw new RuntimeException("Valor tiene q ser numerico: " + string);
		}
	}
}
