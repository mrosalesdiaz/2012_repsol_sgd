package com.inventario.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.baiken.data.xml.DTOBase;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.NONE)
public class Eecc extends DTOBase {

    public static String NOMBRE="nombre";
    @XmlElement
    private String nombre;
    

    public static String RESPONSABLES="responsables";
    @XmlElementWrapper(name = "responsables")
    @XmlElement
    private List<Contacto> responsables;

    public void setResponsables(List<Contacto> responsables) {
        this.responsables = responsables;
    }

    public List<Contacto> getResponsables() {
        return responsables;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

}
