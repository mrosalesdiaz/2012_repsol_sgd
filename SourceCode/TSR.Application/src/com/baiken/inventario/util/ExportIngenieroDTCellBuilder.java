package com.baiken.inventario.util;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

import com.baiken.framework.report.excel.ICellBuilder;
import com.inventario.dto.Solicitud;

public class ExportIngenieroDTCellBuilder implements ICellBuilder {

    private int rowCount;
    private Object[][] data;

    public ExportIngenieroDTCellBuilder(List<Solicitud> listaSolicitudes) {
        rowCount = listaSolicitudes.size();
        data = new Object[rowCount][25];
        for (int i = 0; i < rowCount; i++) {
            Solicitud solicitud = listaSolicitudes.get(i);
            int pos = 0;
            data[i][pos++] = solicitud.getCodigoSolicitud();
            data[i][pos++] = solicitud.getEstado() == 0 ? "PENDIENTE" : "ATENDIDA";
            data[i][pos++] = MessageFormat.format("{0,date,yyyy/MM/dd}", solicitud.getFechaEmision());
            data[i][pos++] = solicitud.getTipoSolicitud();
            data[i][pos++] = solicitud.getSupervisorComercial().getFullName();
            data[i][pos++] = solicitud.getIngenieroDT().getFullName();
            data[i][pos++] = solicitud.getCanal();
            data[i][pos++] = solicitud.getRazonSocial();
            data[i][pos++] = solicitud.getRazonComercial();
            data[i][pos++] = solicitud.getRucOdni();
            data[i][pos++] = solicitud.getTitular();
            data[i][pos++] = solicitud.getDireccionObra();
            data[i][pos++] = solicitud.getInstalacion();
            data[i][pos++] = solicitud.getReferencia();
            data[i][pos++] = solicitud.getDescripcionServicio();
            data[i][pos++] = MessageFormat.format("{0,date,yyyy/MM/dd}", solicitud.getFechaInicioObra());
            data[i][pos++] = MessageFormat.format("{0,date,yyyy/MM/dd}", solicitud.getFechaFinObra());
            data[i][pos++] = solicitud.getTipoPartida();
            data[i][pos++] = solicitud.getCentroCosto();
            data[i][pos++] = solicitud.getCuentaContable();
            data[i][pos++] = solicitud.getNumeroAFI();
            data[i][pos++] = solicitud.getJustificacionAFI();
            data[i][pos++] = solicitud.getPersonaContactoNombre();
            data[i][pos++] = solicitud.getPersonaContactoTelefono();
            data[i][pos++] = solicitud.getPersonaContactoEmail();
        }
    }

    @Override
    public Object processRow(int row, int col) {
        Object returnValue = "";
        if (data[row][col] != null) {
            returnValue = data[row][col];
        }

        if (returnValue.getClass().equals(Date.class)) {
            returnValue = MessageFormat.format("{0,date,yyyy/MM/dd}", returnValue);
        }
        return returnValue;
    }

    public int getRowCount() {
        return rowCount;
    }

}
